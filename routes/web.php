<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BonusGameController as AdminBonusGameController;
use App\Http\Controllers\Admin\BrandController as AdminBrandController;
use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\ChartController;
use App\Http\Controllers\Admin\DeliveryController;
use App\Http\Controllers\Admin\DumpController;
use App\Http\Controllers\Admin\OptimizationController;
use App\Http\Controllers\Admin\OrderReportController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\StoreController;
use App\Http\Controllers\Admin\StoreRepController;
use App\Http\Controllers\Admin\StoreRepresentativeSettingsController;
use App\Http\Controllers\Admin\TelegramUserController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

//Route::get('/waybill', function () {
//    return view('waybill');
//});

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.'
], function () {
    Route::group([
        'prefix' => 'auth',
        'as' => 'auth.'
    ], function () {
        Route::get('/login', [AuthController::class, 'login'])->name('login');
        Route::get('/list', [ProductController::class, 'listReport'])->name('123login');
        Route::get('/game_list', [ProductController::class, 'gameReport'])->name('12sadsa3login');
        Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('authenticate');
    });

    Route::group([
        'middleware' => ['auth:web']
    ], function () {
        Route::group([
            'prefix' => 'brand',
            'as' => 'brand.'
        ], function () {
            Route::get("all", [AdminBrandController::class, 'index'])->name('index');
            Route::get("create", [AdminBrandController::class, 'create'])->name('create');
            Route::post("store", [AdminBrandController::class, 'store'])->name('store');
            Route::get("edit/{brand}", [AdminBrandController::class, 'edit'])->name('edit');
            Route::post("update/{brand}", [AdminBrandController::class, 'update'])->name('update');
            Route::delete("destroy/{brand}", [AdminBrandController::class, 'destroy'])->name('destroy');
            Route::get('filter-brand', [AdminBrandController::class, 'filterBrand']);
            Route::get('all-brand', [AdminBrandController::class, 'allBrand']);
        });

        Route::group([
            'prefix' => 'category',
            'as' => 'category.'
        ], function () {
            Route::get("all", [AdminCategoryController::class, 'allCategory'])->name('allCategory');
            Route::get("create", [AdminCategoryController::class, 'create'])->name('create');
            Route::post("store", [AdminCategoryController::class, 'store'])->name('store');
            Route::get("edit/{category}", [AdminCategoryController::class, 'edit'])->name('edit');
            Route::post("update/{category}", [AdminCategoryController::class, 'update'])->name('update');
            Route::delete("destroy/{category}", [AdminCategoryController::class, 'destroy'])->name('destroy');
            Route::get('filter-category', [AdminCategoryController::class, 'filterCategory']);
        });

        Route::group([
            'prefix' => 'product',
            'as' => 'product.'
        ], function () {
            Route::get("all", [AdminProductController::class, 'index'])->name('index');
            Route::get("create", [AdminProductController::class, 'create'])->name('create');
            Route::post("store", [AdminProductController::class, 'store'])->name('store');
            Route::get("edit/{product}", [AdminProductController::class, 'edit'])->name('edit');
            Route::post("update/{product}", [AdminProductController::class, 'update'])->name('update');
            Route::delete("destroy/{product}", [AdminProductController::class, 'destroy'])->name('destroy');
            Route::get("filter-product", [ProductController::class, 'filterProduct']);
            Route::get("update-remainders", [\App\Http\Controllers\Admin\ProductController::class, 'updateRemaindersPage'])->name('update-remainders');
            Route::post("update-remainders-from-excel", [\App\Http\Controllers\Admin\ProductController::class, 'updateProductRemaindersFromExcel'])->name('updateRemainders');
            Route::get('download-catalogue', [AdminProductController::class, 'downloadCatalogue'])->name('download-catalogue');
        });

        Route::group([
            'prefix' => 'game',
            'as' => 'game.'
        ], function () {
            Route::get("all", [AdminBonusGameController::class, 'allGameHistory'])->name('allGameHistory');
            Route::post("all/search", [AdminBonusGameController::class, 'search'])->name('search');
        });

        Route::group([
            'prefix' => 'store-rep-setting',
            'as' => 'store-rep-settings.'
        ], function () {
            Route::get('all', [StoreRepresentativeSettingsController::class, 'index'])->name('index');
            Route::get('create', [StoreRepresentativeSettingsController::class, 'create'])->name('create');
            Route::delete('destroy/{storeRepresentativeSetting}', [StoreRepresentativeSettingsController::class, 'destroy'])->name('destroy');
            Route::get('edit/{storeRepresentativeSetting}', [StoreRepresentativeSettingsController::class, 'edit'])->name('edit');
            Route::post('update/{storeRepresentativeSetting}', [StoreRepresentativeSettingsController::class, 'update'])->name('update');
        });

        Route::group([
            'prefix' => 'order',
            'as' => 'order.'
        ], function () {
            Route::get('all', [OrderController::class, 'index'])->name('index');
            Route::get('basket/{order}', [OrderController::class, 'basket'])->name('basket');
            Route::get('edit/{order}', [OrderController::class, 'edit'])->name('edit');
            Route::post('update/{order}', [OrderController::class, 'update'])->name('update');
            Route::post('search', [OrderController::class, 'search'])->name('search');
            Route::get('failed-orders', [OrderController::class, 'failedOrders'])->name('failed');
            Route::get('audits', [OrderController::class, 'audits'])->name('audits');
            Route::delete('destroy/{order}', [OrderController::class, 'destroy'])->name('destroy');
            Route::get('{order}/download-single-report', [OrderController::class, 'downloadReport'])->name('download-report');
            Route::get('unknown-basket/{request}', [OrderController::class, 'unknownOrderBasket'])->name('unknown-basket');
        });

        Route::group([
            'prefix' => 'store',
            'as' => 'store.'
        ], function () {
            Route::get('all', [StoreController::class, 'index'])->name('index');
            Route::get('create', [StoreController::class, 'create'])->name('create');
            Route::delete('destroy/{store}', [StoreController::class, 'destroy'])->name('destroy');
            Route::get('edit/{store}', [StoreController::class, 'edit'])->name('edit');
            Route::post('update/{store}', [StoreController::class, 'update'])->name('update');

        });

        Route::group([
            'prefix' => 'user',
            'as' => 'user.'
        ], function () {
            Route::get('all', [UserController::class, 'index'])->name('index');
            Route::get('create', [UserController::class, 'create'])->name('create');
            Route::post('store', [UserController::class, 'store'])->name('store');
            Route::delete('destroy/{user}', [UserController::class, 'destroy'])->name('destroy');
            Route::get('edit/{user}', [UserController::class, 'edit'])->name('edit');
            Route::post('update/{user}', [UserController::class, 'update'])->name('update');
        });

        Route::group([
            'prefix' => 'order-report',
            'as' => 'order-report.'
        ], function () {
            Route::get('all', [OrderReportController::class, 'index'])->name('index');
            Route::get('show/{date}', [OrderReportController::class, 'show'])->name('show');
            Route::get('download-full/{date}', [OrderReportController::class, 'downloadFull'])->name('download-full');
            Route::get('download-report/{report}', [OrderReportController::class, 'downloadReport'])->name('download-report');
            Route::get('show-report/{report}', [OrderReportController::class, 'showReport'])->name('show-report');
        });

        Route::group([
            'prefix' => 'dump',
            'as' => 'dump.'
        ], function () {
            Route::get('all', [DumpController::class, 'index'])->name('index');
            Route::get('{dumpName}/download', [DumpController::class, 'download'])->name('download');
        });

        Route::group([
            'prefix' => 'delivery-status',
            'as' => 'delivery-status.',
        ], function () {
            Route::get('all', [DeliveryController::class, 'index'])->name('index');
        });

        Route::group([
            'prefix' => 'optimization-results',
            'as' => 'optimization-results.'
        ], function () {
            Route::get('', [OptimizationController::class, 'index'])->name('index');
        });

        Route::get('download-optimized-images', [ProductController::class, 'downloadOptimizedImages']);

        Route::group([
            'prefix' => 'store-rep',
            'as' => 'store-rep.'
        ], function () {
            Route::get('all', [StoreRepController::class, 'index'])->name('index');
            Route::get('sku', [StoreRepController::class, 'skuPage'])->name('sku');
        });

        Route::group([
            'prefix' => 'chart',
            'as' => 'charts.'
        ], function () {
            Route::group([
                'prefix' => 'store-rep',
                'as' => 'store-rep.'
            ], function () {
                Route::get('order-buys', [ChartController::class, 'storeRepOrderBuys'])->name('storeRepOrderBuys');
            });
        });

        Route::group([
            'prefix' => 'telegram-user',
            'as' => 'telegram-users.'
        ], function () {
            Route::get('telegram-users', [TelegramUserController::class, 'index'])->name('index');
            Route::post('{user}/attach-mailings', [TelegramUserController::class, 'attachMailings'])->name('attach-mailings');
        });

    });

});
Route::get('filter-category', [CategoryController::class, 'filterCategory']);

