<?php

use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\Admin\StoreRepresentativeSettingsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BonusGameController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\StoreRepresentativeController;
use App\Http\Controllers\StoreRepresentativeSettingController;
use App\Http\Controllers\StreetController;
use App\Http\Controllers\TelegramBotController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('me', [AuthController::class, 'me']);
    Route::get('order', [\App\Http\Controllers\Admin\OrderController::class, 'orderRep']);
});

Route::group([
    'prefix' => 'category'
], function () {
    Route::get('/', [CategoryController::class, 'index']);
});

Route::group([
    'prefix' => 'product'
], function () {
    Route::get('/', [ProductController::class, 'index']);
    Route::get('filter-remainder-product', [ProductController::class, 'filterRemainderProduct']);
    Route::get('/all', [ProductController::class, 'allItems']);
});

Route::group([
    'prefix' => 'brands'
], function () {
    Route::get('/', [BrandController::class, 'index']);
});


Route::group([
    'prefix' => 'order',
    'middleware' => 'auth:api'
], function () {
    Route::post('store-for-store', [OrderController::class, 'storeOrderForStore'])->middleware('store-order-request');
    Route::post('store-for-store-multiple', [OrderController::class, 'storeOrderForStoreMultiple']);
    Route::post('update-for-store', [OrderController::class, 'updateOrderForStore']);
    Route::post('delete', [OrderController::class, 'removeOrder']);
    Route::post('{order}/update-basket', [OrderController::class, 'updateBasket']);

});

Route::group([
    'prefix' => 'bonus-game'
], function () {
    Route::post('/', [BonusGameController::class, 'store']);
    Route::get('/', [BonusGameController::class, 'index']);
});

Route::group([
    'prefix' => 'store-representative',
    # 'middleware' => 'auth:api'
], function () {
    Route::get('get-settings', [StoreRepresentativeController::class, 'getSettings']);
});

Route::group([
    'prefix' => 'delivery-order',
    'middleware' => ['auth:api', 'is_driver']
], function () {
    Route::get('/', [DeliveryController::class, 'index']);
    Route::post('{order}/change-status', [DeliveryController::class, 'changeDeliveryOrderStatus']);
    Route::get('get-delivered-orders', [DeliveryController::class, 'getDeliveredOrders']);
});

Route::group([
    'prefix' => 'store-representative-setting'
], function () {
    Route::get('get-setting', [StoreRepresentativeSettingController::class, 'getSetting']);
});

Route::group([
    'prefix' => 'street'
], function () {
    Route::get('search', [StreetController::class, 'index']);
});

Route::group([
    'prefix' => 'store',
//    'middleware' => 'auth:api'
], function () {
    Route::get('/', [StoreController::class, 'index']);
    Route::get('{store}/get-orders', [StoreController::class, 'getOrders']);
    Route::get('/{store}/get-most-popular-products', [StoreController::class, 'getMostPopularProducts']);
});

Route::group([
    'prefix' => 'store-rep-setting',
    'as' => 'store-rep-settings.'
], function () {
    Route::get('allSettings/{user}', [StoreRepresentativeSettingController::class, 'allSettings'])->name('allSettings');
});


Route::group([
    'prefix' => 'street'
], function () {
    Route::get('search', [StreetController::class, 'index']);
});

Route::group([
    'prefix' => 'telegram-bot'
], function () {
    Route::post('upload-product-remainders-file', [\App\Http\Controllers\Admin\ProductController::class, 'updateProductRemaindersFromExcel']);
    Route::post('launch-product-remainders-update-job', [\App\Http\Controllers\TelegramBotController::class, 'startProductRemaindersUpdateJob']);
    Route::post('getUpdate', [\App\Http\Controllers\TelegramBotController::class, 'getUpdate']);
    Route::post('send-store-info-message', [\App\Http\Controllers\TelegramBotController::class, 'sendOfficialStoreMessage']);
    Route::post('driver-send-message', [TelegramBotController::class, 'driverSendPhoneNumberAndOrderCostMessage']);
});
