<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counterparty extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = ['id'];

    public static function generateId1C()
    {
        $last = self::orderBy('id_1c', 'desc')->first();

        return $last->id_1c + 1;
    }

}
