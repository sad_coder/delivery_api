<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = [
        'id'
    ];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
