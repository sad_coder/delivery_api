<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreRepresentativeSetting extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $casts = [
        'data' => 'array'
    ];

    protected $guarded = [
      'id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
