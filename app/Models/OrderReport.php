<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class OrderReport extends Model
{
    use HasFactory, ModelTableNameTrait;

    const PATH = 'reports/';

    const PATH_1C_PROCESSOR = '/home/dev-pd/index';

    const STATUS_GENERATED = 1;
    const STATUS_GENERATION_FAILED = 2;
    const STATUS_SENT_TO_1C = 3;
    const STATUS_FAILED_SENDING_TO_1C = 4;
    const STATUS_MOVED_TO_LOCAL_1C_PROCESSOR = 5;
    const STATUS_FAILED_MOVING_TO_LOCAL_1C_PROCESSOR = 6;

    const REPORT_TYPE_ORDER = 0;
    const REPORT_TYPE_RETURN = 1;
    const REPORT_TYPE_WAYBILL = 2;


    const REPORTS_TYPES = [
        self::REPORT_TYPE_ORDER => "ORDER",
        self::REPORT_TYPE_RETURN => "RETURN",
        self::REPORT_TYPE_WAYBILL => "WAYBILL",
    ];

    const DOCUMENT_NAMES_1C = [
        self::REPORT_TYPE_ORDER => 220,
        self::REPORT_TYPE_RETURN => 320,
        self::REPORT_TYPE_WAYBILL => 420,
    ];


    protected $guarded = [
        'id'
    ];

    public $timestamps = false;

    public $orderType = '';

    public $sendingStatus = '';

    public $moveStatus = '';

    public static function generate(Order $order, string $date, $type, $rootPath = self::PATH_1C_PROCESSOR)
    {
        $name = self::generateReportName($order, $type);
        $path = self::PATH . "$date/$name";

        try {
            $output = View::make('admin.orders.report_template')->with(compact('order', 'type'))->render();
            $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" . $output;

            //storing in local storage
            Storage::disk('public')->put($path, $output);

            //storing in 1c processor folder
            $client = Storage::createLocalDriver(['root' => $rootPath]);
            $client->put($name, $output);

        } catch (Exception $e) {
            self::query()->create([
                'order_id' => $order->id,
                'path' => $path,
                'status' => self::STATUS_GENERATION_FAILED,
                'created_at' => $date,
                'type' => $type
            ]);

            throw new Exception($e->getMessage());
        }

        self::query()->create([
            'order_id' => $order->id,
            'path' => $path,
            'status' => self::STATUS_GENERATED,
            'created_at' => $date,
            'type' => $type
        ]);

        return $path;
    }

    public function sendTo1C($report)
    {
        $file = Storage::disk('public')->get($report->path);
        $success = Storage::disk('ftp-1c')->put("index/" . basename($report->path), $file);

        if ($success) {
            $this->sendingStatus = 'success';
        } else {
            $this->sendingStatus = 'failed';
        }
    }

    public function getNameAttribute()
    {
        $arr = explode('/', $this->path);

        return $arr[3];
    }

    public static function generateReportName($order, int $type)
    {
        $head = self::REPORTS_TYPES[$type];
        $todayDateFor1C = now()->format('YmdHis');
        return $head . "_$todayDateFor1C"."_{$order->user->counterparty->id_1c}_9864232489962_{$order->id}" . ".xml";
    }

    public function determineOrder($order)
    {
        $totalCount = 0;
        $returnsCount = 0;

        foreach ($order->basket as $product) {
            if ($product['type'] == Order::BASKET_PRODUCT_TYPE_FOR_RETURN) {
                $returnsCount++;
            }
            $totalCount++;
        }

        if ($returnsCount === $totalCount) {
            $this->orderType = 'return';
        } else {
            $this->orderType = 'order';
        }
    }
}
