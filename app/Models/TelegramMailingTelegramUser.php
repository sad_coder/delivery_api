<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramMailingTelegramUser extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $table = 'telegram_mailing_telegram_user';

    protected $guarded = [
      'id'
    ];
}
