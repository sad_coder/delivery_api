<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use App\Traits\SearchableByCommonCols;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BonusGame extends Model
{
    use HasFactory, ModelTableNameTrait, SoftDeletes, SearchableByCommonCols;

    protected $guarded = [
        'id'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
