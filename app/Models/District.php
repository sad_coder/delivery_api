<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'coordinates' => 'array'
    ];

    protected $fillable = [
        'coordinates',
        'user_id'
    ];

    use HasFactory, ModelTableNameTrait;
}
