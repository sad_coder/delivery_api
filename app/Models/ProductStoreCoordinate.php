<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductStoreCoordinate extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $casts = [
        'basket' => 'array'
    ];

    public function productCoordinate()
    {
        $productIds = array_column($this->basket, 'product_id');

        $products = Product::whereIn('id', $productIds)->get();

        return $products;
    }
}

