<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramUser extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function token()
    {
        return $this->hasOne(TelegramBotAuthToken::class);
    }

    public function telegramMailings()
    {
        return $this->belongsToMany(TelegramMailing::class);
    }

    public static function getRelatedUserIds(string $slug)
    {
        $userIds = self::query()
            ->whereHas('telegramMailings', function ($q) use ($slug) {
                $q->where('slug', $slug);
            })
            ->pluck('chat_id')
            ->toArray();

        return implode(' ', $userIds);
    }
}
