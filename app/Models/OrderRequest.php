<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class OrderRequest extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'payload' => 'array',
        'failed_at' => 'datetime'
    ];

    public static function handle($mobileId, $payload)
    {
        if (!self::where('mobile_id', $mobileId)->exists()) {
            self::query()->create([
                'mobile_id' => $mobileId,
                'user_id' => Auth::id(),
                'payload' => ($payload)
            ]);
        }
    }

    public static function failed($mobile_id, $exceptopn)
    {
        $order = self::where('mobile_id', $mobile_id)
            ->first();

        $order->update([
            'exception' => $exceptopn,
            'failed_at' => now(),
            'is_processed' => false
        ]);

        $message = "Заказ $order->id потерпел неудачу при создании\nТекст ошибки: $exceptopn";

        $userIds = TelegramUser::getRelatedUserIds('order_request_error');

        if ($userIds)
            Artisan::call("tgbot:send:message $userIds --message='$message'");
    }

    public static function processed($mobileId)
    {
        self::where('mobile_id', $mobileId)
            ->update([
                'is_processed' => true
            ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getFormattedPayloadAttribute()
    {
        if (is_array($this->payload)) {
            return $this->payload;
        }

        return json_decode($this->payload, true);
    }
}
