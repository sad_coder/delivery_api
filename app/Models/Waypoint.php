<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class Waypoint extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $casts = [
      'data' => 'array'
    ];

    protected $guarded = [
        'id'
    ];

    public static function storeNewInstance($data)
    {
        $waypoint = new self();
        $waypoint->order_id = $data['order_id'];
        $waypoint->roadmap_id = $data['roadmap_id'];
        $waypoint->data = json_encode($data['data']);
        $waypoint->distance = 1;
        $waypoint->save();

        return $waypoint;
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }


    public function roadmap()
    {
        return $this->belongsTo(Roadmap::class);
    }
}
