<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
    use HasFactory, ModelTableNameTrait;

    const STATUS_TAKEN = 0;
    const STATUS_TAKEN_BY_DRIVER = 1;
    const STATUS_PROCESSED = 2;

    protected $guarded = [
      'id'
    ];

    public function scopeOnlyChangeableByStatus($query)
    {
        return $query->where('status', self::STATUS_TAKEN);
    }
}
