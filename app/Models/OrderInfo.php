<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTableNameTrait;

class OrderInfo extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = [
        'id'
    ];
}
