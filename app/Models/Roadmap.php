<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roadmap extends Model
{
    use HasFactory, ModelTableNameTrait;

    const CUSTOM_TIME_FORMAT = '%H часов : %i минут';

    protected $guarded = [
        'id'
    ];

    public function waypoints()
    {
        return $this->hasMany(Waypoint::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDeliveryDuration($orderId)
    {
        if(!Order::find($orderId)->delivered_at) {
            return '-';
        }

        $waypoints = $this->waypoints()
            ->join('orders', 'orders.id','=','waypoints.order_id')
            ->orderBy('orders.delivered_at', 'ASC')
            ->get();

        if ($waypoints[0]->order_id == $orderId) {
            return 'Первый заказ не просчитывается';
        }

        $timeDuration = null;

        for ($i = 1; $i < count($waypoints); $i++) {
            if ($waypoints[$i]->order_id == $orderId) {
                if ($waypoints[$i - 1]->order->delivered_at) {
                    $timeDuration = $waypoints[$i]->order->delivered_at
                        ->diff($waypoints[$i - 1]->order->delivered_at)
                        ->format(self::CUSTOM_TIME_FORMAT);
                } else {
                    $wp = $waypoints[$i];

                    $closestDeliveryTimeOrder = $this->waypoints()->whereHas('order', function ($q) use ($wp) {
                        $q->whereNotNull('delivered_at')
                            ->where('delivered_at', '<', $wp->delivered_at);
                    })->first();

                    if ($closestDeliveryTimeOrder) {
                        $timeDuration = $wp->delivered_at->diff($closestDeliveryTimeOrder->delivered_at)
                        ->format(self::CUSTOM_TIME_FORMAT);
                    } else {
                        $timeDuration = null;
                    }
                }
            }
        }
        return $timeDuration;
    }
}
