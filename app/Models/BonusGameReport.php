<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusGameReport extends Model
{
    use HasFactory, ModelTableNameTrait;

    const STATUS_GENERATED = 1;
    const STATUS_GENERATION_FAILED = 2;
    const STATUS_SENT_TO_MAIL = 3;

    protected $guarded = [
      'id'
    ];
}
