<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, ModelTableNameTrait;

    const ROLES = [
        [
            'description' => 'Торговый представитель',
            'id' => self::ROLE_REPRESENTATIVE
        ],
        [
            'description' => 'Водитель',
            'id' => self::ROLE_DRIVER
        ],
        [
            'description' => 'Админ',
            'id' => self::ROLE_ADMIN
        ]
    ];

    const ROLE_REPRESENTATIVE = 1;
    const ROLE_DRIVER = 2;
    const ROLE_ADMIN = 3;

    const ALLOWED_ROLES = [
        self::ROLE_REPRESENTATIVE,
        self::ROLE_DRIVER,
        self::ROLE_ADMIN
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'surname',
        'patronymic',
        'email',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function storeRepresentativeSettings()
    {
        return $this->belongsToMany(StoreRepresentativeSetting::class, 'store_representative_setting_user', 'user_id', 'store_representative_setting_id', 'id', 'id');
    }

    public function deliveryOrders()
    {
        return $this->hasMany(DeliveryOrder::class, 'driver_id', 'id');
    }

    public function isDriver()
    {
        return $this->role == self::ROLE_DRIVER;
    }

    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function scopeDrivers($query)
    {
        return $query->where('role', self::ROLE_DRIVER);
    }

    public function roadmaps()
    {
        return $this->hasMany(Roadmap::class);
    }

    public function waypoints()
    {
        return $this->hasManyThrough(Waypoint::class, Roadmap::class);
    }

    public function counterparty()
    {
        return $this->hasOne(Counterparty::class);
    }

    public function storeRepresentatives()
    {
        return $this->hasManyThrough(User::class, DriverStoreRepresentative::class, 'driver_id', 'id', 'id', 'store_representative_id');
    }

    public function driver()
    {
        return $this->hasOneThrough(User::class, DriverStoreRepresentative::class, 'store_representative_id', 'id', 'id', 'driver_id');
    }

    public function driverOrders()
    {
        return $this->hasManyThrough(Order::class, DriverStoreRepresentative::class, 'driver_id', 'user_id', 'id', 'store_representative_id');
    }

    public function bonusGames()
    {
        return $this->hasManyThrough(BonusGame::class, Order::class, 'user_id', 'mobile_id', 'id', 'mobile_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function scopeStoreReps($query)
    {
        return $query->where('role', self::ROLE_REPRESENTATIVE);
    }
}
