<?php

namespace App\Models;

use App\Traits\SearchableByCommonCols;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Order extends Model implements Auditable
{
    use HasFactory, ModelTableNameTrait, SoftDeletes, \OwenIt\Auditing\Auditable, SearchableByCommonCols;

    const ALL_STATUSES = [
        [
            'id' => 1,
            'description' => 'Готово к отправке'
        ],
        [
            'id' => 2,
            'description' => 'Доставляется'
        ],
        [
            'id' => 3,
            'description' => 'Доставлено'
        ],
        [
            'id' => 4,
            'description' => 'Неизвестно'
        ]
    ];

    const STATUS_READY_FOR_DELIVERY = 1;
    const STATUS_ON_DELIVERY = 2;
    const STATUS_DELIVERED = 3;

    const BASKET_PRODUCT_TYPE_FOR_PURCHASE = 0;
    const BASKET_PRODUCT_TYPE_FOR_RETURN = 1;

    protected $auditInclude = [
        'basket',
        'status'
    ];

    protected $casts = [
        'basket' => 'array',
        'address' => 'array',
        'delivered_at' => 'datetime: Y-m-d H:i:s'
    ];

    protected $guarded = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function getBasketProducts()
    {
        $productIds = array_column($this->basket, 'product_id');

        $products = Product::whereIn('id', $productIds)->get();

        return $products;
    }

    public function getProducts()
    {
        $productIds = array_column($this->basket, 'product_id');

        $products = Product::whereIn('id', $productIds)->get();

        return $products;
    }

    public function mergeBasketWithProducts()
    {
        $products = $this->getProducts();

        $mergedProducts = collect([]);
        foreach ($this->basket as $item) {
            $product = $products->where('id', $item['product_id'])->first();
            foreach ($item as $key => $value) {
                $product[$key] = $value;
                $product['type'] = (int)$item['type'];
            }
            if (is_object($product)) {
                $mergedProducts->push(clone $product);
            }
        }

        return $mergedProducts;
    }

    public function mergeBasketWithProductsReturn()
    {
        $products = $this->getProducts();
        foreach ($products as $product) {
            foreach ($this->basket as $item) {
                if ($product->id == $item['product_id'] && $item['type'] == 1) {
                    foreach ($item as $key => $value) {
                        $product[$key] = $value;
                    }
                }
            }
        }
        return $products;
    }


    public function scopeForDelivery($query)
    {
        return $query->where('status', 1);
    }

    public function scopeDelivered($query)
    {
        return $query->where('status', 3);
    }

    public function getTotalCostAttribute()
    {
        $totalCost = 0;

        foreach ($this->mergeBasketWithProducts() as $product) {
            $type = $product->type ?? null;
            if ($type !== 1) {
                $totalCost += $product->count * $product->price;
            }
        }

        return $totalCost;
    }

    public function getProductBasketCountAttribute()
    {
        return count($this->mergeBasketWithProducts());
    }


    public function calculateBasketCost($basket)
    {
        $totalCost = 0;

        foreach ($basket as $item) {
            $product = Product::find($item['product_id']);
            $totalCost += $product->price * $item['count'];
        }

        return $totalCost;
    }

    public function report()
    {
        return $this->hasOne(OrderReport::class);
    }

    public function productsWaybills()
    {
        return $this->hasMany(ProductWaybill::class, 'order_id', 'id');
    }

    public function storeId()
    {
        return $this->belongsTo(Store::class);
    }

    public function bonusGames()
    {
        return $this->hasMany(BonusGame::class, 'mobile_id', 'mobile_id');
    }

    public function bonusGamesReport()
    {
        return $this->hasOne(BonusGameReport::class);
    }

    public function getTotalReturnsCostAttribute()
    {
        $products = $this->mergeBasketWithProducts()
            ->where('type', 1);

        $totalCost = 0;

        foreach ($products as $product) {
            $totalCost += $product->count * $product->price;
        }

        return $totalCost;
    }

    public function productWaybill()
    {
        return $this->hasOne(ProductWaybill::class);
    }

    public static function generateStoreRepOrderBuysStatistics($dateFrom, $dateTo, $userId)
    {
        $orders = self::where('user_id', $userId)
            ->whereBetweenDates($dateFrom, $dateTo)
            ->select('id', 'created_at', 'basket')
            ->orderBy('created_at')
            ->get();

        foreach ($orders as $order) {
            $order->gb = $order->created_at->format('Y-m-d');
        }
        $orders = $orders->groupBy('gb');

        $data = [];
        foreach ($orders as $d => $value) {
            $cost = 0;
            foreach ($value as $lul) {
                $cost += $lul->total_cost;
            }
            $data[] = ['x' => $d, 'y' => $cost];
        }

        $user = User::find($userId);

        $rand1 = rand(1, 255);
        $rand2 = rand(1, 255);
        $rand3 = rand(1, 255);

        return [
            'data' => $data,
            'backgroundColor' => "rgb($rand1, $rand2, $rand3)",
            'borderColor' => "rgb($rand1, $rand2, $rand3)",
            'label' => $user->full_name
        ];
    }
}
