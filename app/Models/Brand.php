<?php

namespace App\Models;

use App\Scopes\EnabledScope;
use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected static function booted()
    {
        static::addGlobalScope(new EnabledScope());
    }

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'name',
        'active'
    ];

    public function productsEnabled()
    {
        return $this->hasMany(Product::class,'brand_id','id')->where('enabled',true);
    }



}
