<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Measure extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = [
      'id'
    ];

    protected $fillable = [
      'name',
      'name_1c'
    ];
}
