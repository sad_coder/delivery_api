<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory, ModelTableNameTrait;

    const MOST_POPULAR_PRODUCTS_LENGTH = 5;

    protected $guarded = [
        'id'
    ];

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id', 'id');
    }

    public function bonusGame()
    {
        return $this->hasMany(BonusGame::class, 'store_id', 'id');
    }

    public function getCustomIdOnecAttribute()
    {
        return $this->id_once ?? sprintf("3%014d", $this->id);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getTotalCostOfOrdersAttribute()
    {
        return $this->orders->reduce(function ($total, $order) {
            return $total + $order->total_cost;
        }, 0);
    }

    public function getMostPopularProductsAttribute()
    {
        $orders = $this->orders()
            ->select('id', 'basket')
            ->get();

        $productIdCount = [];

        foreach ($orders as $order) {
            foreach ($order->basket as $item) {
                if (isset($productIdCount[$item['product_id']]))
                    $productIdCount[$item['product_id']] += $item['count'];
                else
                    $productIdCount[$item['product_id']] = $item['count'];
            }
        }

        arsort($productIdCount);

        $finalArray = array_slice($productIdCount, 0, self::MOST_POPULAR_PRODUCTS_LENGTH, true);

        $productsId = array_keys($finalArray);

        $products = Product::whereIn('id', $productsId)
            ->select('id', 'presale_id', 'name_1c', 'price')
            ->get();

        foreach ($products as $product) {
            foreach ($finalArray as $key => $value) {
                if ($key == $product->id) {
                    $product['times_bought'] = $value;
                }
            }
        }

        return $products->sortByDesc('times_bought');
    }
}
