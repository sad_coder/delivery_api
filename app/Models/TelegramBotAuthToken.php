<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramBotAuthToken extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected $guarded = [
      'id'
    ];
}
