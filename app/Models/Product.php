<?php

namespace App\Models;

use App\Scopes\EnabledScope;
use App\Scopes\Product\HasPriceScope;
use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory, ModelTableNameTrait;

    const FIELDS_TRANSLATION = [
        'name_1c' => 'Наименование',
        'price' => 'Цена',
        ''
    ];

    const REMAINDERS_UPDATE_FILE_NAME = 'inventorization.xls';

    protected static function booted()
    {
        static::addGlobalScope(new EnabledScope());
        static::addGlobalScope(new HasPriceScope());
    }

    protected $guarded = [
        'id',
        'id_1c',
        'name_1c'
    ];

    protected $fillable = [
        'id_1c',
        'article',
        'brand_id',
        'category_id',
        'measure_id',
        'presale_id',
        'name_1c',
        'name',
        'price',
        'description',
        'remainder',
        'enabled',
        'image',
        'audio'
    ];

    public function getFullImagePathAttribute()
    {
        return $this->image ? asset('storage/photo/' . $this->image) : null;
    }

    public function getFullAudioPathAttribute()
    {
        return $this->audio ? asset('storage/audio/' . $this->audio) : null;
    }

    public function getOptimizedImagePathAttribute()
    {
        return $this->image_optimized ? asset('storage/photo-optimized/' . $this->image_optimized) : null;
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function measure()
    {
        return $this->belongsTo(Measure::class);
    }

    public function getMeasureCodeAttribute()
    {
        return $this->measure ? ($this->measure->name_1c == 'кг.' ? 'KGM' : 'PCE') : 'PCE';
    }
}
