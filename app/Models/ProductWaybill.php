<?php

namespace App\Models;

use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductWaybill extends Model
{
    use ModelTableNameTrait, HasFactory;

    const NOT_SEND_TO_BOOKKEEPER = 0;
    const SEND_TO_BOOKKEEPER = 1;

    protected $guarded = ['id'];
}
