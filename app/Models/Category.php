<?php

namespace App\Models;

use App\Scopes\EnabledScope;
use App\Traits\ModelTableNameTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, ModelTableNameTrait;

    protected static function booted()
    {
        static::addGlobalScope(new EnabledScope());
    }

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'name_1c',
        'name',
        'brand_id'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function productsEnabled()
    {
        return $this->hasMany(Product::class,'category_id','id')->where('enabled',true);
    }
}
