<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use App\Scopes\EnabledScope;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class GenerateResizedOptimizedImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resize:images {width=300} {height=200}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::withoutGlobalScope(EnabledScope::class)
            ->whereNotNull('image_optimized')
            ->whereNull('image_resized')
            ->get();

        $totalCount = 0;

        foreach ($products as $product) {
            $path = Storage::disk('public')->path("photo-optimized/{$product->image_optimized}");

            $outputDir = Storage::disk('public')->path("photo-resized/");

            if (!is_dir($outputDir)) {
                mkdir($outputDir);
            }

            $outputPath = Storage::disk('public')->path("photo-resized/{$product->image_optimized}");

            Image::load($path)
                ->fit(Manipulations::FIT_STRETCH, $this->argument('width'), $this->argument('height'))
                ->save($outputPath);


            $product->image_resized = $product->image_optimized;
            $product->save();

            $totalCount++;
            $this->info("Product with id $product->id now has resized image");
        }

        $this->info("All product images resized, total count is: $totalCount");
    }
}
