<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class UpdateProductsRemainderFromExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:products:remainder {path?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update products remainder field from new excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->argument('path');

        if (!$path) {
            $path = Storage::disk('storage')->path("inventorization/" . Product::REMAINDERS_UPDATE_FILE_NAME);
        }

        if (!is_file($path)) {
            $this->info('The file from given path does not exist');
            throw new Exception("Error in UPDATEPRODUCTSREMAINEDFROMEXCELCOMMAND path does not exist: $path");
        }

        try {
            $spreadsheet = IOFactory::createReader('Xls');
            $spreadsheet->setReadDataOnly(true);
            $spreadsheet = $spreadsheet->load($path)->getActiveSheet();
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            $spreadsheet = IOFactory::createReader('Xlsx');
            $spreadsheet->setReadDataOnly(true);
            $spreadsheet = $spreadsheet->load($path)->getActiveSheet();
        }

        $highestRow = $spreadsheet->getHighestRow('F');
        $products = Product::all();

        ini_set('max_execution_time', 10000);

        for ($i = 2; $i <= $highestRow; $i++) {
            $presaleId = $spreadsheet->getCell('F' . $i)->getCalculatedValue();
            if ($presaleId && $presaleId !== "#N/A") {
                $product = $products->where('presale_id', $presaleId)->first();
                $this->info("$presaleId - presale id found");
                if ($product) {
                    $this->info("$product->name_1c starting parsing, cur value is $product->remainder");
                    $remainder = $spreadsheet->getCell('E' . $i)->getValue();
                    $this->info("new remainder is $remainder");
                    if ($remainder) {
                        $product->remainder = $remainder;
                        $product->save();
                        $this->info("updated.");
                    }
                }
            }
        }
        $this->info('The products remainder were updated!');

        return 0;
    }
}
