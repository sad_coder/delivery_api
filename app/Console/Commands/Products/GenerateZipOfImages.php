<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use App\Scopes\EnabledScope;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class GenerateZipOfImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zip:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate zip of images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::withoutGlobalScope(EnabledScope::class)
            ->whereNotNull('image_resized')
            ->get();

        $dir = Storage::disk('public')->path('zips/');

        if (!is_dir($dir)) {
            mkdir($dir);
        }

        try {
            $counter = 0;

            $zip = new ZipArchive();
            $zipName = 'optimizedImages.zip';
            $absolutePath = Storage::disk('public')->path('zips');
            $path = $absolutePath . '/' . $zipName;

            if ($zip->open($path, ZipArchive::CREATE) === TRUE) {
                foreach ($products as $product) {
                    $zip->addFile(Storage::disk('public')->path("photo-resized/" . $product->image_resized), $product->image);
                    $counter++;
                }
                $zip->close();
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $this->info("Zip $zipName generated!, total images count is: ");
        return 0;
    }
}
