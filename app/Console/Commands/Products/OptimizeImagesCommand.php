<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class OptimizeImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optimize:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create optimized version of all products images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $products = Product::whereNull('image_optimized')
            ->whereNotNull('image')
            ->get();

        $counter = 0;


        foreach ($products as $product) {
            $optimizedPathDir = Storage::disk('public')->path('photo-optimized/');

            if (!is_dir($optimizedPathDir)) {
                mkdir($optimizedPathDir);
            }

            $optimizedPath = $optimizedPathDir . $product->image;

            ImageOptimizer::optimize(
                Storage::disk('public')->path('photo/' . $product->image),
                $optimizedPath
            );

            $product->image_optimized = $product->image;
            $product->save();
            $counter++;
            $this->info($product->id. 'saved');
        }

        $this->info("Total optimized images count is $counter");

        return 0;
    }
}
