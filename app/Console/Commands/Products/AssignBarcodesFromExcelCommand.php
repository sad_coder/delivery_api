<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;

class AssignBarcodesFromExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attach:product:barcodes {path?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach barcodes from excel to products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->argument('path');

        if ($path && !is_file($path)) {
            $this->error('The provided path is invalid');
        } else {
            $this->info('Trying to get default path');
            $path = Storage::path('barcodes.xls');
            if (!is_file($path)) {
                $this->error("Not found");
                return 0;
            }
        }

        $spreadsheet = IOFactory::createReader('Xls');
        $spreadsheet->setReadDataOnly(true);
        $spreadsheet = $spreadsheet->load($path)->getActiveSheet();
        $highestRow = $spreadsheet->getHighestRow('C');
        $count = 0;

        for ($i = 7; $i <= $highestRow; $i++) {
            $name = $spreadsheet->getCell('C' . $i)->getValue();
            $measure = $spreadsheet->getCell('D' . $i)->getValue();

            $product = Product::where('name_1c', $name)->first();

            if ($product) {
                if ($measure == 'кг.') {
                    $barcode = $spreadsheet->getCell('B' . $i)->getValue();
                } else {
                    $barcode = $spreadsheet->getCell('A' . $i)->getValue();
                }

                $product->barcode = $barcode;
                $product->save();
            } else {
                $count++;
                $this->error("$name не найден в нашей базе данных");
            }
        }

        $registered = $highestRow - 6 - $count;

        $this->info("$count products are not registered in our database and $registered products have been attached barcodes");
    }
}

