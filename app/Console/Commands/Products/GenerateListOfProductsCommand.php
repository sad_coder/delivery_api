<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateListOfProductsCommand extends Command
{
    const EXCEL_HEADERS = [
        'ID',
        'ID 1c',
        'Наименование'
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate list of all products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::all();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        for ($i = 0; $i < count(self::EXCEL_HEADERS); $i++) {
            $sheet->setCellValueByColumnAndRow($i + 1, 1, self::EXCEL_HEADERS[$i]);
        }

        $j = 2;
        $length = count($products);
        for ($i = 0; $i < $length; $i++) {
            $k = 1;
            $sheet->setCellValueByColumnAndRow($k, $j, $products[$i]->id);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, $products[$i]->id_1c);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, $products[$i]->name_1c);
            $j++;
        }

        $filename = now()->format('Y_m_d') . "_products_list.xlsx";

        $writer = new Xlsx($spreadsheet);
        $writer->save(base_path() . "/storage/app/public/" . $filename);

        $this->info('Products parsed successfully');
    }
}
