<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateImageExcelCommand extends Command
{

    const EXCEL_HEADERS = [
        'id',
        'id 1c',
        'название'
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:no-image-products {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate image in excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::all();
        $filename = now()->format('Y_m_d') . "_products_list.xlsx";

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        for ($i = 0; $i < count(self::EXCEL_HEADERS); $i++) {
            $sheet->setCellValueByColumnAndRow($i + 1, 1, self::EXCEL_HEADERS[$i]);
        }

        $j = 2;
        for ($i = 0; $i < count($products); $i++) {
            if ($products[$i]->image == null) {
                $k = 1;
                $sheet->setCellValueByColumnAndRow($k, $j, $products[$i]->id);
                $k++;
                $sheet->setCellValueByColumnAndRow($k, $j, $products[$i]->id_1c);
                $k++;
                $sheet->setCellValueByColumnAndRow($k, $j, $products[$i]->name_1c);
                $j++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($this->argument('path') .'/'. $filename);
    }
}
