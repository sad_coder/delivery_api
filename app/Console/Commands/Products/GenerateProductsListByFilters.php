<?php

namespace App\Console\Commands\Products;

use App\Models\Product;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateProductsListByFilters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "generate:products:list {headers*} {--condition=*}";

    /**
     * The console command description.php artisan generate:products:list id name_1c price --condition=presale_id,like,5%

     *
     * @var string
     */
    protected $description = 'Generate list of products based on conditions and options given in parameters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $headers = $this->argument('headers');
        $headersLength = count($headers);

        $products = $this->dofFilter($this->option('condition'));

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        for ($i = 0; $i < $headersLength; $i++) {
            $sheet->setCellValueByColumnAndRow($i + 1, 1, Product::FIELDS_TRANSLATION[$headers[$i]] ?? $headers[$i]);
        }

        $j = 2;

        $name = "products_list_" . now()->format('Y-m-d') . ".xlsx";
        $path = base_path() . "/storage/app/public/temp/" . $name;

        foreach ($products as $product) {
            $k = 1;
            foreach ($headers as $header) {
                $sheet->setCellValueByColumnAndRow($k, $j, $product->$header);
                $k++;
            }
            $j++;
        }

        if (!is_dir(base_path() . "/storage/app/public/temp/")) {
            mkdir(base_path() . "/storage/app/public/temp/");
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($path);

        $this->info($path);
    }

    public function dofFilter($conditions)
    {
        $query = Product::query();

        foreach ($conditions as $condition) {
            $cnd = explode(',', $condition);

            $query->where($cnd[0], $cnd[1], $cnd[2]);
        }

        return $query->get();
    }
}
