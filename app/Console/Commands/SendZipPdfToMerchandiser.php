<?php

namespace App\Console\Commands;

use App\Mail\MyTestMail;
use App\Models\ProductWaybill;
use App\Models\ZipWaybill;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class SendZipPdfToMerchandiser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail:bookkeeper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $zips = ZipWaybill::query()
            ->where('status', ProductWaybill::NOT_SEND_TO_BOOKKEEPER)
            ->get();

        $date = Carbon::now()->format('Y-m-d');

        $emails = [
            'gebsss0@gmail.com',
            'gebsss29@gmail.com',
            'samen@Foodkz.kz',
        ];

        foreach ($zips as $zip) {
            Mail::send('emails.myTestMail', ['date' => $date], function ($message) use ($zip, $emails) {
                $message->to($emails)
                    ->attach(storage_path('zips/' . $zip->name), [
                        'as' => $zip->name,
                        'mime' => 'application/zip',
                    ])
                    ->subject('sad coder test');

            });

            $zip->status = ProductWaybill::SEND_TO_BOOKKEEPER;
            $zip->save();
        }

        $this->info('Отчет отправлен по почтам');
    }
}
