<?php

namespace App\Console\Commands\Database;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class DoBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "db:backup {--path=/home/dev-pd/dumps/}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Do backups/dump file of a database, provide --path=default so backup will be in storage app folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $process;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->option('path');

        if ($path == 'default') {
            $path = base_path(). '/storage/';
        }

        $name = sprintf($path . 'backup_%s.sql', now()->format('YmdHis'));

        $this->process = Process::fromShellCommandline(
            sprintf(
                'PGPASSWORD="%s" pg_dump -U %s -h %s -p %s %s >> %s',
                config('database.connections.pgsql.password'),
                config('database.connections.pgsql.username'),
                config('database.connections.pgsql.host'),
                config('database.connections.pgsql.port'),
                config('database.connections.pgsql.database'),
                ($name)
            )
        );

        try {
            $this->info('The backup has been started');
            $this->process->mustRun();
            $this->info('The backup has been proceed successfully.');
            logger('test cron');
        } catch (ProcessFailedException $exception) {
            logger()->error('Backup exception', compact('exception'));
            $this->error('The backup process has been failed.');
        }
    }
}
