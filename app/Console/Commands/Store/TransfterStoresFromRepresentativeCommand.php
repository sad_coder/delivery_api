<?php

namespace App\Console\Commands\Store;

use App\Models\Store;
use Illuminate\Console\Command;

class TransfterStoresFromRepresentativeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:transfer {fromId} {toId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer stores from store rep to another';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fromId = $this->argument('fromId');
        $toId = $this->argument('toId');

        Store::where('seller_id', $fromId)
            ->update([
                'seller_id' => $toId
            ]);

        $this->info('Successfully updated');

        return 1;
    }
}
