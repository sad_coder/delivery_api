<?php

namespace App\Console\Commands\Store;

use App\Models\TelegramUser;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class SendReportExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:store:report {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send store report to bookkeeper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filepath = $this->argument('path');
//        $emails = explode(',', env('BOOKKEEPER_MAIL', 'gebsss0@gmail.com'));


        $emails = [
            'gebsss0@gmail.com',
            'khokhloval@foodkz.kz',
            'ivanova@Foodkz.kz',
            'kutanovam@Foodkz.kz',
            'Makpal_2304@mail.ru'
        ];

        $message = 'Во вложении предоставлен отчет о торговых точках';

        try {
            Mail::send('emails.simple_mail', ['text' => $message], function ($message) use ($filepath, $emails) {
                $message->to($emails);
                $message->subject('Отчет о торговых точках');
                $message->attach($filepath);
            });
        } catch (Exception $e) {
            logger($e->getMessage());
        }

        try {
            $userIds = TelegramUser::getRelatedUserIds('store_report');
            if ($userIds)
                Artisan::call("tgbot:send:message $userIds --message='$message' --filepath=$filepath");
        } catch (Exception $e) {
            logger($e->getMessage());
        }

    }
}
