<?php

namespace App\Console\Commands\Store;

use App\Models\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateReportExcelCommand extends Command
{
    const EXCEL_HEADERS = [
        'id',
        'Представитель',
        'Адрес',
        'Номер телефона',
        'Название',
        'БИН'
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate an excel with a list of stores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        Store::query()->whereHas('seller.counterparty')->where('id',">=",193)->update(['id_onec' => null]);
//        dd("Зачистка точек которые давали в ручную");
        $stores = Store::query()->whereHas('seller.counterparty')->where('id_onec', null)->get();

        $filename = now()->format('Y_m_d') . "_stores_list.xlsx";

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        for ($i = 0; $i < count(self::EXCEL_HEADERS); $i++) {
            $sheet->setCellValueByColumnAndRow($i + 1, 1, self::EXCEL_HEADERS[$i]);
        }

        $j = 2;
        for ($i = 0; $i < count($stores); $i++) {
            $k = 1;
            $sheet->setCellValueExplicitByColumnAndRow($k, $j, $stores[$i]->custom_id_onec, DataType::TYPE_STRING);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, $stores[$i]->seller->full_name);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, $stores[$i]->address);
            $k++;
            $sheet->setCellValueExplicitByColumnAndRow($k, $j, $stores[$i]->phone, DataType::TYPE_STRING);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, $stores[$i]->name . " ({$stores[$i]->id})");
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, "");
            $j++;
            $stores[$i]->update(['id_onec' => $stores[$i]->custom_id_onec]);
            $stores[$i]->save();
        }

        $writer = new Xlsx($spreadsheet);
        $path = base_path() . "/storage/app/public/" . $filename;
        $writer->save($path);

        $this->info('Отчет сгенерирован.');
        $this->info("Доступен на сервере по адресу $path");

        Artisan::call('send:store:report', [
            'path' => $path
        ]);

        $this->info('Отчет был отправлен!');
    }
}
