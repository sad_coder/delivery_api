<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Scopes\EnabledScope;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ParseActual1CProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:actual:1c';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Product::query()->withoutGlobalScope(EnabledScope::class)->update(['enabled' => false]);

        $products = Product::query()->withoutGlobalScope(EnabledScope::class)->get();


        $spreadsheet = IOFactory::load(storage_path('catalogBc.xlsx'));
        $sheets = $spreadsheet->getActiveSheet();
        $i = 0;
        $productsToInsert = [];
        foreach ($sheets->toArray() as $k => $sheet) {
            if ($sheet[2] != null && $k > 2) {

                $product = $products->where('id_1c', $sheet[0])->first();
                //@TODO дичь для превращения float в integer потом у нас изменить на float
                $price = preg_replace('#\,|\.#','',substr($sheet[2],0,-2));
                if ($product) {
                    ++$i;
                    $product->enabled = true;
                    $product->price = $price;
                    $product->save();
                } else {
                    ++$i;
                    $productsToInsert[$k] = [
                        "id_1c" => $sheet[1],
                        "name_1c" => $sheet[2],
                        "price" => $price,
                    ];

                }

            }
        }
        Product::query()->insert($productsToInsert);
        $this->info("загружено $i товаров");
        $productCount = Product::query()->count();

        $this->info("всего в таблице $productCount включенных товаров");

    }
}
