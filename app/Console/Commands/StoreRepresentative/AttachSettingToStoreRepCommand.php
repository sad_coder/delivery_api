<?php

namespace App\Console\Commands\StoreRepresentative;

use App\Models\StoreRepresentativeSetting;
use App\Models\User;
use Illuminate\Console\Command;

class AttachSettingToStoreRepCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sr:attach-setting {sr_id} {setting_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach setting to store representative';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::find($this->argument('sr_id'));

        if (!$user) {
            $this->error("User not found with id:{$this->argument('sr_id')}");
            return 0;
        }

        $setting = StoreRepresentativeSetting::find($this->argument('setting_id'));

        if (!$setting) {
            $this->error("Setting not found with id: {$this->argument('setting_id')}");
            return 0;
        }

        $user->storeRepresentativeSettings()
            ->syncWithoutDetaching($setting->id);

        $this->info('Successfully attached setting to user');

        return 0;
    }
}
