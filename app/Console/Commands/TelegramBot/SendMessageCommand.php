<?php

namespace App\Console\Commands\TelegramBot;

use Exception;
use Illuminate\Console\Command;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Request as TelegramRequest;
use GuzzleHttp\Client;

class SendMessageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tgbot:send:message {chatId*} {--message=Напоминание,загрузите эксель с новыми остатками продуктов!} {--filepath=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $telegram;
    protected $token;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Telegram $telegram)
    {
        parent::__construct();

        $this->telegram = $telegram;
        $this->token = config('telegrambot.api_key');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $chatIds = $this->argument('chatId');
        $message = $this->option('message');
        $filepath = $this->option('filepath');


        foreach ($chatIds as $chatId) {
            try {
                if ($filepath) {
                    $client = new Client();

                    $client->request('POST', "https://api.telegram.org/bot$this->token/sendDocument", [
                        'multipart' => [
                            [
                                'name' => 'document',
                                'contents' => fopen($filepath, 'r')
                            ],
                            [
                                'name' => 'chat_id',
                                'contents' => $chatId
                            ],
                            [
                                'name' => 'caption',
                                'contents' => $message
                            ]
                        ]
                    ]);
                } else {
                    TelegramRequest::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $message,
                        'parse_mode' => 'HTML'
                    ]);
                }
            } catch (\Exception $e) {
                logger($e->getMessage());
                throw new Exception($e->getMessage());
            }
        }
        $this->info('Успешно отправлено');
        return 0;
    }
}
