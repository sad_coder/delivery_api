<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Product;
use http\Client\Curl\User;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Output\BufferedOutput;

class GenerateExcelProductsReport extends Command
{
    const EXCEL_HEADERS = [
        'Наименование товара',
        'Кол-во',
        'ед. изм',
        'Цена',
        'Сумма',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:single:order:report {orderIds}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orderIds = explode(',', $this->argument('orderIds'));

        $orders = Order::query()
            ->whereIn('id', $orderIds)
            ->with('store', 'user')
            ->get();

        $paths = [];

        foreach ($orders as $order) {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $address = preg_replace('#/#', '\\', $order->store->address);
            $filename = now()->format('Y_m_d') . "_order_{$order->id}_'" . "{$order->user->full_name}_{$address}_{$order->store->name}" . "'.xlsx";

            $filename = str_replace(['/','\\'],'',$filename);

            for ($i = 0; $i < count(self::EXCEL_HEADERS); $i++) {
                $sheet->setCellValueByColumnAndRow($i + 1, 1, self::EXCEL_HEADERS[$i]);
            }
            $j = 2;

            $products = $order->mergeBasketWithProducts()
                ->where('type', Order::BASKET_PRODUCT_TYPE_FOR_PURCHASE)
                ->all();

            foreach ($products as $product) {
                $k = 1;
                $sheet->setCellValueByColumnAndRow($k, $j, $product->name_1c);
                $k++;
                $sheet->setCellValueByColumnAndRow($k, $j, str_replace('.', ',', $product->count));
                $k++;
                $measureName = $product->measure ? $product->measure->name : 'кг';
                $sheet->setCellValueByColumnAndRow($k, $j, $measureName);
                $k++;
                $sheet->setCellValueByColumnAndRow($k, $j, $product->price);
                $k++;
                $sheet->setCellValueByColumnAndRow($k, $j, round($product->price * $product->count));
                $j++;
            }

            $writer = new Xlsx($spreadsheet);
            $path = base_path() . "/storage/app/public/temp/" . $filename;
            $writer->save($path);

            $paths[] = $path;
        }

        $this->info(json_encode($paths));

        return 1;
    }
}
