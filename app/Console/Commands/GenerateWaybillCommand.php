<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\ProductWaybill;

//use Barryvdh\DomPDF\PDF;
use App\Models\ZipWaybill;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use ZipArchive;

/**
 * Class GenerateWaybillCommand
 * @package App\Console\Commands
 */
class GenerateWaybillCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:waybill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создает расходную накладную для заказа';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $start = microtime(true);

        $ordersGrouped = Order::query()
            ->whereHas('user.counterparty')
            ->whereDoesntHave('productWaybill')
            ->with('user')
            ->get()
            ->groupBy(function ($item) {
                return $item->created_at->format('Y-m-d');
            });

        $dataToInsert = [];

        $zip = new ZipArchive();

        $absolutePath = Storage::disk('storage')->path('zips');

        if (!is_dir($absolutePath)) {
            mkdir($absolutePath);
        }

        foreach ($ordersGrouped as $date => $orders) {
            $zipName = "{$date}.zip";
            $absolutePath = Storage::disk('storage')->path('zips');

            $path = $absolutePath . '/' . $zipName;

            try {
                if ($zip->open($path, ZipArchive::CREATE) === TRUE) {
                    foreach ($orders as $order) {
                        $name = "waybill_{$order->id}_{$date}.pdf";
                        $path = "waybill_reports/$name";

                        $dataToInsert[] = [
                            'order_id' => $order->id,
                            'path' => $name,
                            'status' => ProductWaybill::NOT_SEND_TO_BOOKKEEPER,
                        ];

                        if (!$this->checkForExistenceOfReturns($order->basket)) {
                            continue;
                        }

                        $this->generatePdf($order, $path);

                        $zip->addFile(Storage::disk('storage')->path($path), $name);
                    }
                    $zip->close();

                    ZipWaybill::query()->create([
                        'name' => $zipName,
                    ]);

                    ProductWaybill::query()->insert($dataToInsert);

                    $end = microtime(true);
                    $seconds = $end - $start;
                    $this->info("$seconds seconds passed to generate");
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }

    public function checkForExistenceOfReturns($basket)
    {
        $existReturn = false;

        foreach ($basket as $product) {
            $type = $product['type'] ?? null;
            if ($type == 1) {
                $existReturn = true;
                break;
            }
        }
        return $existReturn;
    }

    public function generatePdf($order, $path)
    {
        $basketProducts = $order->mergeBasketWithProductsReturn();

        $returnProducts = $basketProducts->where('type', 1);
        $pdf = PDF::loadView('waybill', compact('order', 'returnProducts'));

        Storage::disk('storage')->put($path, $pdf->output());
    }
}
