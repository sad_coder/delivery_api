<?php

namespace App\Console\Commands\Order;

use App\Models\Order;
use App\Models\OrderReport;
use Illuminate\Console\Command;

class GenerateReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:report {type=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate order, waybill and returns report by orders to xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $orderReport;

    public function __construct()
    {
        parent::__construct();
        $this->orderReport = new OrderReport();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orderType = (int)$this->argument('type');

        $query = Order::whereHas('user.counterparty')
            ->with(["user.counterparty", 'store']);

        if ($orderType === OrderReport::REPORT_TYPE_ORDER) {
            $query->whereDoesntHave('report', function ($q) use($orderType) {
                $q->where('type', $orderType);
            });
        } else {
            $query->whereDoesntHave('report', function ($q) use($orderType){
                $q->where('type', $orderType);
            });
            $query->where('status', Order::STATUS_DELIVERED);
        }

        $orders = $query->get();

        if (!$orders) {
            $this->info('There is no orders to generate report for');
            return 0;
        }
        $todayDate = now()->format('Y-m-d');

        $reportsCount = 0;
        $ordersCount = count($orders);

        foreach ($orders as $order) {
            $order->products = $order->mergeBasketWithProducts();

            $path = OrderReport::generate($order, $todayDate, $orderType);

            $reportsCount++;

            $this->info("The report for order $order->id is saved here : $path, type is $orderType");
        }
        $this->info("Orders total count $ordersCount, generated reports count $reportsCount");
    }
}
