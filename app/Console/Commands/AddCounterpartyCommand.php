<?php

namespace App\Console\Commands;

use App\Models\Counterparty;
use Illuminate\Console\Command;

class AddCounterpartyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:counterparty {user_id} {name_1c} {1c_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Добавляет контр агента для формирования документов в 1С';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (Counterparty::query()->where('id',$this->argument('user_id'))->exists()) {
            $this->error("Ошибка такой контр агент уже добавлен");
            return 0;
        }

        $id =Counterparty::query()->insertOrIgnore([
            'user_id' => $this->argument('user_id'),
            'name_1c' => $this->argument('name_1c'),
            '1c_id' => $this->argument('1c_id')
        ]);

        $this->info("Контр-агент № $id c именем {$this->argument('name_1c')} добавлен успешно");

    }
}
