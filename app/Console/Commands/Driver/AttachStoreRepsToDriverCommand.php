<?php

namespace App\Console\Commands\Driver;

use App\Models\DriverStoreRepresentative;
use Illuminate\Console\Command;

class AttachStoreRepsToDriverCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attach:store-reps-to-driver {driver_id} {store-rep-ids*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $driverId = $this->argument('driver_id');
        $storeRepIds = $this->argument('store-rep-ids');

        foreach ($storeRepIds as $id) {
            DriverStoreRepresentative::query()->firstOrCreate([
               'driver_id' => $driverId,
               'store_representative_id' => $id
            ]);
        }

        $this->info('Store representatives attached to driver');
    }
}
