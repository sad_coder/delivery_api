<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;


class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'create:user {full_name} {email} {password} {role}';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return
     */

    public function handle()
    {

        if (!in_array($this->argument('role'),User::ALLOWED_ROLES)){
            return null;
        }
//        dd($this->arguments());
        User::query()->create([
            "full_name" => $this->argument('full_name'),
            "email" => $this->argument('email'),
            "password" => bcrypt($this->argument('password')),
            "role" => $this->argument('role')
        ]);

    }
}
