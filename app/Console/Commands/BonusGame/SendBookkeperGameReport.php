<?php

namespace App\Console\Commands\BonusGame;

use App\Models\BonusGameReport;
use App\Models\TelegramUser;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use function PHPUnit\Framework\countOf;

class SendBookkeperGameReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:bg:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send bonus game report to accountant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reports = BonusGameReport::where('status', 1)
            ->distinct('path')
            ->get();

        $emails = [
            'gebsss29@gmail.com',
            'khokhloval@foodkz.kz',
            'ivanova@Foodkz.kz'
        ];

        $messageM = 'Отчет об играх';

        if (count($reports)) {

            try {
                $this->sendMailMessage($reports, $emails, $messageM);
            } catch (Exception $e) {
                logger($e->getMessage());
            }
            try {
                $this->sendTelegramMessage($reports, $messageM);
            } catch (Exception $e) {
                logger($e->getMessage());
            }
        } else {
            $this->info('Нету данных для отправки.');
        }
    }

    public function sendMailMessage($reports, $emails, $messageM)
    {
        Mail::send('emails.simple_mail', [], function ($message) use ($reports, $emails, $messageM) {

            $message->to($emails);

            $message->subject($messageM);

            foreach ($reports as $report) {
                $message->attach($report->path);
            }
        });

        if (count(Mail::failures()) === 0) {
            foreach ($reports as $report) {
                BonusGameReport::query()->where('path', $report->path)
                    ->update([
                        'status' => BonusGameReport::STATUS_SENT_TO_MAIL
                    ]);
            }
        }
    }

    public function sendTelegramMessage($reports, $messageM)
    {
        $userIds = TelegramUser::getRelatedUserIds('bonus_game_report');
        if ($userIds) {
            Artisan::call("tgbot:send:message $userIds --message='$messageM'");
            foreach ($reports as $report) {
                Artisan::call("tgbot:send:message $userIds --message='' --filepath=$report->path");
                BonusGameReport::query()->where('path', $report->path)
                    ->update([
                        'status' => BonusGameReport::STATUS_SENT_TO_MAIL
                    ]);
            }
        }
    }
}
