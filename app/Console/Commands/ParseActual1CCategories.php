<?php

namespace App\Console\Commands;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ParseActual1CCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:actual:categories {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse actual categories from excel sheet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $xlsx = new Xlsx();

        $spreadsheet = $xlsx->load($this->argument('path'))->getActiveSheet();

        for ($i = 4; $i <= 302; $i++) {
            $type = $spreadsheet->getCell('D' . $i)->getValue();
            $name = $spreadsheet->getCell('C' . $i)->getValue();
            switch ($type) {
                case 'brand':
                    $brand = Brand::firstOrCreate([
                        'name' => $name
                    ]);
                    break;
                case 'category':
                    $category = Category::firstOrCreate([
                        'name_1c' => $name,
                        'brand_id' => $brand->id
                    ], [
                        'name_1c' => $name,
                        'brand_id' => $brand->id
                    ]);
                    break;
                case 'skip':
                    continue 2;
                default:
                    $id = $spreadsheet->getCell('B'.$i)->getValue();

                    $product = Product::where('id_1c', $id)
                        ->first();
                    if (!$product) {
                        $this->info("$name not found in our table but exist on new excel");
                    } else {
                        $product->update([
                            'category_id' => $category->id,
                            'brand_id' => $brand->id
                        ]);
                    }
            }
        }
    }
}
