<?php

namespace App\Console\Commands;

use App\Models\BonusGame;
use App\Models\District;
use App\Models\Order;
use Illuminate\Console\Command;

//$coordinates = 'coordinate_a-coordinate_b-coordinate_c-coordinate_d';
//$arr = explode('-', $coordinates);
//echo $arr[0].':'.$arr[1].' '.$arr[2].':'.$arr[3];

class CreateCoordinateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:coordinate coordinate_a coordinate_b coordinate_c coordinate_d';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::query()->with('store')->get();
        $res = [];
        foreach ($orders as $order) {
            $orderBonusSum = BonusGame::query()->where('mobile_id',$order->mobile_id)->sum('sum_win');
            $order->sum_win_all = $orderBonusSum;

        }
        dd($orders->where('sum_win_all','<>',null)->get());

        District::query()->create([
            'coordinates' => [
                "coordinate_a" => $this->argument('coordinate_a'),
                "coordinate_b" => $this->argument('coordinate_b'),
                "coordinate_c" => $this->argument('coordinate_c'),
                "coordinate_d" => $this->argument('coordinate_d')
            ],
            'user_id' => 1
        ]);

    }
}
