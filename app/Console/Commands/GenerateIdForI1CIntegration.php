<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateIdForI1CIntegration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:id:1c';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $startIds = 1000000000000;
        $products = Product::query()->get();
        $spreadsheet = IOFactory::load(storage_path('catalog.xlsx'));
        $sheets = $spreadsheet->getActiveSheet();
        $sheets->insertNewRowBefore(1);

        foreach ( $sheets->toArray() as $k => $sheet) {
            if ($sheet[0] !=null && $k >2){
                $prodInDb = $products->where('id_1c',$sheet[0])->first();
                if ($prodInDb) {
                    $prodInDb->presale_id = $startIds + $prodInDb->id;
                    $prodInDb->save();
                    $sheets->setCellValue('I'.$k, (string)($startIds + $prodInDb->id));
                }

            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path('catalog1.xlsx'));

    }
}
