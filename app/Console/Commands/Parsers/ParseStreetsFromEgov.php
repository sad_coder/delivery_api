<?php

namespace App\Console\Commands\Parsers;

use App\Models\City;
use App\Models\Region;
use App\Models\Street;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ParseStreetsFromEgov extends Command
{
    const EGOV_API_URL = 'https://data.egov.kz/api/detailed/almaty_kalasynyn_kosheleri/';


    const CITIES_AMOUNT = 5000;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:streets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse streets, region from egov api.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $apiKey = env('EGOV_API_KEY');

        $city = City::firstOrCreate(['name' => 'Алматы']);

        foreach ([1, 2, 3, 4] as $i) {
            try {
                $response = Http::withOptions(['verify' => false])->get(self::EGOV_API_URL."v$i". "?apiKey=$apiKey&source={\"size\" : 3342}");
                $result = $response->json()['data'];
            } catch (ClientException $e) {
                throw new Exception($e->getMessage());
            }

            $counter = 0;

            DB::beginTransaction();
            try {
                foreach ($result as $street) {
                    if (isset($street['name_ru']) && isset($street['name_kz']) && isset($street['period_ru']) && isset($street['period_kz'])) {

                        if (!Region::where('name_kz', $street['period_kz'])->exists() && !Region::where('name_ru', $street['period_ru'])->exists()) {
                            $region = Region::create([
                                'name_ru' => $street['period_ru'],
                                'name_kz' => $street['period_kz'],
                                'city_id' => $city->id
                            ]);
                        }

                        if (!Street::where('name_ru', $street['name_ru'])->exists() && !Street::where('name_kz', $street['name_kz'])->exists()) {
                            $counter++;
                            Street::create([
                                'name_ru' => $street['name_ru'],
                                'name_kz' => $street['name_kz'],
                                'region_id' => $region->id,
                            ]);
                        }
                    }
                }
            } catch (\Exception $e) {
                DB::rollBack();
                throw new \Exception($e->getMessage());
            }

            DB::commit();

            $this->info("Success! Total parse streets amount at version $i is: $counter");
        }
    }
}
