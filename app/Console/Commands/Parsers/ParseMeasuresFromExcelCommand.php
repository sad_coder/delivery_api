<?php

namespace App\Console\Commands\Parsers;

use App\Models\Measure;
use App\Services\Parser\Excel\MeasureExcelParser;
use Illuminate\Console\Command;

class ParseMeasuresFromExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:measures {excelPath} {columnName=H}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse excel file by provided path and save to database if new records presented';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $excelPath = $this->argument('excelPath');
        $columnName = $this->argument('columnName');

        $parser = new MeasureExcelParser($excelPath, $columnName);

        $parser->parse();

        foreach ($parser->result as $measure) {
            Measure::updateOrCreate(
                ['name_1c' => $measure],
                ['name_1c' => $measure]
            );
        }

        $this->info('Successfully parsed and saved measures');
    }
}
