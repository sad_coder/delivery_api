<?php

namespace App\Console\Commands\Parsers;

use App\Models\Brand;
use App\Services\Parser\Excel\BrandExcelParser;
use http\Header\Parser;
use Illuminate\Console\Command;

class ParseBrandsFromExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:brands {excelPath} {columnName=F}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse excel file by provided path and save to database if new records presented';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $excelPath = $this->argument('excelPath');
        $columnName = $this->argument('columnName');

        $parser = new BrandExcelParser($excelPath, $columnName);

        $parser->parse();

        foreach ($parser->result as $brand) {
            Brand::updateOrCreate(
                ['name' => $brand],
                ['name' => $brand]
            );
        }

        $this->info('Successfully parsed and saved brands');
    }
}
