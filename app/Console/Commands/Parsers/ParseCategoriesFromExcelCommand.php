<?php

namespace App\Console\Commands\Parsers;

use App\Models\Brand;
use App\Models\Category;
use App\Services\Parser\Excel\CategoryExcelParser;
use Illuminate\Console\Command;

class ParseCategoriesFromExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:categories {excelPath} {columnName=G}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse excel file by provided path and save to database if new records presented';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $excelPath = $this->argument('excelPath');
        $columnName = $this->argument('columnName');

        $parser = new CategoryExcelParser($excelPath, $columnName);

        $parser->parse();

        foreach ($parser->result as $category) {

            $brand = null;

            if ($category['brand_name']) {
                $brand = Brand::where('name', $category['brand_name'])->first();
            }

            Category::updateOrCreate(
                ['name_1c' => $category['name']],
                ['name_1c' => $category['name'], 'brand_id' => $brand ? $brand->id : null]
            );
        }

        $this->info('Successfully parsed and saved categories');
    }
}
