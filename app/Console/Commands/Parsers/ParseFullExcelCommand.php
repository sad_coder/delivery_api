<?php

namespace App\Console\Commands\Parsers;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ParseFullExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:full {excelPath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse full table and models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $excelPath = $this->argument('excelPath');

        Artisan::call('parse:brands', [
           'excelPath' => $excelPath
        ]);

        $this->info('brands done...');

        Artisan::call('parse:categories', [
           'excelPath' => $excelPath
        ]);

        $this->info('categories done...');

        Artisan::call('parse:measures', [
           'excelPath' => $excelPath
        ]);

        $this->info('measures done...');

        Artisan::call('parse:products', [
           'excelPath' => $excelPath
        ]);

        $this->info('products done...');

        $this->info('EVERYTHING PARSED AND SAVED SUCCESSFULLY!');

        return 0;
    }
}
