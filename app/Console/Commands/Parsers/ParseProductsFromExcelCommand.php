<?php

namespace App\Console\Commands\Parsers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Measure;
use App\Models\Product;
use App\Services\Parser\Excel\ProductExcelParser;
use Illuminate\Console\Command;

class ParseProductsFromExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:products {excelPath} {columnName=C}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse excel file by provided path and save to database if new records presented';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $excelPath = $this->argument('excelPath');
        $columnName = $this->argument('columnName');

        $parser = new ProductExcelParser($excelPath, $columnName);

        $parser->parse();

        $brands = Brand::all();
        $categories = Category::all();
        $measures = Measure::all();

        foreach ($parser->result as $item) {

            $brand = $brands->where('name', $item['brand'])->first();
            $category = $categories->where('name_1c', $item['category'])->first();
            $measure = $measures->where('name_1c', $item['measure'])->first();

            Product::updateOrCreate(
                [
                    'id_1c' => $item['id_1c'],
                    'article' => $item['article'],
                    'remainder' => $item['remainder'],
                    'price' => $item['price'],
                    'brand_id' => $brand ? $brand->id : null,
                    'category_id' => $category ? $category->id : null,
                    'measure_id' => $measure ? $measure->id : null,
                    'name_1c' => $item['name_1c']
                ],
                [
                    'id_1c' => $item['id_1c'],
                    'article' => $item['article'],
                    'remainder' => $item['remainder'],
                    'price' => $item['price'],
                    'brand_id' => $brand ? $brand->id : null,
                    'category_id' => $category ? $category->id : null,
                    'measure_id' => $measure ? $measure->id : null,
                    'name_1c' => $item['name_1c']
                ]
            );
        }

        $this->info('products saved successfully');
    }
}
