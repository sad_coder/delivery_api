<?php

namespace App\Console\Commands\Maps;

use Illuminate\Console\Command;
use function PHPUnit\Framework\countOf;

class GetDistanceBetweenCoords extends Command
{
    const EARTH_RADIUS = 6372795;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maps:getDistance {point1 : example : 44.111:75.444} {point2 : example : 44.111:75.444}}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get distance in meters between 2 points on (earth) by Goversins formula. Pass arguments like this: 43.122:76.444 43.333:75.444';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $point1 = explode(':', $this->argument('point1'));
        $point2 = explode(':', $this->argument('point2'));

        if (count($point1) < 2 || count($point2) < 2) {
            $this->error('Incorrect point syntax.');
            return 0;
        }

        $lat1 = $point1[0] * M_PI / 180;
        $lat2 = $point2[0] * M_PI / 180;
        $long1 = $point1[1] * M_PI / 180;
        $long2 = $point2[1] * M_PI / 180;

        $a = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($long2 - $long1));

        $res = self::EARTH_RADIUS * $a;

        if ($res > 1000) {
            $km = $res / 1000;
        }

        $meters = $res % 1000;

        $this->info("The distance between ($point1[0],$point1[1]) and ($point2[0], $point2[1]) is : " . ((isset($km) && $km > 0) ? "$km kilometers" : "$meters meters"));

        return $res;
    }
}
