<?php

namespace App\Console\Commands;

use App\Models\BonusGameReport;
use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateBookkeperGameReport extends Command
{
    const EXCEL_HEADERS = [
        'Номер заказа',
        'Номер РНК',
        'Контрагент',
        'Торговая точка',
        'Сумма заказа',
        'Сумма возврата',
        'Сумма ожид. прихода',
        'Сумма выигрыша',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:game:report {--onlyWhereHasBonusGames}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate game reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $query = Order::query()
            ->whereDoesntHave('bonusGamesReport')
            ->whereHas('user.counterparty')
            ->with([
                'store',
                'bonusGames',
                'user']);

        if ($this->option('onlyWhereHasBonusGames')) {
            $query->whereHas('bonusGames');
        }

        $orders = $query->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        for ($i = 0; $i < count(self::EXCEL_HEADERS); $i++) {
            $sheet->setCellValueByColumnAndRow($i + 1, 1, self::EXCEL_HEADERS[$i]);
        }
        $j = 2;

        $name = "bonus_games_report_" . now()->format('Y-m-d') . ".xlsx";
        $path = base_path() . "/storage/app/public/" . $name;

        foreach ($orders as $order) {
            $k = 1;
            $sheet->setCellValueByColumnAndRow($k, $j, $order->id);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, '');
            $k++;
            $sheet->setCellValueExplicitByColumnAndRow($k, $j, $order->user->counterparty->name_1c, DataType::TYPE_STRING);
            $k++;
            $sheet->setCellValueExplicitByColumnAndRow($k, $j, $order->store->name . " ({$order->id})", DataType::TYPE_STRING);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, round($order->total_cost - $order->total_returns_cost) > 0 ? $order->total_cost - $order->total_returns_cost : 0);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, round($order->total_returns_cost));
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, round($order->total_cost - (2 * $order->total_returns_cost) > 0 ? $order->total_cost - (2 * $order->total_returns_cost) : 0));
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, $order->bonusGames->sum('sum_win'));
            $j++;

            $order->bonusGamesReport()->create([
                'status' => BonusGameReport::STATUS_GENERATED,
                'path' => $path
            ]);
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save($path);

        $this->info('Bonus Games Report Generated');

        $this->info('Sending reports to mail');

        Artisan::call('send:bg:report');

        $this->info('Success');
    }
}
