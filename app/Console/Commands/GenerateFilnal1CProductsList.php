<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Scopes\EnabledScope;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateFilnal1CProductsList extends Command
{

    const EXCEL_HEADERS = [
        'Название',
        'id',
        'единица измерения'
    ];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:final:product:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерирует финальный лист товаров для отправки в 1с';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $products = Product::query()->get();
//        $productsNotPresaleId = $products->where('presale_id',null);
//        $maxPresaleId = Product::query()->withoutGlobalScope(EnabledScope::class)->max('presale_id');
//
//
//        foreach ($productsNotPresaleId as $product) {
//            $product->presale_id = ++$maxPresaleId;
//            $product->save();
//        }
//        $allProducts = Product::query()->get();

        $allProducts = Product::query()
            ->where('enabled',true)
//            ->where('presale_id','<>',null)
            ->get();

//
//        foreach ($allProducts as $product) {
//            Product::query()->where('id',$product->id)->update([
//               'presale_id' => (int)('5'.substr($product->presale_id,1))
//            ]);
//        }
//        dd("dich");

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $countHeaderList = count(self::EXCEL_HEADERS);
        $countProducts = count($allProducts);

        for ($i = 0; $i < $countHeaderList; $i++) {
            $sheet->setCellValueByColumnAndRow($i + 1, 1, self::EXCEL_HEADERS[$i]);
        }

        $j = 2;
        for ($i = 0; $i < $countProducts; $i++) {
            $k = 1;
            $sheet->setCellValueByColumnAndRow($k, $j, $allProducts[$i]->name_1c);
            $k++;
            $sheet->setCellValueByColumnAndRow($k, $j, (string)$allProducts[$i]->presale_id);
            $j++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path('final_1c_list.xlsx'));

    }
}
