<?php

namespace App\Console;

use App\Console\Commands\CreateCoordinateCommand;
use App\Console\Commands\CreateUserCommand;
use App\Console\Commands\Database\DoBackupCommand;
use App\Console\Commands\GenerateWaybillCommand;
use App\Console\Commands\SendZipPdfToMerchandiser;
use App\Models\TelegramUser;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DoBackupCommand::class,
        CreateUserCommand::class,
        CreateCoordinateCommand::class,
        GenerateWaybillCommand::class,
        SendZipPdfToMerchandiser::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('db:backup')->dailyAt('00:00')->timezone('Asia/Almaty');
        //Генерация и отправка торговому возвратных накладных
        $schedule->command('generate:waybill')->dailyAt('17:35')->timezone('Asia/Almaty');
        $schedule->command('send:mail:bookkeeper')->dailyAt('17:40')->timezone('Asia/Almaty');
        //Генерация и отправка торговому возвратных накладных

        // Генерация и отправка торговых точек
        $schedule->command('store:report')->dailyAt('17:00')->timezone('Asia/Almaty');
        // Генерация отчета бухгалтерии
        $schedule->command('generate:game:report')->dailyAt('17:20')->timezone('Asia/Almaty');
        $schedule->command('send:bg:report')->dailyAt('17:40')->timezone('Asia/Almaty');


        // Отправка xml файлов в 1с
        $schedule->command('generate:report')->dailyAt('17:10')->timezone('Asia/Almaty');
        $schedule->command('distribute:delivery')->dailyAt('18:00')->timezone('Asia/Almaty');
        #$schedule->command('generate:report 1')->dailyAt('17:05')->timezone('Asia/Almaty');
        #$schedule->command('generate:report 2')->dailyAt('17:00')->timezone('Asia/Almaty');

        $schedule->call(function () {
            try {
                $userIds = TelegramUser::getRelatedUserIds('update_remainders_notification');
                $message = "Напоминание!\nПожалуйста обновите остатки товаров, воспользовавшись функциями бота!";
                Artisan::call("tgbot:send:message $userIds --message='$message'");
            } catch (Exception $e) {
                logger($e->getMessage());
            }
        })->dailyAt('09:30');

        # $schedule->command('send:reports')->dailyAt('17:30')->timezone('Asia/Almaty');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
