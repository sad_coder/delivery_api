<?php

namespace App\Scopes\Product;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class HasPriceScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereNotNull('price');
    }
}
