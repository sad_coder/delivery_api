<?php

namespace App\Services\TelegramBot;

use App\Models\TelegramUser;
use App\Services\TelegramBot\States\CommandState;
use App\Services\TelegramBot\States\DefaultState;
use App\Services\TelegramBot\States\UploadProductsRemainderState;
use App\Services\TelegramBot\States\WelcomeState;
use Illuminate\Support\Facades\Log;

class BaseTelegramBotProcessor
{
    public $first_name;
    public $username;
    public $chat_id;
    public $text;
    public $sticker;
    public $message_id;
    public $callback_query;
    public $callback_query_from_id;
    public $document;
    public $isBotCommand;

    public $state;

    public function __construct(array $update)
    {
        $this->first_name = $update['message']['chat']['first_name'] ?? $data['callback_query']['message']['chat']['first_name'] ?? '';
        $this->username = $update['message']['chat']['username'] ?? $data['callback_query']['message']['chat']['username'] ?? '';
        $this->chat_id = $update['message']['chat']['id'] ?? $data['callback_query']['message']['chat']['id'] ?? '';
        $this->text = $update['message']['text'] ?? $data['callback_query']['message']['text'] ?? '';
        $this->sticker = $update['message']['sticker'] ?? '';
        $this->callback_query = $update['callback_query'] ?? '';
        $this->callback_query_from_id = $update['callback_query']['from']['id'] ?? '';
        $this->message_id = $update['message']['message_id'] ?? $data['callback_query']['message']['message_id'] ?? '';
        $this->document = $update['message']['document'] ?? '';
        $this->isBotCommand = isset($update['message']['entities'][0]['type']);
    }

    public function processUpdate()
    {
        if ($this->callback_query) {
            $callback = $this->parseCallbackQuery();
            if ($callback['key'] === 'ft') {
                if ($callback['value'] == 1) {
                    $this->state = new UploadProductsRemainderState([
                        'chat_id' => $this->callback_query_from_id,
                    ], 1);
                    TelegramBotWorker::dispatch($this->state);
                    return 0;
                }
            }
        }
        if (!$this->isUserExist()) {
            $this->state = new WelcomeState([
                'username' => $this->username,
                'chat_id' => $this->chat_id,
                'text' => $this->text
            ], 1);
            TelegramBotWorker::dispatch($this->state);
            return 0;
        }
        if (!$this->isUserAuthenticated()) {
            $this->state = new WelcomeState([
                'username' => $this->username,
                'chat_id' => $this->chat_id,
                'text' => $this->text
            ], 2);
            TelegramBotWorker::dispatch($this->state);
            return 0;
        }
        if ($this->isBotCommand) {
            $command = $this->text;

            if ($command == '/info') {
                $this->state = new CommandState(['chat_id' => $this->chat_id], 1);
                TelegramBotWorker::dispatch($this->state);
                return 0;
            }

            if ($command == '/stop') {
                $this->state = new CommandState(['chat_id' => $this->chat_id], 2);
                TelegramBotWorker::dispatch($this->state);
                return 0;
            }
        }
        if ($this->userHasNoState()) {
            $this->state = new DefaultState(['chat_id' => $this->chat_id], 1);
            TelegramBotWorker::dispatch($this->state);
            return 0;
        }
        if (!$this->userHasNoState()) {
            $user = TelegramUser::query()->where('chat_id', $this->chat_id)->first();

            $stateType = $user->state_type;
            $state = $user->state;
            switch ($stateType) {
                case 'upload-products':
                    $this->state = new UploadProductsRemainderState([
                        'document' => $this->document,
                        'chat_id' => $this->chat_id
                    ], $state);
                    TelegramBotWorker::dispatch($this->state);
                    break;
            }
        }
    }

    private function isUserExist()
    {
        return TelegramUser::where('chat_id', $this->chat_id)->exists();
    }

    private function isUserAuthenticated()
    {
        return TelegramUser::where('chat_id', $this->chat_id)->first()->is_authenticated;
    }

    private function userHasNoState()
    {
        $user = TelegramUser::where('chat_id', $this->chat_id)->first();

        return $user->state === null && $user->state_type === null;
    }

    private function parseCallbackQuery()
    {
        $temp = explode(':', $this->callback_query['data']);

        return [
            'key' => $temp[0],
            'value' => $temp[1]
        ];
    }
}
