<?php


namespace App\Services\TelegramBot\States;


use App\Models\Product;
use App\Models\TelegramUser;
use App\Services\TelegramBot\TelegramBotStateContract;
use App\Services\TelegramBot\TelegramBotWorker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Longman\TelegramBot\Request as TelegramRequest;

class UploadProductsRemainderState extends TelegramBotWorker implements TelegramBotStateContract
{
    const STATES = [
        1 => 'send file message',
        2 => 'waiting for file'
    ];

    const ACCEPTABLE_MIMETYPES = [
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];
    const STATE_TYPE = 'upload-products';

    public $state;
    public $data;

    public $exception;

    public function __construct($data, $state)
    {
        $this->state = $state;
        $this->data = $data;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getClassName()
    {
        return get_class($this);
    }

    public function state1()
    {
        $this->sendMessage($this->data['chat_id'], "Пришлите пожалуйста эксель файл с остатками формата - xls или xlsx, в одном количестве\nЕсли файлов несколько просто повторите эту процедуру необходимое количество раз", 'HTML');

        $user = TelegramUser::query()->where('chat_id', $this->data['chat_id'])->first();

        $user->state_type = 'upload-products';
        $user->state = 2;
        $user->save();
    }

    public function state2()
    {
        if ($this->data['document'] == null) {
            $this->sendMessage($this->data['chat_id'], 'Пожалуйста скиньте файл', 'HTMl');
            return 0;
        }

        Log::info($this->data['document']['mime_type']);
        if (!in_array($this->data['document']['mime_type'], self::ACCEPTABLE_MIMETYPES)) {
            $this->sendMessage($this->data['chat_id'], "Данный формат файла не поддерживается\n Поддерживаемые форматы <b>xls</b>,<b>xlsx</b>", 'HTMl');
            return 0;
        }

        $this->sendMessage($this->data['chat_id'], 'Начинаю загрузку файла', 'HTML');

        $successFileDownload = $this->downloadFile();

        if ($successFileDownload) {
            $this->sendMessage($this->data['chat_id'], 'Файл загружен успешно', 'HTML');
        } else {
            $this->sendMessage($this->data['chat_id'], 'Произошла несанкцанированная ошибка, обратитесь к программисту', 'HTML');
            $this->sendMessage($this->data['chat_id'], $this->exception, 'HTML');
            return 0;
        }

        $this->sendMessage($this->data['chat_id'], 'Начинаю обновление остатков', 'HTML');
        Artisan::queue("update:products:remainder");
        $this->sendMessage($this->data['chat_id'],"<b>Об остальном я позабочусь сам!</b>",'HTML');

        $this->nullateUserState($this->data['chat_id']);
        return 0;
    }

    private function downloadFile()
    {
        $token = config('telegrambot.api_key');

        try {
            $serverResponse = TelegramRequest::getFile(['file_id' => $this->data['document']['file_id']]);
            if ($serverResponse->isOk()) {
                $file = $serverResponse->getResult();
                $path = $file->file_path;
                $file = file_get_contents("https://api.telegram.org/file/bot$token/$path");
                Storage::disk('storage')->put("inventorization/" . Product::REMAINDERS_UPDATE_FILE_NAME, $file);
            } else {
                $this->exception = $serverResponse->getErrorCode();
                return 0;
            }
        } catch (\Exception $e) {
            $this->exception = $e->getMessage();
            logger($e->getMessage());
            return 0;
        }

        return 1;
    }
}
