<?php


namespace App\Services\TelegramBot\States;


use App\Services\TelegramBot\TelegramBotStateContract;
use App\Services\TelegramBot\TelegramBotWorker;

class CommandState extends TelegramBotWorker implements TelegramBotStateContract
{
    public $state;
    public $data;

    const COMMANDS_LIST = [
        [
            'id' => 1,
            'command' => '/info',
            'description' => 'Информация о боте'
        ],
        [
            'id' => 2,
            'command' => '/stop',
            'description' => 'Отмена действия'
        ]
    ];

    public function __construct($data, $state)
    {
        $this->data = $data;
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getClassName()
    {
        return get_class($this);
    }

    public function state1()
    {
        $this->sendMessage($this->data['chat_id'], "По всем вопросам к программисту!", 'HTML');
    }

    public function state2()
    {
        $this->sendMessage($this->data['chat_id'], "Все действия были отменены, начните заново!", 'HTML');
        $this->nullateUserState($this->data['chat_id']);
    }
}
