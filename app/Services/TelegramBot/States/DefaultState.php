<?php


namespace App\Services\TelegramBot\States;


use App\Services\TelegramBot\TelegramBotStateContract;
use App\Services\TelegramBot\TelegramBotWorker;
use Longman\TelegramBot\Entities\InlineKeyboard;

class DefaultState extends TelegramBotWorker implements TelegramBotStateContract
{
    public $state;
    public $data;

    const FEATURES_LIST = [
        [
            'id' => 1,
            'name' => 'Обновить остататки'
        ]
    ];

    public function __construct($data, $state)
    {
        $this->state = $state;
        $this->data = $data;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getClassName()
    {
        return get_class($this);
    }

    public function state1()
    {
        $reply_markup = $this->generateInlineKeyboard(self::FEATURES_LIST);

        $this->sendMessage($this->data['chat_id'], "Список моих умении:\nКликните по одной из них, и следуйте инструкциям!", 'HTML', $reply_markup);
    }

    public function state2()
    {
        $this->sendMessage($this->data['chat_id'], "Бот на разработке!", 'HTML');
    }

}
