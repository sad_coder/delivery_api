<?php

namespace App\Services\TelegramBot\States;

use App\Models\TelegramBotAuthToken;
use App\Models\TelegramUser;
use App\Services\TelegramBot\TelegramBotStateContract;
use App\Services\TelegramBot\TelegramBotWorker;

class WelcomeState extends TelegramBotWorker implements TelegramBotStateContract
{
    const STATES = [
        1 => 'new user',
        2 => 'waiting for auth token'
    ];

    const STATE_TYPE = 'welcome';

    public $state;
    public $data;

    public function __construct($data, $state)
    {
        $this->data = $data;
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getClassName()
    {
        return get_class($this);
    }

    public function state1()
    {

        $this->sendMessage($this->data['chat_id'], "Приветствую,я бот предназначенный для облегчения жизни бухгалтерии. Просто следуйте инстуркциям!\nНа данный момент из действующий функциналов есть <b>обновление остатков у товара</b>. После авторизации через токен, впишите /help, и выберите из меню 1 строку(обновление остатков) после нужно будет вставить необходимый эксель файл с новыми остатками продуктов и отправить их мне, а дальше я уже позабочусь об остальном. Также я буду ежедевно отправлять вам отчеты и напоминать об остатках ", 'HTML');
        $this->sendMessage($this->data['chat_id'], "Также буду отправлять отчеты разного вида", 'HTML');
        $this->sendMessage($this->data['chat_id'], 'Для того чтобы продолжить работу введите токен авторизации, его можно получить у администратора!', 'HTML');

        TelegramUser::query()->create([
            'username' => $this->data['username'],
            'chat_id' => $this->data['chat_id'],
            'state_type' => self::STATE_TYPE,
            'state' => 2
        ]);
    }

    public function state2()
    {
        if ($token = TelegramBotAuthToken::query()->where('token', $this->data['text'])->first()) {

            $this->sendMessage($this->data['chat_id'], 'Вы успешно авторизованы!', 'HTML');

            $user = TelegramUser::where('chat_id', $this->data['chat_id'])->first();
            $user->is_authenticated = true;
            $user->save();

            $this->nullateUserState($this->data['chat_id']);

            $token->telegram_user_id = $user->id;
            $token->save();

        } else {
            $this->sendMessage($this->data['chat_id'], 'Токен не действителен! Пожалуйста проверьте корректен ли данный токен', 'HTML');
        }
    }
}
