<?php


namespace App\Services\TelegramBot;

use App\Models\Product;
use App\Models\TelegramUser;
use App\Services\TelegramBot\States\DefaultState;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Request as TelegramRequest;


abstract class TelegramBotWorker
{
    public static function dispatch(TelegramBotStateContract $stateType)
    {
        $state = $stateType->getState();
        $method = "state$state";
        $className = $stateType->getClassName();

        if (!method_exists($stateType, $method)) {
            logger("Method $method not exist in $className");
            throw new Exception("Method $method does not exist in $className");
        }

        $stateType->$method();
    }

    public function sendMessage($chatId, $message, $parseMode, $replyMarkup = null)
    {
        $data = [
            'chat_id' => $chatId,
            'text' => $message,
            'parse_mode' => $parseMode
        ];

        if ($replyMarkup) {
            $data['reply_markup'] = $replyMarkup;
        }

        $response = TelegramRequest::sendMessage($data);
    }

    public function nullateUserState($chatId)
    {
        $user = TelegramUser::query()->where('chat_id', $chatId)->first();
        $user->state_type = null;
        $user->state = null;
        $user->save();

        self::dispatch(new DefaultState(['chat_id' => $chatId], 1));
    }

    public function generateInlineKeyboard($items)
    {
        $itemsArray = array_map(function ($feature) {
            return [
                'text' => $feature['id'] . "." . $feature['name'],
                'callback_data' => "ft:${feature['id']}",
            ];
        }, $items);

        $max_per_row = 1;
        $per_row = sqrt(count($itemsArray));
        $rows = array_chunk($itemsArray, $per_row === floor($per_row) ? $per_row : $max_per_row);
        return new InlineKeyboard(...$rows);
    }
}
