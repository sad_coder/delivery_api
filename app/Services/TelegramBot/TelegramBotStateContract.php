<?php


namespace App\Services\TelegramBot;


interface TelegramBotStateContract
{
    public function getState();

    public function getClassName();
}
