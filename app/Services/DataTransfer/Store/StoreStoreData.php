<?php

namespace App\Services\DataTransfer\Store;

use App\Services\DataTransfer\DataTransferContract;
use Exception;
use Illuminate\Support\Facades\Auth;

class StoreStoreData implements DataTransferContract
{
    const REQUIRED_FIELDS_FROM_REQUEST = [
        'address' => 'shop_addr',
        'phone' => 'shop_phone',
        'name' => 'shop_name',
        'bin' => 'shop_bin',
        'longitude' => 'shop_longitude',
        'latitude' => 'shop_latitude'
    ];

    public $seller_id;
    public $address;
    public $phone;
    public $name;
    public $bin;
    public $latitude;
    public $longitude;


    public function getRequiredFieldsFromRequest()
    {
        return self::REQUIRED_FIELDS_FROM_REQUEST;
    }

    public function get($key)
    {
        return $this->$key;
    }

    public function set($key, $value)
    {
        $this->$key = $value;
    }

    public function getResult()
    {
        return $this;
    }

    public function setFieldsManually(array $request)
    {
        $this->seller_id = Auth::id();
    }

    public function getResultingArray()
    {
        $props = get_object_vars($this);

        foreach ($props as $prop => $value) {
            if (!$prop) {
                throw new Exception("$prop is not defined! Please set it before getting result");
            }
        }

        return $props;
    }

    public function getResultingArrayWithoutValidation()
    {
        return get_object_vars($this);
    }

    public function doPreCasts()
    {

    }
}
