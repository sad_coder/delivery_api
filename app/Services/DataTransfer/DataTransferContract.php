<?php

namespace App\Services\DataTransfer;

interface DataTransferContract
{
    public function getRequiredFieldsFromRequest();

    public function get($key);

    public function set($key, $value);

    public function getResult();

    public function setFieldsManually(array $request);

    public function doPreCasts();
}
