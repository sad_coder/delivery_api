<?php

namespace App\Services\DataTransfer;

use App\Http\Requests\Order\StoreOrderForStoreRequest;
use Exception;

class DataTransfer
{
    public $request;

    public $setter;

    public function __construct(StoreOrderForStoreRequest $request)
    {
        $this->request = $request;
    }

    public function setData(DataTransferContract $setter)
    {
        $this->setter = $setter;

        $fields = $setter->getRequiredFieldsFromRequest();

        foreach ($fields as $field => $requestName) {
            $setter->set($field, $this->request->$requestName);
        }

        $setter->setFieldsManually($this->request->all());

        $setter->doPreCasts();
    }
}
