<?php

namespace App\Services\DataTransfer\Order;


use App\Services\DataTransfer\DataTransferContract;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

class StoreOrderData implements DataTransferContract
{
    const REQUIRED_FIELDS_FROM_REQUEST = [
        'basket' => 'basket',
        'address' => 'delivery_data',
        'mobile_id' => 'mobile_id',
        'payment_at' => 'payment_at'
    ];

    public $user_id;
    public $delivery_time;
    public $basket;
    public $address;
    public $store_id;
    public $mobile_id;
    public $payment_at;

    public function getRequiredFieldsFromRequest()
    {
        return self::REQUIRED_FIELDS_FROM_REQUEST;
    }

    public function get($key)
    {
        return $this->$key;
    }

    public function set($key, $value)
    {
        $this->$key = $value;
    }

    public function getResult()
    {
        return $this;
    }


    public function setFieldsManually($request)
    {
        $this->user_id = Auth::id();
        $this->delivery_time = $request['delivery_data']['delivery_time'];
    }

    public function getResultingArray()
    {
        $props = get_object_vars($this);

        foreach ($props as $prop => $value) {
            if (!$prop) {
                throw new Exception("$prop is not defined! Please set it before getting result");
            }
        }

        return $props;
    }

    public function getResultingArrayWithoutValidation()
    {
        return get_object_vars($this);
    }

    public function doPreCasts()
    {
        $this->payment_at = Carbon::createFromTimestamp($this->payment_at)->toDateTimeString();
    }
}
