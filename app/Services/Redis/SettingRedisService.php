<?php

namespace App\Services\Redis;

use App\Models\StoreRepresentativeSetting;
use App\Models\User;
use Illuminate\Support\Facades\Redis;

class SettingRedisService
{
    public static function setSettingBySlugAndUser(StoreRepresentativeSetting $setting, int $userId)
    {
        Redis::hmset("u:$userId:s:$setting->slug",$setting->data);
    }

    public static function getSettingBySlugAndUser(string $slug, int $userId)
    {
        return Redis::hgetAll("u:$userId:s:$slug");
    }
}
