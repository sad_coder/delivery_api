<?php


namespace App\Services\Parser\Excel;


use Illuminate\Support\Facades\App;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ProductExcelParser extends BaseExcelParser
{
    public function __construct(string $excelPath, string $columnName = self::PRODUCT_COLUMN_COORDINATE)
    {
        parent::__construct(App::make(Xlsx::class));

        $this->excelPath = $excelPath;
        $this->columnName = $columnName;

        $this->initSpreadsheet();
    }

    public function parse()
    {
        $products = [];

        for ($i = 5; $i < $this->highestRow; $i++) {
            $productName = $this->spreadsheet->getCell($this->columnName . $i)->getValue();

            if (
                array_search($productName, array_column($products, 'name_1c')) === false &&
                !in_array($productName, self::POSSIBLE_NULL_VALUES)
            ) {
                $id1c = $this->spreadsheet->getCell(self::ID_1C_COLUMN_COORDINATE . $i)->getValue();
                $article = $this->spreadsheet->getCell(self::ARTICLE_COLUMN_COORDINATE . $i)->getValue();
                $remainder = $this->spreadsheet->getCell(self::REMAINDER_COLUMN_COORDINATE . $i)->getValue();
                $price = $this->spreadsheet->getCell(self::PRICE_COLUMN_COORDINATE . $i)->getValue();
                $brand = $this->spreadsheet->getCell(self::BRANDS_COLUMN_COORDINATE . $i)->getValue();
                $category = $this->spreadsheet->getCell(self::CATEGORY_COLUMN_COORDINATE.$i)->getValue();
                $measure = $this->spreadsheet->getCell(self::MEASURE_COLUMN_COORDINATE.$i)->getValue();
                $products[] = [
                    'name_1c' => $productName,
                    'id_1c' => $id1c,
                    'article' => !in_array($article, self::POSSIBLE_NULL_VALUES) ? $article : null,
                    'remainder' => !in_array($remainder, self::POSSIBLE_NULL_VALUES) ? $remainder : null,
                    'price' => !in_array($price, self::POSSIBLE_NULL_VALUES) ? $price : null,
                    'brand' => !in_array($brand, self::POSSIBLE_NULL_VALUES) ? $brand : null,
                    'category' => !in_array($category, self::POSSIBLE_NULL_VALUES) ? $category : null,
                    'measure' => !in_array($measure, self::POSSIBLE_NULL_VALUES) ? $measure : null
                ];
            }
        }

        $this->result = $products;
    }
}
