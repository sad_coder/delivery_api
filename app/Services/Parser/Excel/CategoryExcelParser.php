<?php


namespace App\Services\Parser\Excel;


use Illuminate\Support\Facades\App;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class CategoryExcelParser extends BaseExcelParser
{
    public function __construct(string $excelPath, string $columnName = self::CATEGORY_COLUMN_COORDINATE)
    {
        parent::__construct(App::make(Xlsx::class));

        $this->excelPath = $excelPath;
        $this->columnName = $columnName;

        $this->initSpreadsheet();
    }

    public function parse()
    {
        $categories = [];

        for ($i = 2; $i < $this->highestRow; $i++) {
            $categoryName = $this->spreadsheet->getCell($this->columnName . $i)->getValue();

            if (
                array_search($categoryName, array_column($categories, 'name')) === false &&
                !in_array($categoryName, self::POSSIBLE_NULL_VALUES)
            ) {
                $brandName = $this->spreadsheet->getCell(BrandExcelParser::BRANDS_COLUMN_COORDINATE . $i)->getValue();

                $categories[] = [
                    'name' => $categoryName,
                    'brand_name' => !in_array($brandName, self::POSSIBLE_NULL_VALUES) ? $brandName : null
                ];
            }
        }

        $this->result = $categories;
    }
}
