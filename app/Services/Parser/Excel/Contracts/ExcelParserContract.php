<?php


namespace App\Services\Parser\Excel\Contracts;


interface ExcelParserContract
{
    public function parse();
}
