<?php


namespace App\Services\Parser\Excel;


use Illuminate\Support\Facades\App;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class BrandExcelParser extends BaseExcelParser
{
    public function __construct(string $excelPath, string $columnName = self::BRANDS_COLUMN_COORDINATE)
    {
        parent::__construct(App::make(Xlsx::class));

        $this->excelPath = $excelPath;
        $this->columnName = $columnName;

        $this->initSpreadsheet();
    }

    public function parse()
    {
        $this->parseOneField();
    }
}
