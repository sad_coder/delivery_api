<?php


namespace App\Services\Parser\Excel;


use App\Services\Parser\Excel\Contracts\ExcelParserContract;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

abstract class BaseExcelParser implements ExcelParserContract
{
    const POSSIBLE_NULL_VALUES = [
        '#NULL!',
        null
    ];

    const BRANDS_COLUMN_COORDINATE = 'F';
    const CATEGORY_COLUMN_COORDINATE = 'G';
    const MEASURE_COLUMN_COORDINATE = 'H';
    const PRODUCT_COLUMN_COORDINATE = 'C';
    const ID_1C_COLUMN_COORDINATE = 'A';
    const ARTICLE_COLUMN_COORDINATE = 'B';
    const REMAINDER_COLUMN_COORDINATE = 'D';
    const PRICE_COLUMN_COORDINATE = 'E';

    public $result;
    public $excelPath;
    public $columnName;
    public $xslxReader;
    public $spreadsheet;
    public $highestRow;

    public function __construct(Xlsx $xlsxReader)
    {
        $this->xslxReader = $xlsxReader;
    }

    public function initSpreadsheet()
    {
        $this->spreadsheet = $this->xslxReader->load($this->excelPath)->getActiveSheet();
        $this->highestRow = $this->spreadsheet->getHighestRow($this->columnName);
    }

    public function parseOneField()
    {
        $items = [];

        for ($i = 2; $i < $this->highestRow; $i++) {
            $item = $this->spreadsheet->getCell($this->columnName.$i)->getValue();

            if (!in_array($item, self::POSSIBLE_NULL_VALUES) && !in_array($item, $items))
                $items[] = $item;
        }
        $this->result = $items;
    }
}
