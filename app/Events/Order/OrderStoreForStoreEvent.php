<?php

namespace App\Events\Order;

use App\Services\DataTransfer\Order\StoreOrderData;
use App\Services\DataTransfer\Store\StoreStoreData;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderStoreForStoreEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $orderData;
    public $storeData;

    public function __construct(StoreOrderData $orderData, StoreStoreData $storeData)
    {
        $this->orderData = $orderData;
        $this->storeData = $storeData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
