<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderUpdateForStoreEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order_id;

    public $store_id;

    public $basket;

    public $shopAddress;

    public $deliveryData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $order_id, int $store_id, array $basket, ?string $shopAddress, ?array $deliveryData)
    {
        $this->order_id = $order_id;
        $this->store_id = $store_id;
        $this->basket = $basket;
        $this->shopAddress = $shopAddress;
        $this->deliveryData = $deliveryData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
