<?php

namespace App\Events\BonusGame;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StoreBonusGameEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $mobileId;

    public $sumWin;

    public $gameId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $mobileId, string $sumWin, string $gameId)
    {
        $this->mobileId = $mobileId;
        $this->sumWin = $sumWin;
        $this->gameId = $gameId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
