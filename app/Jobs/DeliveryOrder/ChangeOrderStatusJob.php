<?php

namespace App\Jobs\DeliveryOrder;

use App\Models\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ChangeOrderStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order;

    private $status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, int $status)
    {
        $this->order = $order;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->order->status = $this->status;

            if ($this->status == Order::STATUS_DELIVERED) {
                $this->order->delivered_at = now();
            }

            $this->order->save();
        } catch (Exception $e) {
            logger('ChangeOrderStatusJob error');
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }
}
