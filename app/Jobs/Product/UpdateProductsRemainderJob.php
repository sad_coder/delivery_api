<?php

namespace App\Jobs\Product;

use App\Models\Order;
use App\Models\Product;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateProductsRemainderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;

        foreach ($order->mergeBasketWithProducts() as $product) {
            $newRemainder = $product->remainder - $product->count;
            $newRemainder = $newRemainder > 0 ? $newRemainder : 0;
//            if ($newRemainder < 0) {
//                throw new Exception('The remainder is below than zero does not passing validation, please retry with another amounts', 417);
//            }

            Product::query()->where('id',$product->id)->update([
               'remainder' => $newRemainder,
            ]);


            logger("Product with id:{$product->id} has new remainder : $newRemainder");
        }
    }
}
