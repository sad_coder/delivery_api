<?php

namespace App\Jobs;

use App\Models\ProductStoreCoordinate;
use Illuminate\Bus\Queueable;
use Exception;
use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductStoreCoordinateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $basket;

    private $longitude;

    private $latitude;

    public function __construct(string $latitude, array $basket, string $longitude)
    {
        $this->basket = $basket;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            ProductStoreCoordinate::query()->create([
                'basket' => ($this->basket),
            ]);
        } catch (Exception $e) {
            logger('StoreOrderJob error');
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }
}
