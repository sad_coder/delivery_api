<?php

namespace App\Jobs\BonusGame;

use App\Models\BonusGame;
use App\Models\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreBonusGameJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $mobileId;

    private $sumWin;

    private $gameId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $mobileId, string $sumWin, string $gameId)
    {
        $this->mobileId = $mobileId;
        $this->sumWin = $sumWin;
        $this->gameId = $gameId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $order = Order::where('mobile_id', $this->mobileId)
                ->with('store')
                ->latest()
                ->first();



            BonusGame::query()->create([
                'mobile_id' => $this->mobileId,
                'sum_win' => $this->sumWin,
                'game_id' => $this->gameId,
                'store_id' => $order ? ( $order->store ? $order->store->id : null) : null
            ]);
        } catch (Exception $e) {
            logger('StoreBonusGameJob error');
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }
}
