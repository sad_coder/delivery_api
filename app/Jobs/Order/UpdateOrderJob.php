<?php

namespace App\Jobs\Order;

use App\Models\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orderId;

    private $basket;

    private $address;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $orderId, array $basket, ?array $address)
    {
        $this->orderId = $orderId;
        $this->basket = $basket;
        $this->address = $address;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $order = Order::find($this->orderId);

            $order->update([
                'basket' => $this->basket,
                'address' => $this->address ? json_encode($this->address) : $order->address,
            ]);
        } catch (Exception $e) {
            logger('UpdateOrderJob error');
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }
}
