<?php

namespace App\Jobs\Order;

use App\Jobs\Product\UpdateProductsRemainderJob;
use App\Models\Order;
use App\Services\DataTransfer\Order\StoreOrderData;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orderData;

    public function __construct(StoreOrderData $orderData)
    {
        $this->orderData = $orderData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {
        try {
            $order = Order::query()
                ->create($this->orderData->getResultingArray());

            if ($order->user->counterparty) {
                dispatch(new UpdateProductsRemainderJob($order))->onConnection('sync');
            }
        } catch (Exception $e) {
            logger('StoreOrderJob error');
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    public function failed()
    {
        logger('FAILED LOLKEK IN CLASS CHECK');
    }
}
