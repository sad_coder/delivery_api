<?php

namespace App\Jobs\Store;

use App\Jobs\Order\StoreOrderJob;
use App\Jobs\Product\UpdateProductsRemainderJob;
use App\Models\Order;
use App\Models\Store;
use App\Services\DataTransfer\Order\StoreOrderData;
use App\Services\DataTransfer\Store\StoreStoreData;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class StoreStoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $storeData;

    private $orderData;

    public $storeId;

    public $event;

    public function __construct(StoreStoreData $storeData, StoreOrderData $orderData, $event)
    {
        $this->storeData = $storeData;
        $this->orderData = $orderData;
        $this->event = $event;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->storeData;
        try {
            $store = Store::where('phone', $this->storeData->phone)
                ->orWhere(function ($q) use ($data) {
                    $q->where('address', $data->phone)
                        ->where('name', $data->name);
                })
                ->first();

            if (!$store) {
                $storeId = Store::query()->create($this->storeData->getResultingArray())->id;
            } else {
                $storeId = $store->id;
            }
            $this->orderData->set('store_id', $storeId);
            dispatch(new StoreOrderJob($this->orderData))->onConnection('sync');
        } catch (Exception $e) {
            logger("StoreStoreJob error");
            logger($e->getMessage());
            $this->event->is_failed = true;
            throw new Exception($e->getMessage());
        }

    }

    public function getStoreId()
    {
        return $this->storeId;
    }
}
