<?php

namespace App\Jobs\Store;

use App\Models\Store;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateStoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $shopAddress;

    private $storeId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $storeId, ?string $shopAddress)
    {
        $this->storeId = $storeId;
        $this->shopAddress = $shopAddress;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $store = Store::find($this->storeId);
            if ($this->shopAddress) {
                $store->update([
                    'address' => $this->shopAddress
                ]);
            }
        } catch (Exception $e) {
            logger('UpdateStoreJob error');
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }
}
