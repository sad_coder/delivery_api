<?php


namespace App\Filters\Store;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\Filters\Filter;

class StoreSortByWeekDayFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->where(DB::raw("extract(isodow from created_at)"), $value);
    }
}
