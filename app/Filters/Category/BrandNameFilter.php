<?php

namespace App\Filters\Category;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class BrandNameFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereHas('brand', function (Builder $q) use ($value) {
            $q->where('name', 'LIKE', '%' . $value . '%');
        });
    }
}
