<?php


namespace App\Filters\Waypoint;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\Filters\Filter;

class OrderWaypointStatusFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereHas('order', function ($q) use ($value){
            return $q->where('status',$value);
        });
    }
}
