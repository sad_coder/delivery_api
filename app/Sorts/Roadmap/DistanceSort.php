<?php

namespace App\Sorts\Roadmap;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Sorts\Sort;

class DistanceSort implements Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        return $query->with(['waypoints' => function ($query) use ($descending, $property) {
            $direction = $descending ? 'DESC' : 'ASC';

            $query->orderByRaw(" distance $direction");
        }]);
    }
}
