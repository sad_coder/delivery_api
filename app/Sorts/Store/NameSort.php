<?php

namespace App\Sorts\Store;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Sorts\Sort;

class NameSort implements Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $dir = $descending ? 'DESC' : 'ASC';
        return $query->orderBy('name', $dir);
    }
}
