<?php


namespace App\Traits;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait SearchableByCommonCols
{
    public function scopeWhereBetweenDates(Builder $query, $from, $to)
    {
        return $query->where('created_at', '>=', Carbon::parse($from)->toDateString())
            ->where('created_at', '<=', Carbon::parse($to)->addDay()->toDateString());
    }
}
