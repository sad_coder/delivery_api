<?php


namespace App\Traits;


trait ModelTableNameTrait
{
    public static function getTableName()
    {
        return (new self())->getTable();
    }

}
