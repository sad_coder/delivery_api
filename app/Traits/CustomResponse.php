<?php


namespace App\Traits;


use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait CustomResponse
{
    public function cresponse($message = null, $data = [], $code = Response::HTTP_OK)
    {
        return new JsonResponse([
            'result_code' => $code,
            'result_message' => $message,
            'data' => $data
        ]);
    }
}
