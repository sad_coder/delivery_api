<?php

namespace App\Providers;

use App\Events\BonusGame\StoreBonusGameEvent;
use App\Events\Order\OrderStoreForStoreEvent;
use App\Events\Order\OrderUpdateForStoreEvent;
use App\Listeners\BonusGame\StoreBonusGameListener;
use App\Listeners\Order\OrderStoreForStoreListener;
use App\Listeners\Order\OrderUpdateForStoreListener;
use App\Models\Order;
use App\Observers\OrderObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        OrderStoreForStoreEvent::class => [
            OrderStoreForStoreListener::class
        ],
        StoreBonusGameEvent::class => [
            StoreBonusGameListener::class
        ],
        OrderUpdateForStoreEvent::class => [
            OrderUpdateForStoreListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);
    }
}
