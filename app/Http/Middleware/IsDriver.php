<?php

namespace App\Http\Middleware;

use App\Traits\CustomResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class IsDriver
{
    use CustomResponse;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->isDriver()) {
            return $next($request);
        } else {
            return $this->cresponse('This action is not authorized', null, Response::HTTP_FORBIDDEN);
        }
    }
}
