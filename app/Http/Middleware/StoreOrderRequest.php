<?php

namespace App\Http\Middleware;

use App\Models\OrderRequest;
use Closure;
use Illuminate\Http\Request;

class StoreOrderRequest
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        OrderRequest::handle($request->mobile_id,
            [
                'basket' => $request->basket,
                'deliveryData' => $request->deliveryData,
                'shop_name' => $request->shop_name,
                'shop_addr' => $request->shop_addr,
                'shop_phone' => $request->shop_phone
            ]);

        return $next($request);
    }
}
