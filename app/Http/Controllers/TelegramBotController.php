<?php

namespace App\Http\Controllers;

use App\Http\Requests\TelegramBot\SendPhoneNumberAndOrderCostRequest;
use App\Http\Requests\TelegramBot\SendOfficialStoreMessageRequest;
use App\Models\TelegramMailing;
use App\Models\TelegramUser;
use App\Services\TelegramBot\BaseTelegramBotProcessor;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Longman\TelegramBot\Telegram;
use Symfony\Component\HttpFoundation\Response;

class TelegramBotController extends Controller
{
    protected $telegram;

    public function __construct(Telegram $telegram)
    {
        $this->telegram = $telegram;
    }

    public function startProductRemaindersUpdateJob()
    {
        ini_set('max_execution_time', 0);

        Artisan::call('update:products:remainder');

        return $this->cresponse('Product remainders updated successfully');
    }

    public function getUpdate(Request $request)
    {
        Log::info(json_encode($request->all()));;

        $processor = new BaseTelegramBotProcessor($request->all());

        $processor->processUpdate();

//        Log::info($processor->message_id);
//        Log::info($processor->text);
//        Log::info($processor->callback_query);
//        Log::info($processor->chat_id);
//        Log::info($processor->first_name);
//        Log::info($processor->username);

        return true;
    }

    public function setWebhook(Request $request)
    {
        $url = $request->url;
        $token = config('telegrambot.api_key');

        $client = new Client();
        $response = $client->get("https://api.telegram.org/bot$token/setWebhook?url=$url");

        return $response;
    }

    public function sendOfficialStoreMessage(SendOfficialStoreMessageRequest $request)
    {
        $chatId = TelegramUser::getRelatedUserIds('store_report_official');

        $fullname = Auth::user()->full_name;

        $message = "Название точки: $request->name \nБИН: $request->bin\nОт пользователя: $fullname  \n Отсрочка платежа $request->delay дней";

        $filename = $request->image->getClientOriginalName();

        try {
            $dirPath = Storage::disk('public')->path('stores/');

            if (!is_dir($dirPath)) {
                mkdir($dirPath);
            }

            Storage::disk('public')->putFileAs("stores/", $request->image, $filename);

            $path = Storage::disk('public')->path("stores/$filename");

            Artisan::call("tgbot:send:message $chatId --message='$message' --filepath=$path");
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), $e->getTrace(), Response::HTTP_EXPECTATION_FAILED);
        }

        return $this->cresponse('Message sent successfully!');
    }

    public function driverSendPhoneNumberAndOrderCostMessage(SendPhoneNumberAndOrderCostRequest $request)
    {
        $user = Auth::user()->full_name;

        $message = "Номер телефона: $request->phone_number \nСтоимость зааказа: $request->order_cost KZT\nСообщение от: <b>$user</b>";

        $chatIds = TelegramUser::getRelatedUserIds('driver_phone_cost_message');

        Artisan::call("tgbot:send:message $chatIds --message='$message'");

        return $this->cresponse('Message sent successfully!');
    }
}
