<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRepresentativeSetting\GetSettingBySlugAndUserIdRequest;
use App\Models\StoreRepresentativeSetting;
use App\Models\User;
use App\Services\Redis\SettingRedisService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StoreRepresentativeSettingController extends Controller
{
    public function getSetting(GetSettingBySlugAndUserIdRequest $request)
    {
        $data = SettingRedisService::getSettingBySlugAndUser($request->slug, $request->user_id);

        if (!count($data)) {
            $setting = User::find($request->user_id)
                ->storeRepresentativeSettings()
                ->where('slug', $request->slug)
                ->first();

            if (!$setting) {
                return $this->cresponse('Setting does not belong to user', null, Response::HTTP_FAILED_DEPENDENCY);
            }

            $data = $setting->data;
        }

        return $this->cresponse('Setting found', $data);
    }

    public function allSettings(User $user)
    {
        $settings = $user->storeRepresentativeSettings()->get();

        return $this->cresponse('All settings', $settings);
    }
}
