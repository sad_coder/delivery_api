<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreRepresentativeController extends Controller
{
    public function getSettings()
    {
        $settings = Auth::user()->storeRepresentativeSettings()
            ->get();

        return $this->cresponse('All settings',$settings);
    }
}
