<?php

namespace App\Http\Controllers;

use App\Models\Street;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StreetController extends Controller
{
    public function index(Request $request)
    {
        $streets = QueryBuilder::for(Street::class)
            ->select('id', 'name_ru', 'name_kz', 'region_id')
            ->allowedFilters([
                AllowedFilter::callback('name', function ($query, $value) {
                    $query->where('name_ru', 'LIKE', "%$value%")
                        ->orWhere('name_kz', 'LIKE', "%$value%");
                })
            ])
            ->with('region:id,name_ru,name_kz')
            ->paginate($request->per_page ?? 10);

        return $this->cresponse("Found streets", $streets);
    }
}
