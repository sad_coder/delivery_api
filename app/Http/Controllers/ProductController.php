<?php

namespace App\Http\Controllers;

use App\Filters\Category\BrandNameFilter;
use App\Http\Controllers\Controller;
use App\Models\BonusGame;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductController extends Controller
{
    public function filterProduct()
    {
        $products = QueryBuilder::for(Product::class)
            ->allowedFilters(['name'])
            ->paginate(10);

        return $products;
    }

    public function filterRemainderProduct(Request $request)
    {
        $products = QueryBuilder::for(Product::class)
            ->allowedFilters(
                AllowedFilter::exact('brand_id'),
                AllowedFilter::exact('category_id'),
                AllowedFilter::exact('id'),
            )->select('id', 'remainder')
            ->get();
        $productForFrontEnd = [];
        //@TODO пишу для фронтеда дичь конечно но мне так очень просто жить
        // @TODO потому что у меня все данные кроме остатка берутся из локальной базы данных
        foreach ($products as $product) {
            $productForFrontEnd[$product['id']] = (float)$product['remainder'];
        }


        return $this->cresponse('All products remainders', $productForFrontEnd);
    }

    public function listReport()
    {
        $orders = Order::query()->get();


        return view('admin.orders.list', compact('orders'));
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {

        $products = QueryBuilder::for(Product::class)
            ->allowedFilters(
                AllowedFilter::partial('name'),
                AllowedFilter::exact('brand_id'),
                AllowedFilter::exact('category_id'),
                AllowedFilter::exact('id'),
            )
            ->paginate($request->get("per_page", 10));

        return $this->cresponse('All products', $products);
    }

    /**
     * @param Request $request
     */
    public function allItems(Request $request)
    {
        $products = QueryBuilder::for(Product::class)
            ->allowedFilters(
                AllowedFilter::partial('name'),
                AllowedFilter::exact('brand_id'),
                AllowedFilter::exact('category_id'),
                AllowedFilter::exact('id'),
            )
            ->allowedFields([
                'id',
                'id_1c',
                'article',
                'brand_id',
                'category_id',
                'measure_id',
                'name_1c',
                'name',
                'image',
                'audio',
                'price',
                'description',
                'remainder',
                'created_at',
                'updated_at',
                'rating',
                'presale_id',
                'barcode'
            ])
            ->get();

        $products->map(function ($product) {
            $product->image = $product->image_optimized;
        });

        return $this->cresponse('all product without pagination', $products);
    }


    public function gameReport(Request $request)
    {
        $orders = Order::query()->with('store')->get();
        $res = [];
        foreach ($orders as $order) {
            $orderBonusSum = BonusGame::query()->where('mobile_id', $order->mobile_id)->sum('sum_win');
            $order->sum_win_all = $orderBonusSum;

        }
        dd($orders->where('sum_win_all', '<>', null)->get());


    }


}
