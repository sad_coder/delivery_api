<?php

namespace App\Http\Controllers;

use App\Events\Order\OrderStoreForStoreEvent;
use App\Events\Order\OrderUpdateForStoreEvent;
use App\Http\Requests\Order\StoreOrderForStoreMultipleRequest;
use App\Http\Requests\Order\StoreOrderForStoreRequest;
use App\Http\Requests\Order\UpdateOrderBasketRequest;
use App\Http\Requests\Order\UpdateOrderForStoreRequest;
use App\Models\Order;
use App\Services\DataTransfer\DataTransfer;
use App\Services\DataTransfer\Order\StoreOrderData;
use App\Services\DataTransfer\Store\StoreStoreData;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function storeOrderForStore(StoreOrderForStoreRequest $request, StoreOrderData $orderData, StoreStoreData $storeData)
    {
        try {

            $dataTransfer = new DataTransfer($request);

            $dataTransfer->setData($orderData);

            $dataTransfer->setData($storeData);

            event(new OrderStoreForStoreEvent($orderData, $storeData));
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), null, Response::HTTP_EXPECTATION_FAILED);
        }

        return $this->cresponse('Successfully stored order', ['mobile_id' => $request->mobile_id], Response::HTTP_CREATED);
    }

    public function updateOrderForStore(UpdateOrderForStoreRequest $request)
    {
        try {
            event(new OrderUpdateForStoreEvent($request->order_id, $request->store_id, $request->basket, $request->shop_addr, $request->delivery_data));
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), null, Response::HTTP_EXPECTATION_FAILED);
        }

        return $this->cresponse('Successfully updated order');
    }

    public function updateBasket(Order $order, UpdateOrderBasketRequest $request)
    {
        DB::beginTransaction();

        try {
            $order->basket = $request->basket;
            $order->save();
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), null, Response::HTTP_EXPECTATION_FAILED);
        }

        DB::commit();

        return $this->cresponse('Successfully updated order basket');
    }

    public function storeOrderForStoreMultiple(StoreOrderForStoreMultipleRequest $request)
    {

        try {
            foreach ($request->orders as $order) {
                event(new OrderStoreForStoreEvent($order['basket'], $order['shop_addr'], $order['delivery_data']));
            }
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), null, Response::HTTP_EXPECTATION_FAILED);
        }

        return $this->cresponse('Successfully stored orders');
    }

    public function removeOrder(Request $request)
    {
        $order = Order::query()->with('bonusGames')->where('mobile_id', $request->get('mobile_id'))->first();

        if ($order && !$order->report) {
            //@TODO проверить casace delete чуть позже добавлен ли он
            foreach ($order->bonusGames as $game) {
                $game->delete();
            }
            $order->delete();

            return $this->cresponse('success', []);
        }

        return $this->cresponse('Order can not be deleted or already deleted', null,Response::HTTP_CONFLICT);

    }

}
