<?php

namespace App\Http\Controllers;

use App\Filters\Waypoint\OrderWaypointStatusFilter;
use App\Http\Requests\DeliveryOrder\ChangeOrderStatusRequest;
use App\Http\Resources\Order\DeliveryOrdersDataResource;
use App\Http\Resources\Order\DeliveryOrdersResource;
use App\Jobs\DeliveryOrder\ChangeOrderStatusJob;
use App\Models\Order;
use App\Models\Waypoint;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Symfony\Component\HttpFoundation\Response;

class DeliveryController extends Controller
{
    public function index(Request $request)
    {
        $roadmap = Auth::user()->roadmaps()
            ->latest('id')
            ->first();

        if (!$roadmap) {
            return $this->cresponse('No roadmaps are generated yet', null, Response::HTTP_EXPECTATION_FAILED);
        }

        $waypoints = QueryBuilder::for(Waypoint::class)
            ->allowedFilters(
                AllowedFilter::custom('status', new OrderWaypointStatusFilter())

            )
            ->with(['order' => function ($q) {
                $q->with(["user.counterparty",'store','bonusGames', 'user:id,phone_number']);
            }])
            ->defaultSort('distance')
            ->where('roadmap_id', $roadmap->id)
            ->get();

        return $this->cresponse('All waypoints of driver', new DeliveryOrdersDataResource((object)['waypoints' => $waypoints]));
    }

    public function changeDeliveryOrderStatus(Order $order, ChangeOrderStatusRequest $request)
    {
        if (!Auth::user()->driverOrders()->where('orders.id', $order->id)->exists()) {
            return $this->cresponse('Order not found in that courier orders', null, Response::HTTP_NOT_FOUND);
        }

        try {
            dispatch(new ChangeOrderStatusJob($order, $request->status));
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), null, Response::HTTP_EXPECTATION_FAILED);
        }
        return $this->cresponse('Successfully changed status');
    }

    public function getDeliveredOrders()
    {
        $orders = Auth::user()
            ->driverOrders()
            ->delivered()
            ->get();

        return $this->cresponse('Delivered orders', $orders);
    }
}
