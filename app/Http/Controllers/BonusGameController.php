<?php

namespace App\Http\Controllers;

use App\Events\BonusGame\StoreBonusGameEvent;
use App\Http\Requests\BonusGame\StoreBonusGameRequest;
use App\Models\BonusGame;
use Exception;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Symfony\Component\HttpFoundation\Response;

class BonusGameController extends Controller
{
    public function index()
    {
        $bonusGames = QueryBuilder::for(BonusGame::class)
            ->allowedFilters('sum_win', 'store_id')
            ->paginate(request('per_page' ?? 10));

        return $this->cresponse('All bonus games', $bonusGames);
    }

    public function store(StoreBonusGameRequest $request)
    {
        try {
            event(new StoreBonusGameEvent($request->mobile_id, $request->sum_win, $request->game_id));
        } catch (Exception $e) {
            return $this->cresponse($e->getMessage(), null, Response::HTTP_EXPECTATION_FAILED);
        }
        return $this->cresponse('Bonus game stored', ['mobile_id' => $request->mobile_id], 201);
    }
}
