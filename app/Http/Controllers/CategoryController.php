<?php

namespace App\Http\Controllers;

use App\Filters\Category\BrandNameFilter;
use App\Models\Category;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryController extends Controller
{

    public function filterCategory()
    {
        $categories = QueryBuilder::for(Category::class)
            ->allowedFilters(['name'])
            ->paginate(10);

        return $categories ;
    }

    public function index()
    {
        $categories = QueryBuilder::for(Category::class)
            ->allowedFilters(
                AllowedFilter::partial('name'),
                AllowedFilter::exact('brand_id'),
                AllowedFilter::exact('id'),
                AllowedFilter::custom('brand_name', new BrandNameFilter())
            )
            ->get();

        return $this->cresponse('All categories', $categories);
    }


}
