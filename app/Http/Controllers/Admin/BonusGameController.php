<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BonusGame;
use App\Models\Order;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BonusGameController extends Controller
{
    public function allGameHistory()
    {
        $orders = QueryBuilder::for(Order::class)
            ->with('bonusGames')
            ->allowedFilters(
                AllowedFilter::partial('mobile_id'),
                AllowedFilter::scope('bonusGames.whereBetweenDates')
            )
            ->withSum('bonusGames', 'sum_win')
            ->whereHas('bonusGames')
            ->get();

        return view('admin.games.index',compact('orders'));
    }

    public function search(Request $request)
    {
        $filters = [];

        if (isset($request->dateFrom)) {
            $filters['filter[bonusGames.whereBetweenDates]'] = $request->dateFrom . ',' . $request->dateTo;
        }

        return redirect()->route('admin.game.allGameHistory', $filters);
    }
}
