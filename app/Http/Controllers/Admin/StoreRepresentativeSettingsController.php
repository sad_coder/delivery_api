<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StoreRepresentativeSetting;
use Illuminate\Http\Request;

class StoreRepresentativeSettingsController extends Controller
{
    public function index()
    {
        $settings = StoreRepresentativeSetting::all();

        return view('admin.store-rep-settings.index',compact('settings'));
    }

    public function edit(StoreRepresentativeSetting $storeRepresentativeSetting)
    {
        return view('admin.store-rep-settings.edit',[
            'setting' => $storeRepresentativeSetting
        ]);
    }

    public function update(Request $request, StoreRepresentativeSetting $storeRepresentativeSetting)
    {
        $data = [
            'enabled' => $request->input('enabled'),
            'description' => $request->input('description'),
            'min_prize' => $request->input('min_prize')
        ];

        $storeRepresentativeSetting->slug = $request->input('slug');
        $storeRepresentativeSetting->data = ($data);

        $storeRepresentativeSetting->save();

        return redirect()->back()->withSuccess('Настройка успешно изменено');
    }

    public function create()
    {
        $settings = StoreRepresentativeSetting::all();
        return view('admin.store-rep-settings.create',compact('settings'));
    }

    public function store(Request $request, StoreRepresentativeSetting $storeRepresentativeSetting)
    {
        $data = [
            'enabled' => $request->enabled,
            'description' => $request->description,
            'min_prize' => $request->min_prize
        ];

        $storeRepresentativeSetting->slug = $request->slug;
        $storeRepresentativeSetting->data = ($data);

        $storeRepresentativeSetting->save();

        return redirect()->route('admin.store-rep-settings.index');
    }

}
