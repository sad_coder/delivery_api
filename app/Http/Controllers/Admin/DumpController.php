<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DumpController extends Controller
{
    const DUMPS_DIR = '/home/dev-pd/dumps/';

    const TEST_DIR = '/var/www/boszhan-delivery-service/repositories/boszhan_delivery/storage/app/public/catalogues/';

    public function index()
    {
        $dumps = scandir(self::DUMPS_DIR);

        foreach ($dumps as $key => $dump) {
            if (!str_starts_with($dump,'backup')) {
                unset($dumps[$key]);
            }
        }
        return view('admin.dumps.index', compact('dumps'));
    }

    public function download($dumpName)
    {
        $file = self::DUMPS_DIR.$dumpName;

        if (file_exists($file)) {
            return response()->download($file, $dumpName);
        }
    }
}
