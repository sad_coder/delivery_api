<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Counterparty;
use App\Models\StoreRepresentativeSetting;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Redis\SettingRedisService;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;


class UserController extends Controller
{
    public function index()
    {
        $users = QueryBuilder::for(User::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('role'),
                AllowedFilter::partial('full_name')
            ])
            ->get();

        return view('admin.users.index', compact('users'));
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', [
            'user' => $user
        ]);
    }

    public function create()
    {
        $settings = StoreRepresentativeSetting::all();
        $roles = json_decode(json_encode(User::ROLES), false);

        return view('admin.users.create', compact('settings', 'roles'));
    }

    public function store(StoreUserRequest $request, User $user)
    {
        DB::beginTransaction();
        try {
            $user->full_name = $request->input('full_name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->role = $request->input('role');
            $user->save();

            if ($request->input('settingIds')) {
                $user->storeRepresentativeSettings()
                    ->sync($request->input('settingIds'));


                $settings = StoreRepresentativeSetting::whereIn('id', $request->input('settingIds'))
                    ->get();

                foreach ($settings as $setting) {
                    SettingRedisService::setSettingBySlugAndUser($setting, $user->id);
                }

            }

            if ($request->input('is_counterparty')) {
                $user->counterparty()->create([
                    'id_1c' => Counterparty::generateId1C(),
                    'name_1c' => $request->input('name_1c')
                ]);
            }
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
        DB::commit();

        return redirect()->route('admin.user.index')->withSuccess('Пользователь успешно добавлен');
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->full_name = $request->input('full_name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');

        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();

        return redirect()->back()->withSuccess('Пользователь успешно изменен');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back()->withSuccess('Пользователь успешно удален');
    }
}
