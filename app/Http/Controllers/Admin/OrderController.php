<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Models\BonusGame;
use App\Models\OrderInfo;
use App\Models\OrderRequest;
use App\Models\Order;
use App\Models\Product;
use App\Models\Store;
use App\Models\User;
use Carbon\Carbon;
use Doctrine\DBAL\Query;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Models\Audit;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrderController extends Controller
{
    public function index()
    {
        $orders = QueryBuilder::for(Order::class)
            ->withSum('bonusGames', 'sum_win')
            ->withCount('bonusGames')
            ->whereDate('created_at', '>', now()->subDays(7))
            ->allowedFilters(
                AllowedFilter::trashed(),
                AllowedFilter::exact('store_id'),
                AllowedFilter::exact('user_id'),
                AllowedFilter::exact('id'),
                AllowedFilter::exact('status'),
                AllowedFilter::scope('whereBetweenDates')
            )
            ->get();

        $totalCost = 0;
        $totalReturnsCost = 0;
        $totalBonusGamesCost = 0;

        foreach ($orders as $order) {
            if ($order->user->counterparty) {
                $totalCost += $order->total_cost;
                $totalReturnsCost += $order->total_returns_cost;
                $totalBonusGamesCost += $order->bonus_games_sum_sum_win;
            }
        }

        return view('admin.orders.index', compact('orders', 'totalCost', 'totalReturnsCost', 'totalBonusGamesCost'));
    }

    public function basket(Order $order)
    {
        return view('admin.orders.orders-basket', ['products' => $order->mergeBasketWithProducts()]);
    }

    public function unknownOrderBasket(OrderRequest $request)
    {
        $order = new Order();

        $order->basket = $request->formatted_payload['basket'];

        return view('admin.orders.orders-basket', ['products' => $order->mergeBasketWithProducts()]);
    }

    public function search(Request $request)
    {
        $filters = [];

        if (isset($request->dateFrom)) {
            $filters['filter[whereBetweenDates]'] = $request->dateFrom . ',' . $request->dateTo;
        }

        if (isset($request->user_id) && is_numeric($request->user_id)) {
            $filters['filter[user_id]'] = $request->user_id;
        }

        return redirect()->route('admin.order.index', $filters);
    }

    public function edit(Order $order)
    {
        $users = User::query()
            ->select('id', 'full_name')
            ->get();

        $stores = Store::query()
            ->select('id', 'address')
            ->get();

        $products = Product::query()
            ->select('id', 'name_1c')
            ->orderBy('name_1c')
            ->get();

        $basketTotalCost = $order->total_cost;

        $statuses = json_decode(json_encode(Order::ALL_STATUSES));

        return view('admin.orders.edit', compact('order', 'users', 'statuses', 'stores', 'products', 'basketTotalCost'));
    }

    public function update(Order $order, UpdateOrderRequest $request)
    {
        DB::beginTransaction();
        try {
            $order->update([
                'user_id' => $request['user_id'],
                'store_id' => $request['store_id'],
                'status' => $request['status'],
                'delivery_time' => $request['delivery_time'],
                'basket' => $request['basket'],
                'address' => $request['address']
            ]);
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
        DB::commit();

        return redirect()->back()->withSuccess('Заказ обновлен');
    }

    public function failedOrders()
    {
        $orders = QueryBuilder::for(OrderRequest::class)
            ->allowedFilters(
                AllowedFilter::callback('dateFrom', function ($query, $value) {
                    $query->where('created_at', '>=', $value);
                }),
                AllowedFilter::callback('dateTo', function ($query, $value) {
                    $query->where('created_at', '<=',  Carbon::parse($value)->addDay()->toDateString());
                })
            )
            ->get();

        return view('admin.orders.failed', compact('orders'));
    }

    public function orderRep(Request $req)
    {
        $order = Order::query()->with('store')->where('id', $req->get('id'))->first();
        $stores = Store::query()->get();
        return view('admin.auth.temp', compact('order'));
    }

    public function audits()
    {
        $audits = QueryBuilder::for(Audit::class)
            ->where('auditable_type', 'App\Models\Order')
            ->where('event', 'updated')
            ->whereHas('auditable')
            ->with(['user', 'auditable'])
            ->allowedFilters(AllowedFilter::exact('auditable_id'))
            ->get();

        return view('admin.orders.audit', compact('audits'));
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->back()->withSuccess('Заказ удален успешно');
    }

    public function downloadReport(Order $order)
    {
        Artisan::call('generate:single:order:report', ['orderIds' => $order->id]);

        $path = json_decode(Artisan::output(), true)[0];

        $name = basename($path);

        return response()->download($path,$name)->deleteFileAfterSend(false);
    }
}
