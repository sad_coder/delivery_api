<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Roadmap;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryController extends Controller
{
    public function index()
    {
        $roadmaps = QueryBuilder::for(Roadmap::class)
            ->with([
                'waypoints' => function($q) {
                    $q->join('orders', 'orders.id', '=', 'waypoints.order_id')
                    ->orderBy('orders.delivered_at', 'ASC');
                },
            ])
            ->allowedFilters([
                AllowedFilter::exact('user_id')
            ])
            ->get();

        return view('admin.delivery-statuses.index', compact('roadmaps'));
    }
}
