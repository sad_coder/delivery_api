<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Scopes\EnabledScope;
use Doctrine\DBAL\Query\QueryBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::withoutGlobalScope(EnabledScope::class)->get();
        return view('admin.brands.index',compact('brands'));
    }

    public function create()
    {
        $brands = Brand::all();
        return view('admin.brands.create2',compact('brands'));
    }

    public function store(Request $request, Brand $brand)
    {
        $brand->name = $request->name;
        $brand->active = $request->active;
        $brand->save();

        return redirect()->route('admin.brand.index');

    }

    public function edit(Brand $brand)
    {
        return view('admin.brands.edit',[
            'brand' => $brand
        ]);
    }

    public function update(Request $request, Brand $brand)
    {

        $brand->name = $request->input('name');
        $brand->save();

        return redirect()->back()->withSuccess('Брэнд успешно изменено');
    }

    public function destroy(Brand $brand)
    {
        $brand->delete();

        return redirect()->back()->withSuccess('Брэнд успешно удалена');
    }

    public function allBrand()
    {
        $brands = Brand::all();
        return response()->json($brands);
    }

    public function filterBrand()
    {
        $brands = QueryBuilder::for(Brand::class)
            ->allowedFilters(['name'])
            ->paginate(10);

        return $brands ;
    }
}
