<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function __construct()
    {
        auth()->setDefaultDriver('web');
    }

    public function login()
    {
        if (Auth::user()) {
            return redirect()->route('admin.user.index');
        }
        return view('admin.auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('admin.user.index');
        }
        return redirect()->back()->withErrors(['message' => 'Login failed, wrong credentials']);
    }
}
