<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StoreRepController extends Controller
{
    public function index()
    {
        $users = QueryBuilder::for(User::class)
            ->where('role', User::ROLE_REPRESENTATIVE)
            ->get();

        return view('admin.store-rep.index', compact('users'));
    }

    public function skuPage()
    {
        $storeReps = User::storeReps()
            ->get();

        $orders = QueryBuilder::for(Order::class)
            ->get();

        foreach ($storeReps as $rep) {
            $stores = $orders->where('user_id', $rep->id)->groupBy('store_id');
            $storesCount = count($stores);
            $allProducts = [];
            foreach ($stores as $store) {
                foreach ($store as $order) {
                    $allProducts = array_merge($allProducts, array_unique(array_column($order->basket, 'product_id')));
                }
            }

            if ($storesCount > 0) {
                $rep->sku = count($allProducts) / $storesCount;
            } else {
                $rep->sku = 0;
            }
        }

        return view('admin.store-rep.sku', compact('storeReps'));
    }
}
