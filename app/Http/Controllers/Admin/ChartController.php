<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ChartController extends Controller
{
    public function storeRepOrderBuys(Request $request)
    {
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;

        if (!$dateFrom && !$dateTo) {
            $dateFrom = now()->subDays(7)->format('Y-m-d');
            $dateTo = now()->format('Y-m-d');
        }

        $userIds = $request->userIds;

        if (!$userIds) {
            return redirect()->back()->withErrors('Пожалуйста отметьте представителей');
        }

        $dataSet = [];
        $labels = [];

        foreach ($userIds as $userId) {
            $data = Order::generateStoreRepOrderBuysStatistics($dateFrom, $dateTo, $userId);
            $dataSet[] = $data;

            $dates = array_column($data['data'],'x');

            $labels = array_merge($labels,$dates);
        }

        $labels = array_unique($labels);

        asort($labels);

        $labels = array_values($labels);

//        dd($labels);
        return view('admin.charts.line-chart', compact('dataSet', 'userIds', 'labels'));

    }
}
