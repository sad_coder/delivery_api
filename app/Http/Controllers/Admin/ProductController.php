<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\UploadHelper;
use App\Http\Requests\Product\UpdateProductsRemainderRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Measure;
use App\Models\Product;
use App\Scopes\EnabledScope;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;
use ZipArchive;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::query()->with(['brand', 'category'])->get();
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $products = Product::all();
        $categories = Category::withoutGlobalScope(EnabledScope::class)->get();
        $brands = Brand::withoutGlobalScope(EnabledScope::class)->get();
        $measures = Measure::query()->get();
        return view('admin.products.create', compact('products','categories','brands','measures'));
    }

    public function store(Request $request, Product $product)
    {
        if ($request->file('images')) {
            UploadHelper::deleteIfExists($product->image);
            $fileName = UploadHelper::uploadOneFile("/photo", $request->file('images')[0]);
        }

        $product->article = $request->article;
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->measure_id = $request->measure_id;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->remainder = $request->remainder;
        $product->image = $fileName ?? null;
        $product->id_1c = $request->id_1c;
        $product->name_1c = $request->name_1c;
        $product->save();

        return redirect()->route('admin.product.index');

    }

    public function edit(Product $product)
    {
        $categories = Category::withoutGlobalScope(EnabledScope::class)->get();
        $brands = Brand::withoutGlobalScope(EnabledScope::class)->get();


        return view('admin.products.edit', [
            'product' => $product,
            'brands' => $brands,
            'categories' => $categories,
            'images' => [[
                "path" => $product->full_image_path,
            ]
            ],
            'audio' => [[
                "path" => $product->full_audio_path,
            ]
            ]
        ]);
    }

    public function update(Request $request, Product $product)
    {
        $product->update(array_filter($request->all()));

        if ($request->file('images')) {
            UploadHelper::deleteIfExists($product->image);
            $fileName = UploadHelper::uploadOneFile("/photo", $request->file('images')[0]);
            $product->image = $fileName;
            $product->save();
        }

        if ($request->file('audio')) {
            $fileName = UploadHelper::uploadOneFile("/audio", $request->file('audio'));
            $product->audio = $fileName;
            $product->save();
        }


        return redirect()->back()->withSuccess('Продукт успешно изменено');

    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->back()->withSuccess('Продукт успешно удалена');
    }

    public function filterProduct()
    {
        $products = QueryBuilder::for(Product::class)
            ->allowedFilters(['name'])
            ->paginate(10);

        return $products;
    }

    public function downloadOptimizedImages()
    {
        $products = Product::whereNotNull('image_optimized')
            ->get();

        $dir = Storage::disk('public')->path('zips/');

        if (!is_dir($dir)) {
            mkdir($dir);
        }

        try {
            $zip = new ZipArchive();
            $zipName = 'optimizedImages.zip';
            $absolutePath = Storage::disk('public')->path('zips');
            $path = $absolutePath . '/' . $zipName;

            if ($zip->open($path, ZipArchive::CREATE) === TRUE) {
                foreach ($products as $product)
                    $zip->addFile(Storage::disk('public')->path("photo-optimized/" . $product->image), $product->image);
                $zip->close();
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return response(Storage::disk('public')->get("/zips/$zipName"), 200)
            ->header('Content-Type', 'application/octet-stream');
    }

    public function updateRemaindersPage()
    {
        return view('admin.products.updateRemainders');
    }

    public function updateProductRemaindersFromExcel(UpdateProductsRemainderRequest $request)
    {
        $file = $request->file('file');

        Storage::disk('storage')->put("inventorization/" . Product::REMAINDERS_UPDATE_FILE_NAME, $file->getContent());

        return redirect()->back()->withSuccess('Остатки продуктов были успешно загружены, запустите команду!');
    }


    public function downloadCatalogue()
    {
        Artisan::call('generate:products:list name_1c price');

        $path = preg_replace('~[\r\n]+~', '', Artisan::output());

        return response()->download($path)->deleteFileAfterSend(true);
    }
}
