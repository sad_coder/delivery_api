<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderReport;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class OrderReportController extends Controller
{
    public function index()
    {
        $dates = OrderReport::select('created_at')
            ->distinct()
            ->pluck('created_at')
            ->toArray();

        return view('admin.order-reports.index', ['dates' => $dates]);
    }

    public function show(string $date)
    {
        $reports = OrderReport::where('created_at', $date)
            ->get();

        return view('admin.order-reports.show', [
            'date' => $date,
            'reports' => $reports
        ]);
    }

    public function downloadFull(string $date)
    {
        $modifiedDate = Carbon::parse($date)->format('Y-m-d');

        if (!Storage::disk('public')->exists('zips')) {
            Storage::disk('public')->makeDirectory('zips');
        }

        $absolutePath = Storage::disk('public')->path('zips');

        $reports = OrderReport::where('created_at', $date)
            ->get();

        try {
            $zip = new ZipArchive();
            $zipFileName = "$modifiedDate.zip";
            $path = $absolutePath . '/' . $zipFileName;

            if ($zip->open($path, ZipArchive::CREATE) === TRUE) {
                foreach ($reports as $report)
                    $zip->addFile(Storage::disk('public')->path($report->path), "Order:$report->order_id.xml");
                $zip->close();
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return response(Storage::disk('public')->get("/zips/$zipFileName"), 200)
            ->header('Content-Type', 'application/octet-stream');

    }

    public function downloadReport(OrderReport $report)
    {
        return Storage::disk('public')->download($report->path);
    }

    public function showReport(OrderReport $report)
    {
        return response(Storage::disk('public')->get($report->path), 200)
            ->header('Content-Type', 'application/xml');
    }
}
