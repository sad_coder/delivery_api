<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TelegramMailing;
use App\Models\TelegramUser;
use Exception;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class TelegramUserController extends Controller
{
    public function index()
    {
        $users = QueryBuilder::for(TelegramUser::class)
            ->with(['user', 'token', 'telegramMailings'])
            ->get();

        $mailings = TelegramMailing::all();

        return view('admin.telegram-users.index', compact('users', 'mailings'));
    }

    public function attachMailings(TelegramUser $user, Request $request)
    {
        try {
            $user->telegramMailings()->sync($request->mailings);
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->back()->withSuccess('Успешно обновлено');
    }
}
