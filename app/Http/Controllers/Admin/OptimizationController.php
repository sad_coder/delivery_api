<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class OptimizationController extends Controller
{
    public function index()
    {
        $products = Product::select('id','name_1c','image','image_optimized')
            ->whereNotNull('image')
            ->get();

        return view('admin.optimization-results.index', compact('products'));
    }
}
