<?php

namespace App\Http\Controllers;

use App\Filters\Store\StoreSortByWeekDayFilter;
use App\Http\Resources\Statistics\Product\StoreMostPopularProductResource;
use App\Models\Order;
use App\Models\Store;
use App\Sorts\Store\NameSort;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class StoreController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $stores = QueryBuilder::for(Store::class)
            ->allowedFilters(
                AllowedFilter::exact('seller_id'),
                AllowedFilter::custom('week_day', new StoreSortByWeekDayFilter())
            )
            ->allowedSorts(
                AllowedSort::custom('name', new NameSort())
            )
            ->get();
        return $this->cresponse('all stores get', $stores);
    }

    public function getOrders(Store $store)
    {
        $orders = QueryBuilder::for(Order::class)
            ->where('store_id', $store->id)
            ->where('user_id', Auth::id())
            ->withCasts([
                'created_at' => 'date:Y-m-d',
                'updated_at' => 'date:Y-m-d'
            ])
            ->allowedFilters(
                AllowedFilter::exact('id')
            )
            ->get();

        return $this->cresponse('all orders of store-rep in store', $orders);
    }

    public function getMostPopularProducts(Store $store)
    {
        return $this->cresponse('Most popular products', StoreMostPopularProductResource::collection($store->most_popular_products));
    }

}
