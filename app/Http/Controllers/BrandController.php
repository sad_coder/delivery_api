<?php


namespace App\Http\Controllers;


use App\Models\Brand;
use App\Traits\CustomResponse;
use Illuminate\Http\JsonResponse;

class BrandController
{
    use CustomResponse;

    public function index()
    {
        $brands = Brand::all();
        return  $this->cresponse('ok',$brands,JsonResponse::HTTP_OK);
    }

}
