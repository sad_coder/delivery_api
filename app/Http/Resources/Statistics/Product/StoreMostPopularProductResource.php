<?php

namespace App\Http\Resources\Statistics\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreMostPopularProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'name_1c' => $this->name_1c,
          'presale_id' => $this->presale_id,
          'times_bought' => $this->times_bought
        ];
    }
}
