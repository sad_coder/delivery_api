<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryOrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        //@TODO переделать через нормальный sql запрос, щас 5 утра уже и я туплю

        $hasBonusGames = $this->order->bonusGames;

        return [
            'id' => $this->id,
            'order_id' => $this->order->id,
            'status' => $this->order->status,
            'store_rep_phone_number' => $this->order->user->phone_number,
            'basket' => $this->order->mergeBasketWithProducts(),
            'delivery_at' => $this->order->created_at->addDay()->format('d-m-Y'),
            'store_name' => $this->order->store->name,
            'store_id' =>$this->order->store->id,
            'store_address' => $this->order->store->address,
            'total_cost' => $this->order->total_cost,
            'total_returns_cost' => $this->order->total_returns_cost,
            'counterparty_name' => $this->order->user->counterparty->name_1c,
            'bonus_game_sum' => $hasBonusGames ? $hasBonusGames->sum('sum_win') : 0,
        ];
    }
}
