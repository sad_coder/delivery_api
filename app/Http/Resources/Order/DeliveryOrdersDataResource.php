<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryOrdersDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $waypoints = DeliveryOrdersResource::collection($this->waypoints);

        $totalCost = 0;
        $totalReturnsCost = 0;

        foreach ($waypoints as $waypoint) {
            $totalCost += $waypoint->order->total_cost;
            $totalReturnsCost += $waypoint->order->total_returns_cost;
        }

        return [
            'data' => $waypoints,
            'total_cost' => $totalCost,
            'total_return_cost' => $totalReturnsCost
        ];
    }
}
