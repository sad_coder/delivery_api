<?php

namespace App\Http\Requests\TelegramBot;

use Illuminate\Foundation\Http\FormRequest;

class SendOfficialStoreMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bin' => 'required',
            'image' => 'required|image',
            'name' => 'required|string'
        ];
    }
}
