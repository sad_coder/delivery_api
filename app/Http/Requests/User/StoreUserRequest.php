<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string',
            'email' => 'required|unique:users',
            'password' => 'required',
            'role' => 'required|integer',
            'settingIds' => 'array',
            'name_1c' => 'required_if:is_counterparty,on'
        ];
    }

    public function messages()
    {
        return [
            "full_name.required" => 'поле ФИО обязательное',
            "email.required" => 'поле Email обязательное',
            "password.required" => 'поле Пароль обязательное'
        ];
    }

}
