<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string',
            'email' => 'required',
            'password' => '',
            'role' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            "full_name.required" => 'поле ФИО обязательное',
            "email.required" => 'поле Email обязательное',
            "password.required" => 'поле Пароль обязательное'
        ];
    }

}
