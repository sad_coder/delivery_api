<?php

namespace App\Http\Requests\BonusGame;

use App\Traits\CustomResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StoreBonusGameRequest extends FormRequest
{
    use CustomResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile_id' => 'required|string',
            'store_id' => 'integer|exists:stores,id',
            'sum_win' => 'required|numeric',
            'game_id' => 'required|unique:bonus_games'
        ];
    }

    public function messages()
    {
        return [
            "mobile_id.required" => 'поле mobile_id обязательное',
            "sum_win.required" => 'поле сумма выигрыша обязательное'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if (isset($validator->failed()['game_id']['Unique'])) {
            throw new ValidationException($validator, $this->cresponse('Bonus game was processed already', null));
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
