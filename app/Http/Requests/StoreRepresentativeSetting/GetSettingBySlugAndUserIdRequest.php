<?php

namespace App\Http\Requests\StoreRepresentativeSetting;

use Illuminate\Foundation\Http\FormRequest;

class GetSettingBySlugAndUserIdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'slug' => 'required|string|exists:store_representative_settings,slug'
        ];
    }

    public function messages()
    {
        return [
            "user_id.required" => 'поле userId обязательное',
            "slug.required" => 'поле slug обязательное',
        ];
    }

}
