<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderBasketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'basket' => 'required|array|min:1',
            'basket.*.product_id' => 'required|exists:products,id',
            'basket.*.type' => 'required|integer|in:0,1',
            'basket.*.count' => 'required|numeric',
        ];
    }
}
