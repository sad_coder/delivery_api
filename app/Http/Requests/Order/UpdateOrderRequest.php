<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'delivery_time' => 'required|string|date_format:H:i',
            'status' => 'required|integer',
            'store_id' => 'required|exists:stores,id',
            'basket' => 'required|array',
            'basket.*.product_id' => 'required|exists:products,id',
            'basket.*.count' => 'required|integer',
            'address.latitude' => 'required|string',
            'address.longitude' => 'required|string'
        ];
    }
}
