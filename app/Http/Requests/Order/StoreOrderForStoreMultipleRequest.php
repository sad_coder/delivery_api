<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderForStoreMultipleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orders' => 'required',
            'orders.*.basket' => 'required|array|min:1',
            'orders.*.basket.*.product_id' => 'required|exists:products,id',
            'orders.*.basket.*.count' => 'required|integer',
            'orders.*.shop_addr' => 'required|string',
            'orders.*.delivery_data' => 'required|array',
            'orders.*.delivery_data.latitude' => 'required|string',
            'orders.*.delivery_data.longitude' => 'required|string',
            'orders.*.delivery_data.delivery_time' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            "orders.required" => 'поле Orders обязательное',
            "orders.*.basket.required" => 'поле Корзина обязательное',
            "orders.*.basket.*.product_id.required" => 'поле basketProductId обязательное',
            "orders.*.basket.*.count.required" => 'поле basketCount обязательное',
            "orders.*.shop_addr.required" => 'поле shopAddr обязательное',
            "orders.*.delivery_data.required" => 'поле deliveryData обязательное',
            "orders.*.delivery_data.latitude.required" => 'поле dataDeliveryLatitude обязательное',
            "orders.*.delivery_data.longitude.required" => 'поле dataLongitude обязательное',
            "orders.*.delivery_data.delivery_time.required" => 'поле dataDeliveryTime обязательное'
        ];
    }

}
