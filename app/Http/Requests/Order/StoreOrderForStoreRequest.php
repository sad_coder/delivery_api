<?php

namespace App\Http\Requests\Order;

use App\Traits\CustomResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class StoreOrderForStoreRequest extends FormRequest
{
    use CustomResponse;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orders.*.basket' => 'required|array|min:1',
            'basket.*.product_id' => 'required|exists:products,id',
            'basket.*.type' => 'required|integer|in:0,1',
            'basket.*.count' => 'required|numeric',
            'shop_addr' => 'required|string',
            'shop_phone' => 'required|string',
            'shop_name' => 'required|string',
            'shop_bin' => 'nullable|string',
            'delivery_data' => 'required|array',
            'delivery_data.latitude' => 'required|string',
            'delivery_data.longitude' => 'required|string',
            'delivery_data.delivery_time' => 'required|string',
            'mobile_id' => 'required|string|unique:orders',
            'payment_at' => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            "basket.required" => 'поле Корзина обязательное',
            "basket.*.product_id.required" => 'поле basketProductId обязательное',
            "basket.*.count.required" => 'поле basketCount обязательное',
            "basket.*.type.required" => 'поле type обязательное',
            "shop_addr.required" => 'поле shopAddr обязательное',
            "delivery_data.required" => 'поле deliveryData обязательное',
            "delivery_data.latitude.required" => 'поле dataDeliveryLatitude обязательное',
            "delivery_data.longitude.required" => 'поле dataLongitude обязательное',
            "delivery_data.delivery_time.required" => 'поле dataDeliveryTime обязательное'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        if (isset($validator->failed()['mobile_id']['Unique'])) {
            throw new ValidationException($validator, $this->cresponse('Order with given hash is already processed', null));
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }

}
