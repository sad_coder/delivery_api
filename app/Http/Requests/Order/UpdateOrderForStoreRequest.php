<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderForStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'store_id' => 'required|exists:stores,id',
            'basket' => 'required|array|min:1',
            'basket.*.product_id' => 'required|exists:products,id',
            'basket.*.count' => 'required',
            'shop_addr' => 'string',
            'delivery_data' => 'array',
            'delivery_data.latitude' => 'string',
            'delivery_data.longitude' => 'string'
        ];
    }

    public function messages()
    {
        return [
            "order_id.required" => 'поле orderId обязательное',
            "store_id.required" => 'поле storeId обязательное',
            "basket.required" => 'поле Корзина обязательное',
            "basket.*.product_id.required" => 'поле basketProductId обязательное',
            "basket.*.count.required" => 'поле basketCount обязательное',
            "shop_addr.required" => 'поле shopAddr обязательное',
            "delivery_data.required" => 'поле deliveryData обязательное',
            "delivery_data.latitude.required" => 'поле dataDeliveryLatitude обязательное',
            "delivery_data.longitude.required" => 'поле dataLongitude обязательное',
        ];
    }

}
