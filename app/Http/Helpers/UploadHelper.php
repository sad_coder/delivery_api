<?php


namespace App\Http\Helpers;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadHelper
{
    /**
     * @param string $path
     * @param UploadedFile $file
     * @return String
     */
    public static function uploadOneFile(string $path, UploadedFile $file) :String
    {
        $randomFileName = Str::random(40) .'.' .$file->getClientOriginalExtension();
        Storage::disk('public')->put("$path/{$randomFileName}",file_get_contents($file));
        return $randomFileName;
    }

    public static function deleteIfExists($path)
    {
        if(Storage::disk('public')->exists("/photo/$path")) {
            Storage::disk('public')->delete("/photo/$path");
        }
    }



}
