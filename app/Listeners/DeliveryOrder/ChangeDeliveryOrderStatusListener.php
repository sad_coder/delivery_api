<?php

namespace App\Listeners\DeliveryOrder;

use App\Events\DeliveryOrder\ChangeDeliveryOrderStatusEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ChangeDeliveryOrderStatusListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ChangeDeliveryOrderStatusEvent $event)
    {

    }
}
