<?php

namespace App\Listeners\BonusGame;

use App\Events\BonusGame\StoreBonusGameEvent;
use App\Jobs\BonusGame\StoreBonusGameJob;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Throwable;

class StoreBonusGameListener
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(StoreBonusGameEvent $event)
    {
        Bus::chain([
            function () {
                DB::beginTransaction();
            },
            new StoreBonusGameJob($event->mobileId, $event->sumWin, $event->gameId),
            function () {
                DB::commit();
            }
        ])->catch(function (Throwable $e) {
            DB::rollBack();
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        })->dispatch();
    }
}
