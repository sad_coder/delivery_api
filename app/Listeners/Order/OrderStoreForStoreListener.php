<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderStoreForStoreEvent;
use App\Jobs\ProductStoreCoordinateJob;
use App\Jobs\Store\StoreStoreJob;
use App\Jobs\Order\StoreOrderJob;
use App\Models\OrderRequest;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Throwable;

class OrderStoreForStoreListener
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(OrderStoreForStoreEvent $event)
    {
        Bus::chain([
            function () {
                DB::beginTransaction();
            },
            new StoreStoreJob($event->storeData, $event->orderData, $event),
            function () use ($event) {
                OrderRequest::processed($event->orderData->mobile_id);
                DB::commit();
            }
        ])->catch(function (Throwable $e) use ($event) {
            DB::rollBack();
            OrderRequest::failed($event->orderData->mobile_id, $e->getMessage());
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        })->dispatch();
    }
}
