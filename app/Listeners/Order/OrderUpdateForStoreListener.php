<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderUpdateForStoreEvent;
use App\Jobs\Order\UpdateOrderJob;
use App\Jobs\Store\UpdateStoreJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Matrix\Exception;
use Throwable;

class OrderUpdateForStoreListener
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(OrderUpdateForStoreEvent $event)
    {
        Bus::chain([
            function () {
                DB::beginTransaction();
            },
            new UpdateOrderJob($event->order_id, $event->basket, $event->deliveryData),
            new UpdateStoreJob($event->store_id, $event->shopAddress),
            function () {
                DB::commit();
            }
        ])->catch(function (Throwable $e) {
            DB::rollBack();
            logger($e->getMessage());
            throw new Exception($e->getMessage());
        })->dispatch();
    }
}
