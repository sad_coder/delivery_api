<?php

namespace Database\Seeders;

use App\Models\TelegramMailing;
use Illuminate\Database\Seeder;

class TelegramMailingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mailings = [
            [
                'name' => 'Сообщение об ошибка не созданных заказов',
                'description' => 'Будет приходить сообщение с айди заказа и текстом сообщения',
                'slug' => 'order_request_error',
                'is_programmatically' => false
            ],
            [
                'name' => 'Отчет о торговых точках',
                'description' => 'Будет приходить эксель файл с отчетом о торговых точках',
                'slug' => 'store_report',
                'is_programmatically' => false
            ],
            [
                'name' => 'Отчет об играх',
                'description' => 'Будет приходить эксель файл с отчетом об играх',
                'slug' => 'bonus_game_report',
                'is_programmatically' => false
            ],
            [
                'name' => 'Напоминание об остатках',
                'description' => 'Будет приходить напоминанание об обновлении остатков товаров',
                'slug' => 'update_remainders_notification',
                'is_programmatically' => false
            ],
            [
                'name' => 'Сообщение о заказе от водителя с номером и стоимостью заказа',
                'description' => 'Сообщение о заказе от водителя с номером и стоимостью заказа',
                'slug' => 'driver_phone_cost_message',
                'is_programmatically' => false
            ],
            [
                'name' => 'Сообщение официальные точки',
                'description' => 'Сообщение официальные точки',
                'slug' => 'store_report_official',
                'is_programmatically' => false
            ]
        ];

        foreach ($mailings as $mailing) {
            TelegramMailing::query()->firstOrCreate($mailing);
        }
    }
}
