<?php

namespace Database\Seeders;

use App\Models\StoreRepresentativeSetting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreRepresentativeSettingsSeeder extends Seeder
{
    const DEFAULT_SETTINGS = [
        [
            'slug' => 'game_miz_prize_500',
            'data' => [
                'min_prize' => 500,
                'enabled' => 1,
                'description' => 'Приз за игру 500'
            ]
        ],
        [
            'slug' => 'game_win_prize_1000',
            'data' => [
                'min_prize' => 1000,
                'enabled' => '1',
                'description' => 'Приз за игру 1000'
            ]
        ],
        [
            'slug' => 'game_start_prize_20000',
            'data' => [
                'min_prize' => 20000,
                'enabled' => '1',
                'description' => 'Запускать игру после покупки более минимальной суммы чека'
            ]
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::DEFAULT_SETTINGS as $setting) {
            StoreRepresentativeSetting::query()
                ->create([
                    'slug' => $setting['slug'],
                    'data' => ($setting['data'])
                ]);
        }
    }
}
