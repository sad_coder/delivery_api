<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Product;
use Illuminate\Database\Seeder;

class BrandFilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = Brand::query()->with('productsEnabled')->get();

        foreach ($brands as $brand) {
            if($brand->productsEnabled->count() === 0 ) {
                $brand->enabled = false;
                $brand->save();
            }
        }

    }
}
