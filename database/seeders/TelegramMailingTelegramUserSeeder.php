<?php

namespace Database\Seeders;

use App\Models\TelegramMailing;
use App\Models\TelegramUser;
use Illuminate\Database\Seeder;

class TelegramMailingTelegramUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $telegramChatIds = [
            '992951666',
            '424232165'
        ];

        $mailing = TelegramMailing::query()->where('slug', 'order_request_error')->first();

        foreach ($telegramChatIds as $id) {
            $telegramUser = TelegramUser::where('chat_id', $id)->first();

            if ($telegramUser) {
                $telegramUser->telegramMailings()->syncWithoutDetaching($mailing->id);
            }
        }
    }
}
