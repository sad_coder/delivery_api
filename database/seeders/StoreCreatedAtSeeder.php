<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;

class StoreCreatedAtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stores = Store::query()
            ->whereNull('created_at')
            ->whereHas('orders')
            ->each(function ($store) {
                $store->created_at = $store->orders()->latest()->first()->created_at;
                $store->save();
            });
    }
}
