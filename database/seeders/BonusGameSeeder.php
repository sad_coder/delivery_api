<?php

namespace Database\Seeders;

use App\Models\BonusGame;
use Illuminate\Database\Seeder;

class BonusGameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $j = 10;

        for ($i = 0; $i < 50000; $i++) {
            BonusGame::query()->create([
               'store_id' => 1,
               'sum_win' => 5000,
               'mobile_id' => '123311hgjgjutgjg123211ujjuy5r6r2sdf2qhgjghjsghjhjgwdqwd2e21e21e2e21',
               'game_id' => $j++
            ]);
        }

        BonusGame::query()->create([
            'store_id' => 1,
            'sum_win' => 5000,
            'mobile_id' => '123311hgjgjutgjg123211ujjuy5r6r2sdf2qhgjghjsghjhjgwdqwd2e21e21e2e21e',
            'game_id' => $j++
        ]);
    }
}
