<?php

namespace Database\Seeders;

use App\Models\TelegramBotAuthToken;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TelegramBotAuthTokensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $token = Str::random('40');

            TelegramBotAuthToken::query()->create([
               'token' => $token
            ]);
        }
    }
}
