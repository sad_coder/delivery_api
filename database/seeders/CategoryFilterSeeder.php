<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryFilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::query()->with('productsEnabled')->get();

        foreach ($categories as $category) {
            if($category->productsEnabled->count() === 0 ) {
                $category->enabled = false;
                $category->save();
            }
        }

    }
}
