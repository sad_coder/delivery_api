<?php

namespace Database\Seeders;

use App\Models\StoreRepresentativeSetting;
use App\Models\TelegramBotAuthToken;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
           StoreRepresentativeSettingsSeeder::class
        ]);
    }
}
