<?php

use App\Models\OrderRequest;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OrderRequest::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('mobile_id');
            $table->json('payload');
            $table->text('exception')->nullable();
            $table->timestamp('failed_at')->nullable();
            $table->boolean('is_processed')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on(User::getTableName())
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(OrderRequest::getTableName());
    }
}
