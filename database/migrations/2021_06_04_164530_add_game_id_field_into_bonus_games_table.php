<?php

use App\Models\BonusGame;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGameIdFieldIntoBonusGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(BonusGame::getTableName(), function (Blueprint $table) {
            $table->string('game_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(BonusGame::getTableName(), function (Blueprint $table) {
            $table->dropColumn('game_id');
        });
    }
}
