<?php

use App\Models\TelegramBotAuthToken;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramBotAuthTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TelegramBotAuthToken::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->string('token');
            $table->unsignedBigInteger('telegram_user_id')->nullable();
            $table->timestamps();

            $table->foreign('telegram_user_id')
                ->references('id')
                ->on('telegram_users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TelegramBotAuthToken::getTableName());
    }
}
