<?php

use App\Models\ProductWaybill;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductWaybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductWaybill::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->string('order_id');
            $table->string('path');
            $table->unsignedTinyInteger('status')->default(ProductWaybill::NOT_SEND_TO_BOOKKEEPER)->comment('статус заказа (0 - не отправленный, 1 - отправленный)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ProductWaybill::getTableName());
    }
}
