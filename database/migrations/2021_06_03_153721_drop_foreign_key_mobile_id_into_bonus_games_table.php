<?php

use App\Models\BonusGame;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeyMobileIdIntoBonusGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(BonusGame::getTableName(), function (Blueprint $table) {
            $table->dropForeign(['mobile_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(BonusGame::getTableName(), function (Blueprint $table) {
            $table->foreign('mobile_id')
                ->references('mobile_id')
                ->on(Order::getTableName())
                ->onDelete('cascade');
        });

    }
}
