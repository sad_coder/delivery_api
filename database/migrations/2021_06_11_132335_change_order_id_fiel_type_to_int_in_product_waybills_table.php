<?php

use App\Models\ProductWaybill;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeOrderIdFielTypeToIntInProductWaybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ProductWaybill::getTableName(), function (Blueprint $table) {
            DB::statement('ALTER TABLE product_waybills ALTER COLUMN order_id TYPE bigint USING (order_id::bigint)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE product_waybills ALTER COLUMN
                  order_id TYPE varchar USING (order_id::varchar)');
    }
}
