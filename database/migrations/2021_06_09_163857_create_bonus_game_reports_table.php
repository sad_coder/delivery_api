<?php

use App\Models\BonusGameReport;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusGameReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(BonusGameReport::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->string('path');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(BonusGameReport::getTableName());
    }
}
