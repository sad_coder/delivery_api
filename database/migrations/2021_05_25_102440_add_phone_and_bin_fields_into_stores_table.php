<?php

use App\Models\Store;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneAndBinFieldsIntoStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Store::getTableName(), function (Blueprint $table) {
            $table->string('phone')->unique()->nullable()->after('address');
            $table->string('bin')->unique()->nullable()->after('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Store::getTableName(), function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('bin');
        });
    }
}
