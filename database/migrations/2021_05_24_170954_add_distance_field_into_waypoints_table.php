<?php

use App\Models\Waypoint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistanceFieldIntoWaypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Waypoint::getTableName(), function (Blueprint $table) {
            $table->integer('distance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Waypoint::getTableName(), function (Blueprint $table) {
            $table->dropColumn('distance');
        });
    }
}
