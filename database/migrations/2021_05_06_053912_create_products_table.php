<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Product::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->string('id_1c');
            $table->unsignedBigInteger('article')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('measure_id')->nullable();
            $table->string('name_1c');
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->string('audio')->nullable();
            $table->double('price')->nullable();
            $table->text('description')->nullable();
            $table->integer('remainder')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists(Product::getTableName());
    }
}
