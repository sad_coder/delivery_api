<?php

use App\Models\Order;
use App\Models\OrderReport;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OrderReport::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->string('path');
            $table->tinyInteger('status');
            $table->timestamp('created_at');

            $table->foreign('order_id')
                ->references('id')
                ->on(Order::getTableName())
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(OrderReport::getTableName());
    }
}
