<?php

use App\Models\Counterparty;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add1cInCounterpartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Counterparty::getTableName(), function (Blueprint $table) {
            $table->string("1c_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Counterparty::getTableName(), function (Blueprint $table) {
            $table->dropColumn("1c_id");
        });
    }
}
