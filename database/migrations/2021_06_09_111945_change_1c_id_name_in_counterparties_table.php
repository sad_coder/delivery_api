<?php

use App\Models\Counterparty;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Change1cIdNameInCounterpartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Counterparty::getTableName(), function (Blueprint $table) {
            $table->dropColumn('1c_id',);
            $table->string('id_1c')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Counterparty::getTableName(), function (Blueprint $table) {
            $table->dropColumn('id_1c',);
            $table->string('1c_id')->nullable();
        });
    }
}
