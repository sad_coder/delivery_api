<?php

use App\Models\Order;
use App\Models\Roadmap;
use App\Models\Waypoint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Waypoint::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roadmap_id');
            $table->unsignedBigInteger('order_id');
            $table->json('data');
            $table->timestamps();

            $table->foreign('roadmap_id')
                ->references('id')
                ->on(Roadmap::getTableName())
                ->onDelete('cascade');

            $table->foreign('order_id')
                ->references('id')
                ->on(Order::getTableName())
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Waypoint::getTableName());
    }
}
