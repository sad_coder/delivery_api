<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdForPresaleInPorudctsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Product::getTableName(), function (Blueprint $table) {
            $table->unsignedBigInteger('presale_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Product::getTableName(), function (Blueprint $table) {
            $table->dropColumn('presale_id');
        });
    }
}
