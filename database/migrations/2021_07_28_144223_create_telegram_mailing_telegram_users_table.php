<?php

use App\Models\TelegramMailing;
use App\Models\TelegramMailingTelegramUser;
use App\Models\TelegramUser;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramMailingTelegramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TelegramMailingTelegramUser::getTableName(), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('telegram_mailing_id');
            $table->unsignedBigInteger('telegram_user_id');
            $table->timestamps();

            $table->foreign('telegram_mailing_id')
                ->references('id')
                ->on(TelegramMailing::getTableName())
                ->onDelete('cascade');

            $table->foreign('telegram_user_id')
                ->references('id')
                ->on(TelegramUser::getTableName())
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TelegramMailingTelegramUser::getTableName());
    }
}
