<?php

use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Order::getTableName(), function (Blueprint $table) {
            $table->unsignedTinyInteger('status')->nullable()->default(Order::STATUS_READY_FOR_DELIVERY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Order::getTableName(), function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
