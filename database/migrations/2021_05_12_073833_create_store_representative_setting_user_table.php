<?php

use App\Models\StoreRepresentativeSetting;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreRepresentativeSettingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_representative_setting_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('store_representative_setting_id');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on(User::getTableName())
                ->onDelete('cascade');

            $table->foreign('store_representative_setting_id')
                ->references('id')
                ->on(StoreRepresentativeSetting::getTableName())
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_representative_setting_user');
    }
}
