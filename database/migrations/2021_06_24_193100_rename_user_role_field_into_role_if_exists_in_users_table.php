<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameUserRoleFieldIntoRoleIfExistsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::getTableName(), function (Blueprint $table) {
            if (Schema::hasColumn('users','user_role')) {
                $table->renameColumn('user_role', 'role');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::getTableName(), function (Blueprint $table) {

        });
    }
}
