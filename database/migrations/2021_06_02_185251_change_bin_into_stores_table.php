<?php

use App\Models\Store;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBinIntoStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Store::getTableName(), function (Blueprint $table) {
            $table->string('bin')->nullable()->after('seller_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Store::getTableName(), function (Blueprint $table) {
            $table->string('bin')->nullable()->after('seller_id')->change();
        });
    }
}
