<?php

use App\Models\OrderReport;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeFieldIntoOrderReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(OrderReport::getTableName(), function (Blueprint $table) {
            $table->unsignedTinyInteger('type')->default(OrderReport::REPORT_TYPE_ORDER);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(OrderReport::getTableName(), function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
