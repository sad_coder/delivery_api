@extends('admin.layouts.index')

@section('title')
    Телеграм пользователи
@endsection

@section('content')
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true"></button>
            <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
        </div>
    @endif
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Телеграм пользователи</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table" style="z-index: 0">
                                <thead>
                                <tr>
                                    <th class="defaultSort">ID</th>
                                    <th>ФИО</th>
                                    <th>Айди чата</th>
                                    <th>Токен</th>
                                    <th>Регистрация</th>
                                    <th>Послднее изменение</th>
                                    <th>Последнее действие</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="pk_id">{{ $user->id }}</td>
                                        <td>{{$user->user ? $user->user->full_name : "Пока не прикреплен к нашему пользователю"}}</td>
                                        <td>{{$user->chat_id}}</td>
                                        <td>{{$user->token ? $user->token->token : 'Нету токена'}}</td>
                                        <td>{{$user->created_at->format('Y-m-d H:s')}}</td>
                                        <td>{{$user->updated_at->format('Y-m-d H:s')}}</td>
                                        <td>{{$user->state_type ?? 'Главное меню'}}</td>
                                        <td>
                                            <div class="">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-outline-primary block btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#{{'myModalLabel'.$user->id}}">
                                                        Добавить рассылку
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="{{'myModalLabel'.$user->id}}" style="z-index: 214748364" tabindex="1999"
                                                         role="dialog" aria-labelledby="{{'myModalLabel'.$user->id}}"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document" style="z-index: 214748364">
                                                            <div class="modal-content">
                                                                <div class="modal-header bg-primary white">
                                                                    <h4 class="modal-title white" id="myModalLabel8">
                                                                        Добавить рассылку</h4>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <form method="post"
                                                                      action="{{route('admin.telegram-users.attach-mailings',['user' => $user->id])}}">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <h5>Рассылки</h5>
                                                                        <div class="card-content">
                                                                            <div class="card-body">
                                                                                @foreach($mailings as $mail)
                                                                                    <fieldset>
                                                                                        <div
                                                                                            class="custom-control custom-checkbox">
                                                                                            <input type="checkbox"
                                                                                                   class="custom-control-input"
                                                                                                   name="mailings[]"
                                                                                                   @if($user->telegramMailings->where('id', $mail->id)->first())
                                                                                                   checked
                                                                                                   @endif
                                                                                                   value="{{$mail->id}}"
                                                                                                   id="{{"mailing$mail->id.$user->id"}}">
                                                                                            <label
                                                                                                class="custom-control-label"
                                                                                                for="{{"mailing$mail->id.$user->id"}}">{{$mail->name}}</label>
                                                                                        </div>
                                                                                    </fieldset>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button"
                                                                                class="btn grey btn-outline-secondary"
                                                                                data-dismiss="modal">Закрыть
                                                                        </button>
                                                                        <button type="submit"
                                                                                class="btn btn-outline-primary">
                                                                            Сохранить
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>

    <script>

        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
