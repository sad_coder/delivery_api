<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
    <meta name="keywords"
          content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="/robust-admin/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/robust-admin/app-assets/images/ico/favicon.ico">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
        rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/vendors.css">

    {{--<link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/vendors/css/tables/datatable/datatables.min.css">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/app.css">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/core/colors/palette-gradient.css">

    <!-- END Page Level CSS-->
    {{--deleted--}}
    {{--<link rel="stylesheet" href="/css/jquery.treetable.css" />--}}
    {{--<link rel="stylesheet" href="/css/jquery.treetable.theme.default.css" />--}}

    {{--to add styles in other pages--}}
    @stack('styles')
    <style>
        .storeRepName {
            cursor: pointer;
        }

        .storeRepName:hover {
            box-shadow: 0 0 11px rgba(33, 33, 33, .2);
        }
    </style>

    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/css/admin-style.css">
    <!-- END Custom CSS-->

</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
      data-open="click" data-menu="vertical-menu" data-col="2-columns">
<!-- fixed-top-->
@include('admin.layouts.header')

<!-- ////////////////////////////////////////////////////////////////////////////-->
@include('admin.layouts.sidebar')

<div class="app-content content">
    <div class="content-wrapper">
        @yield('content')
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

{{--@include('admin.layouts.footer')--}}

<!-- BEGIN VENDOR JS-->
<script src="/robust-admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<script src="/robust-admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
{{--<script src="/robust-admin/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<!-- BEGIN PAGE VENDOR JS-->

@stack('scripts')

<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->

<script src="/robust-admin/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="/robust-admin/app-assets/js/core/app.js" type="text/javascript"></script>
<script src="/robust-admin/app-assets/js/scripts/customizer.js" type="text/javascript"></script>


<!-- END ROBUST JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="/robust-admin/app-assets/js/scripts/extensions/block-ui.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="/robust-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
<script src="/robust-admin/app-assets/js/scripts/tables/datatables/datatable-api.js" type="text/javascript"></script>

{{--<script src="/js/jquery.treetable.js"></script>--}}
{{--<script src="/js/jquery.jquery-ui.js"></script>--}}

{{--<script src="/js/admin-my.js"></script>--}}

</body>
</html>
