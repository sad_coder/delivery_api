<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span data-i18n="nav.category.layouts">Меню</span><i class="ft-more-horizontal ft-minus"
                                                                     data-toggle="tooltip" data-placement="right"
                                                                     data-original-title="Layouts"></i>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.user.index')}}">
                    <i class="fa fa-users"></i><span class="menu-title">Пользователи</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{route('admin.store-rep.index')}}"
                           data-i18n="nav.dash.ecommerce">Торговый</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('admin.user.index', ['filter[role]=2'])}}"
                           data-i18n="nav.dash.ecommerce">Водитель</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('admin.user.index', ['filter[role]=3'])}}"
                           data-i18n="nav.dash.ecommerce">Админ</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('admin.telegram-users.index')}}"
                           data-i18n="nav.dash.ecommerce">Телеграм пользователи</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.brand.index')}}"><i class="fa fa-shopping-basket"></i><span class="menu-title"
                                                                                                    data-i18n="nav.changelog.main">Брэнды</span></a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.order.index')}}"><i class="fa fa-ticket"></i><span class="menu-title"
                                                                                           data-i18n="nav.changelog.main">Заказы</span></a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.order.index')}}">
                    <i class="fa fa-users"></i><span class="menu-title">Заказы</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{route('admin.order.index', ['filter[trashed]=only'])}}"
                           data-i18n="nav.dash.ecommerce">Удаленные</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('admin.order.index', ['filter[status]=1'])}}"
                           data-i18n="nav.dash.ecommerce">Готовы к доставке</a>
                    </li>
                    <li>
                        <a class="menu-item" href="#"
                           data-i18n="nav.dash.ecommerce">В доставке</a>
                    </li>
                    <li>
                        <a class="menu-item" href="#"
                           data-i18n="nav.dash.ecommerce">Доставлено</a>
                    </li>
                    <li>
                        <a class="menu-item" href="#"
                           data-i18n="nav.dash.ecommerce">Неизвестно</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                {{--                <a href="{{route('admin.category.index')}}"><i class="fa fa-tasks"></i><span class="menu-title" data-i18n="nav.changelog.main">Категории</span></a>--}}
            </li>
            <li class="nav-item">
                <a href="{{route('admin.category.allCategory')}}"><i class="fa fa-object-group"></i><span
                        class="menu-title" data-i18n="nav.changelog.main">Категории</span></a>
            </li>

            <li class="nav-item">
                <a href="{{route('admin.store-rep-settings.index')}}"><i class="fa fa-gear"></i><span class="menu-title"
                                                                                                      data-i18n="nav.changelog.main">Настройки</span></a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.product.index')}}"><i class="fa fa-shopping-basket"></i><span class="menu-title"
                                                                                                      data-i18n="nav.changelog.main">Продукты</span></a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.store.index')}}"><i class="fa fa-shopping-cart"></i><span class="menu-title"
                                                                                                  data-i18n="nav.changelog.main">Торг.точки</span></a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.game.allGameHistory')}}"><i class="ft-box"></i><span class="menu-title"
                                                                                             data-i18n="nav.changelog.main">Истории игр</span></a>
            </li>
            {{-- <li class="nav-item">
                 <a href="{{route('admin.order-report.index')}}"><i class="ft-box"></i><span class="menu-title" data-i18n="nav.changelog.main">Отчет</span></a>
             </li> --}}
            <li class="nav-item">
                {{--                <a href="{{route('admin.category.index')}}"><i class="fa fa-building"></i><span class="menu-title" data-i18n="nav.changelog.main">Категории</span></a>--}}
            </li>
            <li class="nav-item">
                <a href="{{route('admin.order.failed')}}">Не состоявшиеся заказы</a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.order.audits')}}">Аудит заказов</a>
            </li>
        </ul>
    </div>
</div>
