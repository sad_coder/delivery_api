@extends('admin.layouts.index')

@section('title')
    Статус доставок заказов
@endsection

@section('content')

    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Статус доставок заказов</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th>Водитель</th>
                                    <th>Дата формирования маршрута</th>
                                    <th>ID заказа</th>
                                    <th>Заказ</th>
                                    <th>Адресс доставки</th>
                                    <th>Время доставки:</th>
                                    <th>Ушло времени на доставку</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--                                {{dd($roadmaps)}}--}}
                                @foreach($roadmaps as $key => $roadmap)
                                    @foreach($roadmap->waypoints as $index=>$waypoint)
                                        <tr>
                                            @if($index == 0)
                                                <td rowspan="{{count($roadmap->waypoints)}}">{{$roadmap->user->full_name}}</td>
                                            @endif
                                            @if($index == 0)
                                                <td rowspan="{{count($roadmap->waypoints)}}">{{$roadmap->created_at}}</td>
                                            @endif
                                            <td>{{$waypoint->order->id}}</td>
                                            <td>
                                                <a href="{{route('admin.order.index',["filter[id]={$waypoint->order->id}"])}}">
                                                    просмотр
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{"https://yandex.kz/maps/162/almaty/?pt={$waypoint->order->address['longitude']}%2C{$waypoint->order->address['latitude']}&z=17"}}">
                                                    просмотр
                                                </a>
                                            </td>
                                            <td>{{$waypoint->order->delivered_at ?? '-'}}</td>
                                            <td>{{$roadmap->getDeliveryDuration($waypoint->order->id)}}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                            <div class="crud-create">
                                <a href="{{route('admin.user.create')}}" type="button"
                                   class="btn btn-success btn-min-width"><i style="margin-right: 5px"
                                                                            class="fa fa-check"></i>Создать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>

    <script>

        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
