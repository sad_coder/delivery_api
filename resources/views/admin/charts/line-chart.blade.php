@extends('admin.layouts.index')

@section('title')
    График
@endsection

@section('content')
    <section id="configuration">
        <form class="form form-horizontal d-flex" method="get"
              action="{{ route('admin.charts.store-rep.storeRepOrderBuys')}}">
            @csrf
            <div class="input-group input-daterange" data-provide="datepicker" id="fromToFilter">
                <div class="input-group-addon">С</div>
                <input type="text" class="form-control" name="dateFrom" autocomplete="off">
                <div class="input-group-addon">до</div>
                <input type="text" class="form-control" name="dateTo" autocomplete="off">
            </div>
            @foreach($userIds as $id)
                <input type="hidden" name="userIds[]" value="{{$id}}">
            @endforeach
            <button type="submit" class="btn btn-sm btn-grey ml-1">Поиск</button>
        </form>
        <div>
            <canvas id="myChart"></canvas>
        </div>
    </section>
@endsection

@push('scripts')
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script type="text/javascript">
        $('#fromToFilter input').datepicker({language: "ru", format: "yyyy-mm-dd"});
        let chartLabel = "{{$label ?? ''}}";
        let dataSet = JSON.parse('{!! json_encode($dataSet) !!}')
        let labels = JSON.parse('{!! json_encode($labels) !!}')
        console.log(labels)
        const data = {
            labels: labels,
            datasets: dataSet
        };
        const config = {
            type: 'line',
            data,
            options: {}
        };

        var myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>
@endpush
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@endpush
