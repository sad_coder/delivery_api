@extends('admin.layouts.index')

@section('title')
    Добавить телефон
@endsection


<style>
    .filails-block-radio {
        display: flex;
    }

    .one-radio-f-item {
        display: flex;
        margin-left: 5%;
    }

    .one-radio-f-item>input {
        margin-top: 0%;
        margin-left: 0%;
        width: 35px;
        height: 19px;
    }
</style>


@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Добавить телефон</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.phone.store') }}"
                              method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form form-horizontal">
                                @include('admin.form_blocks.input',['type'=>'text','label'=>'Номер телефона','name'=>'number'])
                                <div class="filails-block-radio">
                                    @foreach($filials as $filial)
                                        <div class="one-radio-f-item">
                                            <div class="one-radio-f-name">{{$filial->number .' Филиал '. $filial->address}}</div>
                                            <input type="radio" name="filial_id" value="{{$filial->id}}">
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                            @include('admin.form_blocks.submit',['title'=>'Добавить'])
                            @include('admin.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
