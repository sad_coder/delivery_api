@extends('admin.layouts.index')

@section('title')
    Пользователи
@endsection

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true"></button>
            <h4><i class="icon fa fa-warning"></i> {{implode('', $errors->all(':message'))}}</h4>
        </div>
    @endif
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Пользователи</h4>
                        <div class="mt-2 mb-n2 text-lg-right">
                            <a href="{{route('admin.store-rep.sku')}}"><i class="fa fa-bar-chart"></i>SKU</a>
                        </div>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <form action="{{route('admin.charts.store-rep.storeRepOrderBuys')}}" method="get">
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard table-responsive">
                                <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                    <thead>
                                    <tr>
                                        <th class="defaultSort">ID</th>
                                        <th class="defaultSort">ID 1c</th>
                                        <th>ФИО</th>
                                        <th>Email</th>
                                        <th>Регистрация</th>
                                        <th>Послднее изменение</th>
                                        <th>Статистика</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td class="pk_id">{{ $user->id }}</td>
                                            <td class="pk_id">{{ $user['1c_id']}}</td>
                                            <td>{{$user->full_name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->created_at->format('Y-m-d H:s')}}</td>
                                            <td>{{$user->updated_at->format('Y-m-d H:s')}}</td>
                                            <td>
                                                {{--                                            <a href="{{route('admin.charts.store-rep.storeRepOrderBuys', ['userIds[]' => $user->id])}}">--}}
                                                {{--                                                <i class="fa fa-line-chart"></i>--}}
                                                {{--                                            </a>--}}
                                                <input type="checkbox" value="{{$user->id}}" name="userIds[]">
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="button button-primary" type="submit">
                                <i class="fa fa-line-chart"></i>
                                Показать статистику
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>

    <script type="text/javascript">
        let userIds = [];

        function addUserToStats(userId) {
            userIds.push(userId)
            console.log(userId);
        }
    </script>

@endpush


@push('styles')

    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
