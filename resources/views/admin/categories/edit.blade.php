@extends('admin.layouts.index')

@section('title')
    Редактировать категорию
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Редактировать категорию: {{ $category->title }}</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.category.update',$category->id)}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form form-horizontal">
                                <ul class="nav nav-tabs">
{{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabGeneralInfo','title'=>'Общие',])--}}
{{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabSeo','title'=>'SEO'])--}}
{{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabImages','title'=>'Картинки'])--}}
                                    @include('admin.form_blocks.tab.nav',['name'=>'tabCategories','title'=>'Категория'])
{{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabCities','title'=>'Города'])--}}
                                </ul>
                                <div class="tab-content px-1 pt-1  border-grey border-lighten-2 border-0-top">

{{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabGeneralInfo','active'=>'1'])--}}
{{--                                        <div--}}
{{--                                            class="tab-content  px-1 pt-1 ">--}}
{{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Заголовок','name'=>'title', 'value'=> $category->title])--}}
{{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Название','name'=>'name', 'generateSlug' => true, 'value'=> $category->name])--}}
{{--                                            @include('admin.form_blocks.textarea',['label'=>'Описание','name'=>'category', 'summernote'=>1, 'text'=>$category->text])--}}
{{--                                            @include('admin.form_blocks.textarea',['label'=>'Краткое описание','name'=>'category', 'placeholder'=> 'Краткое описание', 'text'=>$category->text])--}}
{{--                                        </div>--}}
{{--                                    @endcomponent--}}
{{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabSeo',])--}}
{{--                                        <div class="tab-content  px-1 pt-1 ">--}}
{{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Slug - ссылка','name'=>'slug', 'value'=> $category->slug, 'id' => 'slug'])--}}
{{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Seo - заголовок','name'=>'seo_title', 'value' => $category->seo_title])--}}
{{--                                            @include('admin.form_blocks.textarea',['label'=>'Seo Описание','name'=>'seo_description', 'text'=>$category->seo_description])--}}
{{--                                            @include('admin.form_blocks.input',['type'=>'checkbox','label'=>'Популярная категория в поиске?','name'=>'popular_in_search','value'=>$category->popular_in_search,'id'=>'main-page-display'])--}}
{{--                                        </div>--}}
{{--                                    @endcomponent--}}
{{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabImages'])--}}
{{--                                        @include('admin.form_blocks.ajax_image',['label'=>'Главная картинка','name'=>'image','type'=>'file'])--}}
{{--                                        @include('admin.form_blocks.file',['label'=>'Картинки','name'=>'images[]','load'=>'','multiple'=>''])--}}
{{--                                    @endcomponent--}}

                                    @if (session('success'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                                        </div>
                                    @endif

                                    @component('admin.form_blocks.tab.content',['name'=>'tabCategories'])
                                        <div class="tab-content  px-1 pt-1 ">
                                            <div>
                                                <input type="text" value="{{ $category->name }}" name="name" class="form-control">
                                                <input type="text" value="{{ $category->description }}" name="description" class="form-control">
                                                <input type="text" value="{{ $category->title }}" name="title" class="form-control">
                                            </div>
                                        </div>
                                    @endcomponent

{{--                                    @foreach($categories as $category)--}}
{{--                                        <tr>--}}
{{--                                            <td class="pk_id">{{ $category->id }}</td>--}}
{{--                                            <td>@include('admin.form_blocks.imageView',['image'=>$category->mainImagePath])</td>--}}
{{--                                            <td>{{ $category->parent ? '<a href=#>'. $category->parent->title .'</a>' : null }}</td>--}}
{{--                                            <td>{{ $category->slug }}</td>--}}
{{--                                            <td>{{$category->title}}</td>--}}
{{--                                            <td>{{ $category->created_at }}</td>--}}
{{--                                            <td>--}}
{{--                                                @include('admin.form_blocks.actions',['route_edit'=>route('admin.category.edit', $category['id']),'route_destroy'=>route('admin.category.destroy', $category['id'])])--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
                                </div>
                            </div>
                            @include('admin.form_blocks.submit',['title'=>'Изменить'])
                            @include('admin.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            function generateSlug(value) {
                console.log(value);
                $.ajax({
                    type: 'POST',
                    url: '/admin/product/generate_slug',
                    data: {
                        slug: value,
                    },
                    success: function (response) {
                        $('#slug').val(response);
                    },
                });
            }
        </script>
    @endpush

@endsection

{{--<script type="text/javascript">--}}
{{--    $.ajaxSetup({--}}
{{--        headers: {--}}
{{--            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--        }--}}
{{--    });--}}
{{--</script>--}}

{{--<script>--}}

{{--    --}}

{{--    $('#myForm a').click(function (e) {--}}
{{--        e.preventDefault();--}}
{{--        $(this).tab('show');--}}
{{--    });--}}

{{--    function addPhoto() {--}}

{{--        var formData = new FormData();--}}
{{--        formData.append('file', $('#sendImage')[0].files[0]);--}}
{{--        $.ajax({--}}
{{--            type: "POST",--}}
{{--            url: '{{route('admin.ajax.add_image')}}',--}}
{{--            data: formData,--}}
{{--            processData: false,  // Important!--}}
{{--            contentType: false,--}}
{{--            contentType: false,--}}
{{--            cache: false,--}}

{{--            success: function (response) {--}}
{{--                console.log(response);--}}
{{--                let imageContainer = document.getElementById('image-container');--}}
{{--                console.log(response['url']);--}}
{{--                let image = document.createElement("img");--}}
{{--                let imgId = document.getElementById("imageId");--}}
{{--                imgId.value = response['id'];--}}
{{--                console.log(response['id']);--}}
{{--                image.setAttribute('width', 500);--}}
{{--                image.setAttribute('src', response['url']);--}}
{{--                image.setAttribute('height', 'auto');--}}
{{--                imageContainer.appendChild(image);--}}

{{--            },--}}
{{--        });--}}

{{--    }--}}

{{--    document.getElementById("addPhoto").addEventListener("click", addPhoto);--}}

{{--</script>--}}
{{--</body>--}}
{{--</html>--}}


