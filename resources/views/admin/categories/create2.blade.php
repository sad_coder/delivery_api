@extends('admin.layouts.index')

@section('title')
    Добавить категорию
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Добавить категорию</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.category.store') }}"
                              method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form form-horizontal">
                                <ul class="nav nav-tabs">
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabGeneralInfo','title'=>'Общие',])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabSeo','title'=>'SEO'])--}}
                                    @include('admin.form_blocks.tab.nav',['name'=>'tabCategories','title'=>'Основная информация', 'active' => true])
                                    @include('admin.form_blocks.tab.nav',['name'=>'tabImages','title'=>'Картинки'])
                                </ul>
                                <div class="tab-content px-1 pt-1  border-grey border-lighten-2 border-0-top">

                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabGeneralInfo','active'=>'1'])--}}
                                    {{--                                        <div--}}
                                    {{--                                            class="tab-content  px-1 pt-1 ">--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Заголовок','name'=>'title'])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Название','name'=>'name','generateSlug' => true])--}}
                                    {{--                                            @include('admin.form_blocks.textarea',['label'=>'Описание','name'=>'category', 'summernote'=>1])--}}
                                    {{--                                            @include('admin.form_blocks.textarea',['label'=>'Краткое описание','name'=>'category', 'placeholder'=> 'Краткое описание'])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'checkbox','label'=>'Популярная категория в поиске?','name'=>'popular_in_search','value'=>0,'id'=>'main-page-display'])--}}

                                    {{--                                        </div>--}}
                                    {{--                                    @endcomponent--}}
                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabSeo',])--}}
                                    {{--                                        <div class="tab-content  px-1 pt-1 ">--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Slug - ссылка','name'=>'slug', 'id' =>'slug'])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Seo - заголовок','name'=>'seo_title',])--}}
                                    {{--                                            @include('admin.form_blocks.textarea',['label'=>'Seo Описание','name'=>'seo_description'])--}}
                                    {{--                                        </div>--}}
                                    {{--                                    @endcomponent--}}

                                    @component('admin.form_blocks.tab.content',['name'=>'tabImages'])
                                        @include('admin.form_blocks.file',['label'=>'Картинки','name'=>'images[]','load'=>'','multiple'=>true])
                                    @endcomponent

                                    @if (session('success'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                                        </div>
                                    @endif

                                    @component('admin.form_blocks.tab.content',['name'=>'tabCategories', 'active' => true])
                                        <div class="tab-content  px-1 pt-1 ">
                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Имя','name'=>'name'])
                                        </div>
                                    @endcomponent

                                </div>
                            </div>
                            @include('admin.form_blocks.submit',['title'=>'Добавить'])
                            @include('admin.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script>
            function generateSlug(value) {
                console.log(value);
                $.ajax({
                    type: 'POST',
                    url: '/admin/product/generate_slug',
                    data: {
                        slug: value,
                    },

                    success: function (response) {
                        $('#slug').val(response);
                    },
                });

            }


        </script>
    @endpush

@endsection
