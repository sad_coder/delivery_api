<style>
    span.select2.select2-container.select2-container {
        width: 89% !important;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


@extends('admin.layouts.index')

@section('title')
    Добавить пользователя
@endsection

@section('content')

    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Добавить пользователя</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.user.store') }}"
                              method="POST"
                              enctype="multipart/form-data">
                            <div class="form form-horizontal">
                                <ul class="nav nav-tabs">
                                    @include('admin.form_blocks.tab.nav', ['name' => 'main', 'title'=>'Основная информация', 'active' => true])
                                    @include('admin.form_blocks.tab.nav', ['name' => 'settings', 'title'=>'Настройки'])
                                </ul>
                                <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                    @if (session('success'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                                        </div>
                                    @endif
                                    @component('admin.form_blocks.tab.content', ['name' => 'main', 'active' => true])
                                        @include('admin.form_blocks.input',['type'=>'email','label'=>'Email','name'=>'email'])
                                        @include('admin.form_blocks.input',['type'=>'text','label'=>'ФИО','name'=>'full_name',])
                                        @include('admin.form_blocks.input',['type'=>'password','label'=>'Пароль','name'=>'password',])
                                        @include('admin.form_blocks.select2',['label'=>'Роль','name'=>'role','items' => $roles, 'field' => 'description', 'existOption' => false])
                                        <div class="form-group row justify-content-center">
                                            <label>
                                                <input type="checkbox" name="is_counterparty">
                                                Контрагент
                                            </label>
                                        </div>
                                        @include('admin.form_blocks.input',['type'=>'text','placeholder' => 'Имя контрагента','label'=>'','name'=>'name_1c',])
                                    @endcomponent
                                    @component('admin.form_blocks.tab.content', ['name' => 'settings'])
                                        @include('admin.form_blocks.checkbox', ['items'=>$settings, 'name'=>'settingIds[]', 'jsonNestedField' => 1, 'jsonName' => 'data', 'field' => 'description'])
                                    @endcomponent
                                </div>
                            </div>
                            @csrf
                            @include('admin.form_blocks.submit',['title'=>'Добавить'])
                            @include('admin.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
            $('input[name="name_1c"]').hide();

            //show it when the checkbox is clicked
            $('input[name="is_counterparty"]').on('click', function () {
                if ($(this).prop('checked')) {
                    $('input[name="name_1c"]').fadeIn();
                } else {
                    $('input[name="name_1c"]').hide();
                }
            });
        });
    </script>
@endpush
