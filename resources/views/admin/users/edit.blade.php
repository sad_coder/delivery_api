<style>
    span.select2.select2-container.select2-container {
        width: 89% !important;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@extends('admin.layouts.index')

@section('title')
    Редактировать пользователя
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Редактировать пользователя</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                    </div>
                @endif
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.user.update',$user->id)}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @include('admin.form_blocks.input',['type'=>'email','label'=>'Email','name'=>'email', 'value' => $user->email])
                            @include('admin.form_blocks.input',['type'=>'text','label'=>'ФИО','name'=>'full_name','value' => $user->full_name])
                            @include('admin.form_blocks.input',['type'=>'password','label'=>'Пароль','name'=>'password', 'placeholder' => 'Оставьте поле пустым если без изменений'])
                            @include('admin.form_blocks.input',['type'=>'number','label'=>'Роль','name'=>'role', 'value' => $user->role])
                            @include('admin.form_blocks.submit',['title'=>'Изменить'])
                            @include('admin.form_blocks.error')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
