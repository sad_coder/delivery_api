@extends('admin.layouts.index')

@section('title')
    Истории игр
@endsection

@section('content')
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Истории игр</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6">
                        <form class="form form-horizontal d-flex" method="post"
                              action="{{ route('admin.game.search')}}">
                            @csrf
                            <div class="input-group input-daterange" data-provide="datepicker" id="fromToFilter">
                                <div class="input-group-addon">С</div>
                                <input type="text" class="form-control" name="dateFrom" autocomplete="off">
                                <div class="input-group-addon">до</div>
                                <input type="text" class="form-control" name="dateTo" autocomplete="off">
                                <input type="hidden" name="user_id" class="user_id">
                            </div>
                            <button type="submit" class="btn btn-sm btn-grey ml-1">Поиск</button>
                            <button type="button" onclick="refresh()" class="btn btn-sm btn-grey ml-1">Сбросить</button>
                        </form>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th class="defaultSort">ID</th>
                                    <th>Торговая точка</th>
                                    <th>ID 1C торговой точки</th>
                                    <th>Представитель</th>
                                    <th>ID 1c представителя</th>
                                    <th>Сумма выигрыша</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td class="storeRepName">
                                            <a href="{{route('admin.order.basket', ['order' => $order->id])}}">
                                                {{$order->id}}
                                            </a>
                                        </td>
                                        <td>{{$order->store ? ($order->store->address . ' ' . $order->store->name) : ''}}</td>
                                        <td>{{$order->store ? $order->store->id_once : 'не задано'}}</td>
                                        <td>{{$order->store->seller->full_name}}</td>
                                        <td>{{isset($order->store->seller->counterparty) ? $order->store->seller->counterparty->id_1c : 'не задано' }}</td>
                                        <td>{{ $order->bonus_games_sum_sum_win}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript">
        function refresh() {
            window.location = "/admin/game/all";
        }
    </script>

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>

    <script>
        $('#fromToFilter input').datepicker({language: "ru", format: "yyyy-mm-dd"});

        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
