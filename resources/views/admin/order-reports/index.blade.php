@extends('admin.layouts.index')

@section('title')
    Отчеты
@endsection

@section('content')
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Отчет заказов</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row d-flex ml-2">
                        <div>
                            <form class="form form-horizontal d-flex" method="post"
                                  action="{{ route('admin.order.search')}}">
                                @csrf
                                <div class="input-group input-daterange" data-provide="datepicker" id="fromToFilter">
                                    <div class="input-group-addon">С</div>
                                    <input type="text" class="form-control" name="dateFrom">
                                    <div class="input-group-addon">до</div>
                                    <input type="text" class="form-control" name="dateTo">
                                </div>
                                <button type="submit" class="btn btn-sm btn-grey ml-1">Поиск</button>
                            </form>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th class="defaultSort">Отчеты за</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dates as $date)
                                    <tr>
                                        <td class="pk_id">
                                            <div class="d-flex justify-content-between">
                                                <a href="{{route('admin.order-report.show', ['date'=> $date])}}">
                                                    <i class="fa fa-folder"></i>
                                                    {{\Carbon\Carbon::parse($date)->format('Y-m-d')}}
                                                </a>
                                                <a href="{{route('admin.order-report.download-full',['date' => $date])}}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script>
        $('#fromToFilter input').datepicker({language: "ru", format: "yyyy-mm-dd"});
    </script>

@endpush


@push('styles')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
