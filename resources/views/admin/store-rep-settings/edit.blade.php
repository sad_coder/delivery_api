<style>
    span.select2.select2-container.select2-container {
        width: 89% !important;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@extends('admin.layouts.index')

@section('title')
    Редактировать настройку
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Редактировать настройку</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.store-rep-settings.update',$setting->id)}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                        <label>Слаг</label>
                                        <input type="text" value="{{ $setting->slug }}" name="slug" class="form-control">
                            </div>
                            <div class="form-group">
                                        <label>Описание</label>
                                        <input type="text" value="{{ $setting->data['description'] }}" name="description" class="form-control">
                            </div>
                            <div class="form-group">
                                        <label>Минимальный приз</label>
                                        <input type="text" value="{{ $setting->data['min_prize'] }}" name="min_prize" class="form-control">
                            </div>
                            <div class="form-check">
                                <input type="checkbox" {{$setting->data['enabled'] ? 'checked' : ''}} name="enabled" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Доступен</label>
                            </div>

                            @include('admin.form_blocks.submit',['title'=>'Изменить'])
                            @include('admin.form_blocks.error')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            function generateSlug(value) {
                //console.log(value);
                $.ajax({
                    type: 'POST',
                    url: '/admin/product/generate_slug',
                    data: {
                        slug: value,
                    },

                    success: function (response) {
                        $('#slug').val(response);
                    },
                });


            }


        </script>
    @endpush






@endsection
