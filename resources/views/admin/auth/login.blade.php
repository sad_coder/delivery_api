<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
    <meta name="keywords"
          content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Войти в админку</title>
    <link rel="apple-touch-icon" href="/robust-admin/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/robust-admin/app-assets/images/ico/favicon.ico">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
        rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/vendors.css">

    {{--<link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/vendors/css/tables/datatable/datatables.min.css">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/app.css">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/css/core/colors/palette-gradient.css">
    <style>
        .login-form {
            width: 500px;
            font-size: 15px;
        }
    </style>
    <!-- END Page Level CSS-->
{{--deleted--}}
{{--<link rel="stylesheet" href="/css/jquery.treetable.css" />--}}
{{--<link rel="stylesheet" href="/css/jquery.treetable.theme.default.css" />--}}

{{--to add styles in other pages--}}
@stack('styles')

<!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/css/admin-style.css">
    <!-- END Custom CSS-->

</head>
<body class="d-flex justify-content-center align-items-center">
<div class="login-form">
    <form method="post" action="{{route('admin.auth.authenticate')}}">
        @csrf
        <h2 class="text-center">Администратирование</h2>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Email" name="email" required>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Пароль" name="password" required>
        </div>

        @if($errors->any())
            <div class="form-group text-danger text-center">
                {{$errors->first('message')}}
            </div>
        @endif
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Войти</button>
        </div>
    </form>
</div>
</body>
</html>
