<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <style>
        .nacladnoi{
            text-align: -webkit-center;
        }
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: center;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ECE9E0;
            color: #656665;
            border-radius: 2px;
        }
        th {
            font-size: 16px;
            padding: 10px;
        }
        td {
            padding: 2px;
        }
        input[type=text] {
            padding: 4px 0px;
            border: 1px solid #82ab7b;
            box-sizing: border-box;
        }
        .number{
            width: 33px;
        }
        .name{
            width: 300px;
        }
        .measure{
            width: 70px;
        }
        .quantity{
            width: 107px;
        }
        .price{
            width: 92px;
        }
        .sum{
            width: 80px;
        }
        .n{
            background-color: #0c199c;
        }
    </style>
</head>
<body class="antialiased">
<div class="container">
    <div class="nacladnoi">
        <table>
            <h1>Накладная {{$order->store->address}} Магазин {{$order->store->name}}</h1>
            <div class="n">
                <tr>
                    <th>№</th>
                    <th>Наименование</th>
                    <th>Ед.изм.</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                </tr>
                @foreach($order->mergeBasketWithProducts() as $k => $product)
                    <tr>
                        <td><input class="number" type="text" value="{{$k}}" ></td>
                        <td><input class="name" type="text" value="{{$product->name_1c}}"></td>
                        <td><input class="measure" type="text" value="шт"></td>
                        <td><input class="quantity" type="text" value="{{$product->count}}"></td>
                        <td><input class="price" type="text" value="{{$product->price}}"></td>
                        <td><input class="sum" type="text" value="{{$product->price * $product->count}}"></td>
                    </tr>
                @endforeach
            Общая сумма {{$order->total_cost}}
            </div>
        </table>
    </div>
</div>
</body>
</html>