@extends('admin.layouts.index')

@section('title')
    Заказы
@endsection

@section('content')
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Заказы</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row d-flex">
                        <div class="crud-create ml-3">
                            <a href="" type="button" class="btn btn-success btn-min-width"><i style="margin-right: 5px"
                                                                                              class="fa fa-check"></i>Создать</a>
                        </div>
                        <div class="border ml-1 mr-1"></div>
                        <div>
                            <form class="form form-horizontal d-flex" method="post"
                                  action="{{ route('admin.order.search')}}">
                                @csrf
                                <div class="input-group input-daterange" data-provide="datepicker" id="fromToFilter">
                                    <div class="input-group-addon">С</div>
                                    <input type="text" class="form-control" name="dateFrom" autocomplete="off">
                                    <div class="input-group-addon">до</div>
                                    <input type="text" class="form-control" name="dateTo" autocomplete="off">
                                    <input type="hidden" name="user_id" class="user_id">
                                </div>
                                <button type="submit" class="btn btn-sm btn-grey ml-1">Поиск</button>
                                <button type="button" onclick="refresh()" class="btn btn-sm btn-grey ml-1">Сбросить
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th>Действие</th>
                                    <th class="defaultSort">ID</th>
                                    <th>Пользователь</th>
                                    <th>ID 1c торгового</th>
                                    <th>Время доставки</th>
                                    <th>Время заказа</th>
                                    <th>Корзина</th>
                                    <th>Адресс доставки</th>
                                    <th>Сумма заказа</th>
                                    <th>Сумма возврата</th>
                                    <th>Сумма выигрыша</th>
                                    <th>Число продуктов</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr style="{{$order->user->counterparty  ? '' : 'background:pink'}}">
                                        <td>
                                            @include('admin.form_blocks.actions',[
                                                        'route_edit'=>route('admin.order.edit',[
                                                            'order' => $order->id]),
                                                        'route_destroy' => route('admin.order.destroy', [
                                                            'order' => $order->id])
                                                            ])
                                            <a href="{{route('admin.order.download-report', ['order' => $order->id])}}">
                                                <button class="btn btn-sm btn-primary" style="margin-top: 5px;"><i
                                                        class="fa fa-download"></i> Отчет
                                                </button>
                                            </a>
                                        </td>
                                        <td class="pk_id">{{ $order->id }}</td>
                                        <td class="storeRepName"
                                            onclick="addFilter(this,{{$order->user->id}})">{{$order->user->full_name}}</td>
                                        <td>{{$order->user->counterparty ? $order->user->counterparty->id_1c : 'не задано'}}</td>
                                        <td>{{$order->created_at->addDay()->format("Y.m.d")}}</td>
                                        <td>{{$order->created_at->format("Y-m-d H:i:s")}}</td>
                                        <td>
                                            <button class="" style="border:none; background-color: #e3ebf3"><a
                                                    class="fa fa-shopping-cart"
                                                    href="{{route('admin.order.basket', ['order' => $order->id])}}"></a>
                                            </button>
                                        </td>
                                        <td>
                                            {{$order->store->name}}, {{$order->store->address}}
                                            {{--                                            <a href="{{"https://yandex.kz/maps/162/almaty/?pt={$order->address['longitude']}%2C{$order->address['latitude']}&z=17"}}">--}}
                                            {{--                                            <a href="#">--}}
                                            {{--                                                просмотр--}}
                                            {{--                                            </a>--}}
                                        </td>
                                        <td>{{$order->total_cost}}</td>
                                        <td>{{$order->total_returns_cost}}</td>
                                        <td class="storeRepName" onclick="goToBonusGamesPage(this, '{{$order->mobile_id}}')">
                                            {{$order->bonus_games_sum_sum_win}} тенге.
                                            <br>
                                            Сыграно игр {{$order->bonus_games_count}}
                                        </td>
                                        <td>{{$order->product_basket_count}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-center align-items-center">
                            <h4><span class="text-lg-center mr-5">
                               {{$totalCost}} - общая суммая заказа
                            </span></h4>
                            <h4><span class="text-lg-center">
                                {{$totalReturnsCost}} - общая суммая возврата
                            </span></h4>
                            <h4><span class="text-lg-center ml-5">
                                {{$totalBonusGamesCost}} - общая суммая игр
                            </span></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script type="text/javascript">
        function addFilter(element, userId) {
            console.log(userId)
            $('input[name="user_id"]').val(userId)
        }

        function refresh() {
            window.location = "/admin/order/all";
        }


        function goToBonusGamesPage(element, mobileId) {
            console.log(mobileId)
            window.location = "/admin/game/all?filter[mobile_id]=" + mobileId
        }
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script>
        $('#fromToFilter input').datepicker({language: "ru", format: "yyyy-mm-dd"});

        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
