<style>
    span.select2.select2-container.select2-container {
        width: 89% !important;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


@extends('admin.layouts.index')

@section('title')
    Редактировать заказ № {{$order->id}}
@endsection

@section('content')

    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Редактировать заказ</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal"
                              id="myForm"
                              action="{{ route('admin.order.update', ['order' => $order->id])}}"
                              method="post"
                              enctype="multipart/form-data">
                            <div class="form form-horizontal">
                                <ul class="nav nav-tabs">
                                    @include('admin.form_blocks.tab.nav', ['name' => 'main', 'title'=>'Основная информация', 'active' => true])
                                    @include('admin.form_blocks.tab.nav', ['name' => 'basket', 'title'=>'Корзина'])
                                    @include('admin.form_blocks.tab.nav', ['name' => 'address', 'title'=>'Адрес доставки'])
                                </ul>
                                <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                    @if (session('success'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                                        </div>
                                    @endif
                                    @component('admin.form_blocks.tab.content', ['name' => 'main', 'active' => true])
                                        @include('admin.form_blocks.select2',['label'=>'Заказчик','name'=>'user_id', 'items' => $users,'value' => 'id' ,'field' => 'full_name', 'selectedId' => $order->user_id])
                                        @include('admin.form_blocks.input',['type'=>'time','label'=>'Время доставки','name'=>'delivery_time', 'value' => $order->delivery_time])
                                        @include('admin.form_blocks.select2',['label'=>'Статус','name'=>'status', 'items' =>$statuses, 'value' => 'id', 'field' => 'description', 'selectedId' => $order->status])
                                        @include('admin.form_blocks.select2',['label'=>'Торговая точка','name'=>'store_id', 'items' =>$stores, 'value' => 'id', 'field' => 'address', 'selectedId' => $order->store_id])
                                    @endcomponent
                                    @component('admin.form_blocks.tab.content', ['name' => 'basket'])
                                        <div class="form-group col-12 mb-2 file-repeater">
                                            <div data-repeater-list="basket">
                                                @foreach($order->basket as $key=>$item)
                                                    <div data-repeater-item>

                                                        <div class="row mb-1">
                                                            <div class="col-9 col-xl-10">
                                                                @include('admin.form_blocks.select2', ['id' => 'pi:'.$item['product_id'],'name' => "basket[$key][product_id]", 'label' => '', 'field' => "name_1c", 'items' => $products, 'selectedId' => $item['product_id'], 'key' => $key, 'onchange' => "test({$item['product_id']})"])
                                                                @include('admin.form_blocks.input', ['id' => 'count:'.$item['product_id'],'type' => 'number','name' => "basket[$key][count]", 'label' => '', 'value' => $item['count']])
                                                            </div>
                                                            <div class="col-2 col-xl-1">
                                                                <button type="button" data-repeater-delete
                                                                        class="btn btn-icon btn-danger mr-1"><i
                                                                        class="ft-x"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="row d-flex justify-content-between">
                                                <button type="button" data-repeater-create class="btn btn-primary">
                                                    <i class="ft-plus"></i> Добавить продукт
                                                </button>
                                                <div class="justify-content-between">
                                                    <span>Сумма (тенге)</span>
                                                    <strong id="basketTotalCost">
                                                        {{$basketTotalCost}}
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    @endcomponent
                                    @component('admin.form_blocks.tab.content', ['name' => 'address'])
                                        @include('admin.form_blocks.input',['type'=>'text','label'=>'Широта','name'=>'address[latitude]', 'value' => $order->address['latitude']])
                                        @include('admin.form_blocks.input',['type'=>'text','label'=>'Высота','name'=>'address[longitude]', 'value' => $order->address['longitude']])
                                    @endcomponent
                                </div>
                            </div>
                            @csrf
                            @include('admin.form_blocks.submit',['title'=>'Внести правки'])
                            @include('admin.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"
        type="text/javascript"></script>
    <script src="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/js/scripts/forms/form-repeater.min.js"
            type="text/javascript"></script>
@endpush
