@extends('admin.layouts.index')

@section('title')
    Не удавшиеся заказы
@endsection

@section('content')
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Не удавшиеся заказы</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <form class="form form-horizontal d-flex mr-2 ml-2" method="get"
                          action="{{ route('admin.order.failed')}}">
                        @csrf
                        <div class="input-group input-daterange" data-provide="datepicker" id="fromToFilter">
                            <div class="input-group-addon">С</div>
                            <input type="text" class="form-control" name="filter[dateFrom]" autocomplete="off">
                            <div class="input-group-addon">до</div>
                            <input type="text" class="form-control" name="filter[dateTo]" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-sm btn-grey ml-1">Поиск</button>
                        </button>
                    </form>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th class="defaultSort">ID</th>
                                    <th class="defaultSort">Статус</th>
                                    <th>Хеш с мобильного приложения</th>
                                    <th>Пользователь</th>
                                    <th>Точка</th>
                                    <th>Адрес</th>
                                    <th>Телефон</th>
                                    <th>Корзина</th>
                                    <th>Игры</th>
                                    <th>Ошибка</th>
                                    <th>Дата не сосотояния</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td class="pk_id">{{ $order->id }}</td>
                                        <td>{{$order->is_processed ? 'Заказ успешно сформирован' : 'Ошибка'}}</td>
                                        <td>{{substr($order->mobile_id,0,15)}}</td>
                                        <td>{{$order->user->full_name}}</td>
                                        <td>{{$order->formatted_payload['shop_name']}}</td>
                                        <td>{{$order->formatted_payload['shop_addr']}}</td>
                                        <td>{{$order->formatted_payload['shop_phone']}}</td>
                                        <td>
                                            @if($order->is_processed)
                                                <button class="" style="border:none; background-color: #e3ebf3"><a
                                                        class="fa fa-shopping-cart"
                                                        href="{{route('admin.order.basket', ['order' => $order->id])}}"></a>
                                                </button>
                                            @else
                                                <button class="" style="border:none; background-color: #e3ebf3"><a
                                                        class="fa fa-shopping-cart"
                                                        href="{{route('admin.order.unknown-basket', ['request' => $order->id])}}"></a>
                                                </button>
                                            @endif
                                        </td>
                                        <td>
                                            <button class="" style=""><a
                                                    class="fa fa-gamepad"
                                                    href="{{route('admin.game.allGameHistory', ['filter[mobile_id]' => $order->mobile_id])}}"></a>
                                            </button>
                                        </td>
                                        <td>{{$order->exception}}</td>
                                        <td>{{$order->failed_at ? $order->failed_at->format('Y-m-d') : ''}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script>
        $('#fromToFilter input').datepicker({language: "ru", format: "yyyy-mm-dd"});

        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
