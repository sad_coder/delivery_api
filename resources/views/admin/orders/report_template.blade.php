<{{\App\Models\OrderReport::REPORTS_TYPES[$type]}}>
<DOCUMENTNAME>{{\App\Models\OrderReport::DOCUMENT_NAMES_1C[$type]}}</DOCUMENTNAME>
<NUMBER>{{$order->id}}-0100</NUMBER>
<DATE>{{$order->created_at->format('Y-m-d')}}</DATE>
<DELIVERYDATE>{{$order->created_at->addDay()->format('Y-m-d')}}</DELIVERYDATE>
<MANAGER>{{$order->user->driver->id_1c}}</MANAGER>
<DRIVER>{{$order->user->id_1c}}</DRIVER>
<CURRENCY>KZT</CURRENCY>
<HEAD>
    <SUPPLIER>9864232489962</SUPPLIER>
    <BUYER>{{$order->user->counterparty->id_1c}}</BUYER>
    <DELIVERYPLACE>{{$order->store->custom_id_onec}}</DELIVERYPLACE>
    <INVOICEPARTNER>{{$order->user->counterparty->id_1c}}</INVOICEPARTNER>
    <SENDER>{{$order->store->custom_id_onec}}</SENDER>
    <RECIPIENT>9864232489962</RECIPIENT>
    <EDIINTERCHANGEID>0</EDIINTERCHANGEID>
    <FINALRECIPIENT>{{$order->store->custom_id_onec}}</FINALRECIPIENT>
    @foreach($order->products as $key => $product)
        @if($product->type === 0 && $type !== \App\Models\OrderReport::REPORT_TYPE_RETURN)
            <POSITION>
                <POSITIONNUMBER>{{$key + 1}}</POSITIONNUMBER>
                <PRODUCT>{{$product->presale_id}}</PRODUCT>
                <PRODUCTIDSUPPLIER/>
                <PRODUCTIDBUYER>{{$order->store->custom_id_onec}}</PRODUCTIDBUYER>
                <ORDEREDQUANTITY>{{number_format((float)$product->count, 3, '.', '')}}</ORDEREDQUANTITY>
                <ORDERUNIT>{{$product->measureCode}}</ORDERUNIT>
                <ORDERPRICE>{{ round($product->price - ($product->price / 100 * 12))}}.00</ORDERPRICE>
                <PRICEWITHVAT>{{$product->price}}.00</PRICEWITHVAT>
                <VAT>12</VAT>
                <CHARACTERISTIC>
                    <DESCRIPTION>{{$product->name_1c}}</DESCRIPTION>
                </CHARACTERISTIC>
            </POSITION>
        @endif
        @if($product->type === 1 && $type == \App\Models\OrderReport::REPORT_TYPE_RETURN)
            <POSITION>
                <POSITIONNUMBER>{{$key + 1}}</POSITIONNUMBER>
                <PRODUCT>{{$product->presale_id}}</PRODUCT>
                <PRODUCTIDSUPPLIER/>
                <PRODUCTIDBUYER>{{$order->store->custom_id_onec}}</PRODUCTIDBUYER>
                <ORDEREDQUANTITY>{{number_format((float)$product->count, 3, '.', '')}}</ORDEREDQUANTITY>
                <ORDERUNIT>{{$product->measureCode}}</ORDERUNIT>
                <ORDERPRICE>{{ round($product->price - ($product->price / 100 * 12))}}.00</ORDERPRICE>
                <PRICEWITHVAT>{{$product->price}}.00</PRICEWITHVAT>
                <VAT>12</VAT>
                <CHARACTERISTIC>
                    <DESCRIPTION>{{$product->name_1c}}</DESCRIPTION>
                </CHARACTERISTIC>
            </POSITION>
        @endif
    @endforeach</HEAD>
</{{\App\Models\OrderReport::REPORTS_TYPES[$type]}}>


