@extends('admin.layouts.index')

@section('title')
    Аудит заказов
@endsection

@section('content')
    <style>
        .storeRepName {
            cursor: pointer;
        }

        .storeRepName:hover {
            box-shadow: 0 0 11px rgba(33, 33, 33, .2);
        }
    </style>
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Аудирование заказов</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th class="defaultSort">ID изменения</th>
                                    <th>ID заказа</th>
                                    <th>Номер заказа</th>
                                    <th class="defaultSort">Редактор</th>
                                    <th class="defaultSort">Статус заказа</th>
                                    <th class="defaultSort">Корзина</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($audits as $audit)
                                    @if($audit->auditable)
                                        <tr>
                                            <td class="pk_id">{{ $audit->id }}</td>
                                            <td>{{$audit->auditable->id}}</td>
                                            <td>{{substr($audit->auditable->mobile_id,0,15)}}</td>
                                            <td class="pk_id">{{ $audit->user->full_name }}</td>
                                            <td class="pk_id">
                                                @if (isset($audit->old_values['status']))
                                                    Статус был изменен с
                                                    @if ($audit->old_values['status'] == 1)
                                                        <span class="text-italic">Сформирован</span>
                                                    @endif
                                                    @if ($audit->old_values['status'] == 2)
                                                        <span class="text-warning">Доставляется</span>
                                                    @endif
                                                    @if ($audit->old_values['status'] == 3)
                                                        <span class="text-success">Доставлен</span>
                                                    @endif
                                                    на
                                                    @if ($audit->new_values['status'] == 1)
                                                        <span class="text-italic">Сформирован</span>
                                                    @endif
                                                    @if ($audit->new_values['status'] == 2)
                                                        <span class="text-warning">Доставляется</span>
                                                    @endif
                                                    @if ($audit->new_values['status'] == 3)
                                                        <span class="text-success">Доставлен</span>
                                                    @endif
                                                @else
                                                    <span class="text-muted">Статус не менялся</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($audit->new_values['basket']) && isset($audit->old_values['basket']))
                                                    @php
                                                        $newBasket = json_decode($audit->new_values['basket'], true);
                                                        $oldBasket = json_decode($audit->old_values['basket'], true);
                                                    @endphp
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">ID продукта</th>
                                                            <th scope="col">Количество</th>
                                                            <th scope="col">тип</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($oldBasket as $item)
                                                            <tr>
                                                                <th scope="row">{{$item['product_id']}}</th>
                                                                <td>{{$item['count']}}</td>
                                                                <td>{{$item['type']}}</td>
                                                            </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="3" class="text-center">изменено на</td>
                                                        </tr>
                                                        @foreach($newBasket as $item)
                                                            <tr>
                                                                <th scope="row">{{$item['product_id']}}</th>
                                                                <td>{{$item['count']}}</td>
                                                                <td>{{$item['type']}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                @else
                                                    Корзина не менялась
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@push('scripts')
    <script>
        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush

