<style>
    span.select2.select2-container.select2-container {
        width: 89% !important;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@extends('admin.layouts.index')

@section('title')
    Добавить товар
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Редактировать товар</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.product.update',$product->id)}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form form-horizontal">
                                <ul class="nav nav-tabs">
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabGeneralInfo','title'=>'Общие',])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabSeo','title'=>'SEO'])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabImages','title'=>'Картинки'])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabCategories','title'=>'Категории'])--}}
                                    @include('admin.form_blocks.tab.nav',['name'=>'tabProducts','title'=>'Продукт'])
                                    @include('admin.form_blocks.tab.nav',['name'=>'tabImages','title'=>'Картинки'])
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabCities','title'=>'Города'])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabAlternativeProducts','title'=>'Cвязанные товары'])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabProductsMarkers','title'=>'Маркеры'])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabAlternativeProductName','title'=>'доп названия'])--}}
                                    {{--                                    @include('admin.form_blocks.tab.nav',['name'=>'tabFilialDeliveryBlock','title'=>'Филиалы'])--}}

                                </ul>
                                <div class="tab-content px-1 pt-1  border-grey border-lighten-2 border-0-top">

                                    @component('admin.form_blocks.tab.content',['name'=>'tabImages'])
                                        @include('admin.form_blocks.file',['label'=>'Картинки','name'=>'images[]','load'=>'','multiple'=>false, 'value' => $images])
                                    @endcomponent

                                    @component('admin.form_blocks.tab.content',['name'=>'tabProducts', 'active' => true])
                                        <div class="tab-content  px-1 pt-1 ">
                                            <div>
                                                <label for="" class="col-md-10">Артикул<input type="text" value="{{ $product->article }}" name="article" class="form-control"></label>
                                                <label
                                                    for="" class="col-md-10">Брэнд
                                                    <select name="categories_id[]" id="" class="form-control">
                                                        @foreach($brands as $brand)
                                                            <option value="">{{$brand->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </label>
                                                    <label
                                                        for="" class="col-md-10">Категория
                                                        <select name="categories_id[]" id="" class="form-control">
                                                            @foreach($categories as $category)
                                                                <option value="">{{$category->name_1c}}</option>
                                                            @endforeach
                                                        </select>
                                                    </label>
                                                            <label for="" class="col-md-10">Мерка<input type="text" value="{{ $product->measure_id }}" name="measure_id" class="form-control"></label>
                                                                <label for="" class="col-md-10">Имя<input type="text" value="{{ $product->name }}" name="name" class="form-control"></label>
                                                                     <label for="" class="col-md-10">Цена<input type="text" value="{{ $product->price }}" name="price" class="form-control"></label>
                                                                        <label for="" class="col-md-10">Описание<input type="text" value="{{ $product->description }}" name="description" class="form-control"></label>
                                                                            <label for="" class="col-md-10">Остаток<input type="text" value="{{ $product->remainder }}" name="remainder" class="form-control"><br></label>
                                                <label for="">Аудио<input type="file" value="{{ $product->audio }}" name="audio"></label>
                                                <audio controls style="height:54px;"><source src="{{$product->full_audio_path}}"></audio>
                                            </div>
                                        </div>
                                    @endcomponent
                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabGeneralInfo','active'=>'1'])--}}
                                    {{--                                        <div--}}
                                    {{--                                            class="tab-content  px-1 pt-1 ">--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Название товара','name'=>'name', 'id'=>'product_name', 'generateSlug' => true,'value'=>$product->name])--}}
                                    {{--            {{$record->recording_filename}}                                @include('admin.form_blocks.input',['type'=>'text','label'=>'Заголовок','name'=>'title', 'value'=>$product->title])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Цена','name'=>'price','value'=>$product->price])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'checkbox','label'=>'Отображать на главной','name'=>'display_main_page','value'=>$product->display_main_page,'id'=>'main-page-display'])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'checkbox','label'=>'Популярный товар в поиске?','name'=>'popular_in_search','value'=>$product->popular_in_search,'id'=>'main-page-display'])--}}
                                    {{--                                            @include('admin.form_blocks.radio',['items'=>$mainPageBlocks,'edit' =>$product->popular_in_search, 'product' => $product ])--}}
                                    {{--                                            @include('admin.form_blocks.textarea',['label'=>'Описание','name'=>'category', 'summernote'=>1 , 'text'=>$product->description])--}}
                                    {{--                                            @include('admin.form_blocks.textarea',['label'=>'Краткое описание','name'=>'short_description', 'placeholder'=> 'Краткое описание', 'text'=>$product->short_description])--}}

                                    {{--                                        </div>--}}
                                    {{--                                    @endcomponent--}}
                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabSeo',])--}}
                                    {{--                                        <div class="tab-content  px-1 pt-1 ">--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Slug - ссылка','name'=>'slug', 'id'=>'slug', 'value'=>$product->slug])--}}
                                    {{--                                            @include('admin.form_blocks.input',['type'=>'text','label'=>'Seo - заголовок','name'=>'seo_title', 'value' => $product->seo_title])--}}
                                    {{--                                            @include('admin.form_blocks.textarea',['label'=>'Seo Описание','name'=>'seo_description','text'=>$product->seo_description])--}}
                                    {{--                                        </div>--}}
                                    {{--                                    @endcomponent--}}

                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabImages'])--}}
                                    {{--                                        @include('admin.form_blocks.upload_images')--}}

                                    {{--                                    @endcomponent--}}

                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabCategories'])--}}
                                    {{--                                        <div class="form-group сategories">--}}
                                    {{--                                            <label for="exampleFormControlSelect2">Выбрать--}}
                                    {{--                                                категорию</label>--}}
                                    {{--                                            <select name="categories_id[]" multiple class="form-control" size="18"--}}
                                    {{--                                                    id="exampleFormControlSelect2">--}}
                                    {{--                                                @foreach($categories as $category )--}}
                                    {{--                                                    <option--}}
                                    {{--                                                        {{in_array($category->id,$productCategories) ? 'selected' : ''}}--}}

                                    {{--                                                        value="{{$category->id}}">{{$category->title}}
                                                                                            </option>--}}
                                    {{--                                                @endforeach--}}
                                    {{--                                            </select>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    @endcomponent--}}
                                    {{--                                    --}}{{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabCities'])--}}
                                    {{--                                    --}}{{--                                        @TO DO add cities--}}
                                    {{--                                    --}}{{--                                    @endcomponent--}}
                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabAlternativeProducts'])--}}
                                    {{--                                        @include('admin.form_blocks.ajaxSearch',['editMode'=>true])--}}
                                    {{--                                    @endcomponent--}}
                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabProductsMarkers'])--}}
                                    {{--                                        @include('admin.form_blocks.select2',['label'=>'Аренда','name'=>'rent_link','url'=>route('admin.product.markers.by.ajax'),'load'=>'', 'labelClass'=> '1_label', 'selectClass'=>'select_class', 'loadItem' =>$markers[\App\Models\Marker::RENT_CLASS]])--}}
                                    {{--                                        @include('admin.form_blocks.select2',['label'=>'Ремонт','name'=>'repairs_link','url'=>route('admin.product.markers.by.ajax'),'load'=>'', 'labelClass'=> '2_label','selectClass'=>'select_class', 'loadItem' =>$markers[\App\Models\Marker::REPAIRS_CLASS]])--}}
                                    {{--                                        @include('admin.form_blocks.select2',['label'=>'Запчасти','name'=>'spares_link','url'=>route('admin.product.markers.by.ajax'),'load'=>'', 'labelClass'=> '3_label','selectClass'=>'select_class', 'loadItem' =>$markers[\App\Models\Marker::SPARES_CLASS]])--}}
                                    {{--                                        @include('admin.form_blocks.select2',['label'=>'Купить','name'=>'spares_link','url'=>route('admin.product.markers.by.ajax'),'load'=>'', 'labelClass'=> '4_label','selectClass'=>'select_class'])--}}
                                    {{--                                    @endcomponent--}}

                                    {{--                                    @component('admin.form_blocks.tab.content',['name'=>'tabAlternativeProductName'])--}}
                                    {{--                                        @include('admin.form_blocks.alternative-product-name',['editMode'=>true])--}}
                                    {{--                                    @endcomponent--}}

                                    {{--                                        @component('admin.form_blocks.tab.content',['name'=>'tabFilialDeliveryBlock'])--}}
                                    {{--                                            @include('admin.form_blocks.filails-product',['filialsId' => $filialsId])--}}
                                    {{--                                        @endcomponent--}}
                                </div>
                            </div>
                            <br><br>
                            @include('admin.form_blocks.submit',['title'=>'Изменить'])
                            @include('admin.form_blocks.error')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            function generateSlug(value) {
                //console.log(value);
                $.ajax({
                    type: 'POST',
                    url: '/admin/product/generate_slug',
                    data: {
                        slug: value,
                    },

                    success: function (response) {
                        $('#slug').val(response);
                    },
                });
            }

        </script>
    @endpush

@endsection
