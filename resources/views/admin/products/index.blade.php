@extends('admin.layouts.index')

@section('title')
    Товары
@endsection

@section('content')

    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Товары</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div style="margin-left: 2%" class="crud-create">
                            <a href="{{route('admin.product.create')}}" type="button"
                               class="btn btn-success btn-min-width"><i style="margin-right: 5px"
                                                                        class="fa fa-check"></i>Создать</a>
                        </div>
                        <div style="margin-left: 2%;" class="crud-create">
                            <a href="{{route('admin.product.update-remainders')}}" type="button"
                               class="btn btn-success btn-min-width"><i style="margin-right: 5px"
                                                                        class="fa fa-refresh"></i>Обновить остатки продуктов</a>
                        </div>
                        <div style="margin-left: 2%;" class="crud-create">
                            <a href="{{route('admin.product.download-catalogue')}}" type="button"
                               class="btn btn-warning btn-min-width"><i style="margin-right: 5px"
                                                                        class="fa fa-download"></i>Скачать каталог</a>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th>Actions</th>
                                    <th>Имя 1С</th>
                                    <th class="defaultSort">ID</th>
                                    <th>ID 1C</th>
                                    <th>Артикул</th>
                                    <th>Брэнд</th>
                                    <th>Картинка</th>
                                    <th>Категории</th>
                                    <th>Мерка</th>
                                    <th>Имя</th>
                                    <th>Цена</th>
                                    <th>Описание</th>
                                    <th>Остаток</th>
                                    <th>Создано</th>
                                    <th>Доступен</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            @include('admin.form_blocks.actions',['route_edit'=>route('admin.product.edit',$product->id),'route_destroy'=>route('admin.product.destroy',$product)])
                                        </td>
                                        <td class="pk_id">{{ $product->name_1c }}</td>
                                        <td class="pk_id">{{ $product->id }}</td>
                                        <td class="pk_id">{{ $product->id_1c }}</td>
                                        <td class="pk_id">{{ $product->article }}</td>
                                        <td class="pk_id">{{ $product->brand->name ?? '' }}</td>
                                        <td class="pk_id"><img style="height:55px; width: 55px;"
                                                               src="{{$product->full_image_path}}"/></td>
                                        <td>{{ $product->category->name_1c ?? '' }}</td>
                                        <td class="pk_id">{{ $product->measure_id }}</td>
                                        <td class="pk_id">{{ $product->name }}</td>
                                        <td class="pk_id">{{ $product->price }}</td>
                                        <td class="pk_id">{{ $product->description }}</td>
                                        <td class="pk_id">{{ $product->remainder }}</td>
                                        <td>{{ $product->created_at }}</td>
                                        <td>{{$product->enabled ? 'Да' : 'Нет'}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

    <script src="/robust-admin/app-assets/js/scripts/extensions/jquery-sortable.js" type="text/javascript"></script>

    <script>

        $('.destroyFormSubmit').click(function () {
            return confirm('Вы действительно хотите удалить?');
        });

        /*datatables*/
        var table = $('#categoriesTable').dataTable({
            order: [[$('th.defaultSort').index(), 'asc']],
            aLengthMenu: [7, 25, 50, 100],
        });

        // Sortable rows
        $('.sorted_table').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);
                sortdb();
            }
        });

        function sortdb() {
            var table = $('.sorted_table tbody tr');
            $perPage = $("select[name=categoriesTable_length]").val();
            $first_position = parseInt(table.find('td.position').eq(0).text()) - 1;
            $start_pos = parseInt($first_position / $perPage) * $perPage;
            $pos = $start_pos;
            table.each(function (index) {
                $(this).find('td.position').text(++$pos);
                $id = $(this).find('td.pk_id').text();
            });

        }
    </script>

@endpush


@push('styles')

    <style>
        .dragged {
            position: absolute;
            top: 0;
            opacity: 0.5;
            z-index: 2000;
        }

        .placeholder:before {
            content: "";
            position: relative;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-top-color: red;
            top: -6px;
            margin-left: -5px;
            border-bottom: none;
        }
    </style>

@endpush
