@extends('admin.layouts.index')

@section('title')
    Обновить остатки продуктов
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Загрузите эксель файл</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('admin.product.updateRemainders') }}"
                              method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form form-horizontal">
                                <div class="tab-content px-1 pt-1  border-grey border-lighten-2 border-0-top">
                                    @include('admin.form_blocks.file',['label'=>'Файл','name'=>'file','multiple'=>false, 'load' => ''])
                                    @if (session('success'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @include('admin.form_blocks.submit',['title'=>'Загрузить'])
                            @include('admin.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
