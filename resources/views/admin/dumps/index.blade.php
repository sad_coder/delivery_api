@extends('admin.layouts.index')

@section('title')
    Бекапы базы данных
@endsection

@section('content')
    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Бекапы</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="categoriesTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th class="defaultSort">Нумерация</th>
                                    <th>Название</th>
                                    <th>Создано</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dumps as $key=>$dump)
                                    <tr>
                                        <td class="pk_id">{{ $key }}</td>
                                        <td>{{$dump}}</td>
                                        <td>{{\Carbon\Carbon::createFromFormat('YmdHis',str_replace('.sql','',explode('_', $dump)[1]))->format('Y-m-d')}}</td>
                                        <td>
                                            @include('admin.form_blocks.actions',['route_download'=>route('admin.dump.download',['dumpName' => $dump]), 'destroy_disable' => true])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

