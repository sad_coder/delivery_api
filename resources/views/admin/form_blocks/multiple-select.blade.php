@if(isset($selectedId))
    <div class="form-group сategories">
        <label for="exampleFormControlSelect2">{{$label}}</label>
        <select name="{{$name}}[]" multiple class="form-control" size="18"
                id="exampleFormControlSelect2">
            @foreach($items as $item )
                <option value="{{$item->id}}" {{in_array($item->id,$selectedId) ? "selected" : null}}   ">{{$item->$attrName}}</option>
            @endforeach
        </select>
    </div>

@else

<div class="form-group сategories">
    <label for="exampleFormControlSelect2">{{$label}}</label>
    <select name="{{$name}}[]" multiple class="form-control" size="18"
            id="exampleFormControlSelect2">
        @foreach($items as $item )
            <option value="{{$item->id}}">{{$item->$attrName}}</option>
        @endforeach
    </select>
</div>
@endif
