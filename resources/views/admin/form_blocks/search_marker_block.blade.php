{{--//search/product_categories--}}

@if($needle_link === 'Аренда')
    <select value="" class="{{$class}} selected_search" id="rent_link" onchange="chandeFunctRent(value)">
        <option  selected value>--выберите ссылку--</option>
    </select>
@endif
@if($needle_link === 'Ремонт')
    <select value="" class="{{$class}} selected_search" id="repairs_link" onchange="chandeFunctRepairs(value)">
        <option  selected value>--выберите ссылку--</option>
    </select>
@endif

@if($needle_link === 'Запчасти')
    <select value="" class="{{$class}} selected_search" id="spare_parts_link" onchange="chandeFunctSpare(this)">
        <option selected value>--выберите ссылку--</option>
    </select>
@endif

<script !src="">

    @if($class === 'rent_class')
    function chandeFunctRent(value) {
        //console.log(value);
        let inputElement = document.getElementById("{{$class}}");
        inputElement.value = value;
        let selectElement = document.getElementsByClassName("{{$class}}")[0];
        selectElement.setAttribute('size',0);
        $(".rent_class").empty();


    }
    @endif

    @if($class === 'repairs_class')
    function chandeFunctRepairs(value) {
        let inputElement = document.getElementById("{{$class}}");
        inputElement.value = value;
        let selectElement = document.getElementsByClassName("{{$class}}")[0];
        selectElement.setAttribute('size',0);
        $(".repairs_class").empty();
    }

    @endif
    @if($class === 'spares_class')
    function chandeFunctSpare(value) {
        //console.log(value);
        let inputElement = document.getElementById("{{$class}}");
        inputElement.value = value;
        let selectElement = document.getElementsByClassName("{{$class}}")[0];
        selectElement.setAttribute('size',0);
        $(".spares_class").empty();
    }
    @endif



</script>

