<div role="tabpanel" class="tab-pane {{isset($active)?'active':''}}" id="{{$name}}" aria-expanded="true" aria-labelledby="base-{{$name}}">
    <div class="form-body">
        {{$slot}}
    </div>
</div>
