<li class="nav-item">
    <a class="nav-link {{isset($active)?'active':''}}" id="base-{{$name}}" data-toggle="tab" aria-controls="{{$name}}" href="#{{$name}}" aria-expanded="true">{{$title}}</a>
</li>
