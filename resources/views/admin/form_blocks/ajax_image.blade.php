<input id="sendImage" data-buttonText="Your label here."
       type="file" name="file" placeholder="test"
       multiple>
<button type="button" onclick="addPhoto" id="addPhoto"
        class="mt-2 add-photo btn btn-outline-success">
    Добавить изображение
</button>
<input name="imageId" id="imageId" type="hidden" value="">
<div id="image-container"></div>


<script>
    let imageContainer = document.getElementById('image-container');

    function addPhoto() {

        let formData = new FormData();
        formData.append('file', $('#sendImage')[0].files[0]);
        $.ajax({
            type: "POST",
            url: '{{route('admin.ajax.add_image')}}',
            data: formData,
            processData: false,  // Important!
            contentType: false,
            contentType: false,
            cache: false,

            success: function (response) {
                //console.log(response);

                //console.log(response['url']);
                while (imageContainer.firstChild) {
                    imageContainer.removeChild(imageContainer.firstChild);
                }
                let image = document.createElement("img");
                let imgId = document.getElementById("imageId");
                imgId.value = response['id'];
               // console.log(response['id']);
                image.setAttribute('width', 500);
                image.setAttribute('src', response['url']);
                image.setAttribute('height', 'auto');
                imageContainer.appendChild(image);


            },
        });

    }

    document.getElementById("addPhoto").addEventListener("click", addPhoto);

    @if(isset($category))
    @if($category->mainImagePath)
        let image = document.createElement("img");
        image.setAttribute('width', 500);
        image.setAttribute('src', "{{$category->mainImagePath}}");
        image.setAttribute('height', 'auto');
        imageContainer.appendChild(image);
    @endif
    @endif
</script>
