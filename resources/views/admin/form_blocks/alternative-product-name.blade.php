<style>
    .parent-alternative-name-block {
        display: flex;
        flex-wrap: wrap;
    }

    .add_alternative-input {
        width: 100%;
        margin-left: 21%;
        margin-top: 2%;
        margin-bottom: 2%;
    }

    .parent-alternative-name-block {
        width: 100%;
    }

    .add_alternative-input > input {
        width: 80%;
        font-size: 17px;
        text-align: center;
    }

    select.getIssetAlternativeNames {
        width: 100%;
    }

    input#sad_alternative {
        margin: 4% 0% 10% 25%;
        width: 50%;
        font-size: 19px;
        text-align: center;
    }

    .parent-alternative-name-block {
        display: flex;
        flex-direction: column;
    }


    div#ignoretags {
        display: flex;
        margin: 1% 1% 3%;
        flex-wrap: wrap;
    }

    .ignore_keywords {
        margin-right: 3%;
    }

    .deleteIgnoredIcon {
        width: 19px;
        position: absolute;
        top: -4%;
        right: -3%;
        transition: .3s;
    }

    .ignore_keywords:hover > svg {
        transform: scale(1);
    }

    .ignore_keywords {
        position: relative;
    }

    button.remove {
        width: 25px;
        background: #ff000000;
        border: none;
        position: absolute;
        top: -8%;
        right: 0%;
        transform: scale(0);
        transition: .4s;
    }

    .ignore_keywords:hover > button.remove {
        transform: scale(1);
    }

    .ignore_keywords {
        font-size: 15px;
        margin-top: .4%;
        background: #1574de;
        color: white;
        font-weight: 900;
        padding: 5px;
        border-radius: 3px;
        text-shadow: 1px 2px 0px #0000008f;
    }


</style>

<div class="parent-alternative-name-block">

    <input type="text" id="sad_alternative" onkeydown="isKeyPressedAlternative(event)"
           placeholder="Альтернативные слова для поиска"
           name="price_max">

    <select onchange="getSelectValue()" onkeydown="cleanSelectValue(event)" multiple class="form-control"
            name="selectedKeyWord" id="selectedKeywords">
    </select>

    <div class="alternativeNames" id="ignoretags">

    </div>

    <input type="hidden" id="allAlternativeNames" value="" name="allAlternativeProduct">

</div>

<script>

    let closeSvg = '<button type="button" class="remove"><svg class="deleteIgnoredIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">\n' +
        '<circle style="fill:#D75A4A;" cx="25" cy="25" r="25"/>\n' +
        '    <polyline style="fill:none;stroke:#FFFFFF;stroke-width:7;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16   "/>\n' +
        '    <polyline style="fill:none;stroke:#FFFFFF;stroke-width:7;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34   "/>\n' +
        '</svg></button>';
    let ignoredKeywords = [];
    let CountKeyword = 0;
    let selectedValues = [];
    let IgnoredWordSelect = document.getElementById("selectedKeywords");

    // if isset alternative  names for edit

    @if(isset($alternativeNames))

        @foreach($alternativeNames as $alternativeName)
            $('#ignoretags').append('<div data-keyword-id=' + CountKeyword + ' class="ignore_keywords">' + "{{$alternativeName}}" + closeSvg + '</div>');
            ignoredKeywords.push("{{$alternativeName}}");
            CountKeyword++;

        @endforeach
    @endif
    // if isset alternative  names for edit


    function isKeyPressedAlternative(event) {
        let alternativeNames = $('#sad_alternative').val();
        console.log(alternativeNames);
        $.ajax({
            type: 'POST',
            url: '/admin/get_ajax_alternative_names',
            data: {
                alternativeNames: alternativeNames,
            },


            success: function (response) {
                let removedOldSearchElements = document.getElementsByClassName("KeywordOption");
                while (removedOldSearchElements[0]) {
                    removedOldSearchElements[0].parentNode.removeChild(removedOldSearchElements[0]);
                }

                response.forEach(function (ignoredWord) {
                    let option = document.createElement("option");
                    option.text = ignoredWord;
                    option.value = ignoredWord;
                    option.className = "KeywordOption";
                    IgnoredWordSelect.setAttribute('size', response.length);
                    IgnoredWordSelect.add(option);
                })
            },
        });

        if (event.keyCode == 13) {
            let regex = new RegExp("^\s*$");
            let input = $('#sad_alternative').val();
            let inputCheck = regex.test(input);
            if (!inputCheck) {
                let ignoreValue = $('#sad_alternative').val();
                console.log(ignoredKeywords);
                if (!ignoredKeywords.includes(ignoreValue)) {

                    $('#ignoretags').append('<div data-keyword-id=' + CountKeyword + ' class="ignore_keywords">' + ignoreValue + closeSvg + '</div>');
                    ignoredKeywords.push(ignoreValue);
                    CountKeyword++;

                    let alternativeName = ignoreValue;
                    $.ajax({
                        type: 'POST',
                        url: '/admin/add_ajax_alternative_name',
                        dataType: 'JSON',
                        data: {
                            alternativeName: alternativeName,
                        }
                    });
                }
                $('input#sad_alternative').val('');

            }
            // convert values ignored words
            $('#allAlternativeNames').val(ignoredKeywords.join(','));
        }
    }


    function getSelectValue() {
        let selectElement = document.getElementById('selectedKeywords');
        selectedValues = Array.from(selectElement.selectedOptions)
            .map(option => option.value);


    }

    function cleanSelectValue(event) {

        if (event.keyCode == 13) {
            selectedValues.forEach(function (ignoreValue) {
                    if (!ignoredKeywords.includes(ignoreValue)) {
                        ignoredKeywords.push(ignoreValue);
                        $('#ignoretags').append('<div data-keyword-id=' + CountKeyword + ' class="ignore_keywords">' + ignoreValue + closeSvg + '</div>');
                    }
                }
            );
            document.getElementById('selectedKeywords').setAttribute('size', 0);
            // convert values ignored words
            $('#allAlternativeNames').val(ignoredKeywords.join(','));


            $('#selectedKeywords').empty();
        }
    }

    $(document).on('click', "button.remove", function (e) {
        e.preventDefault();
        let deletedKeywordValue = this.parentElement.textContent.replace(/\s+/g, " ").trim();
        ignoredKeywords = ignoredKeywords.filter(e => e !== deletedKeywordValue);
        $(this).closest(".ignore_keywords").remove();
        console.log(ignoredKeywords);
        $('#allAlternativeNames').val(ignoredKeywords.join(','));
        console.log($('#allAlternativeNames').val());
        return false;
    });


</script>




