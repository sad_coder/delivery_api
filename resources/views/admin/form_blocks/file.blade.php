<div class="form-group row">
    <label class="label-control col-md-3 col-sm-3">{{$label}}</label>
    <div class="col-md-9 col-sm-9">
        <input id="dropFile" name="{{$name}}" type="file"
               class="" {{isset($multiple)?'multiple':''}} {{isset($required)?'required':''}} >
    </div>
</div>
@if(isset($load))

    @push('scripts')
        <script src="/robust-admin/app-assets/vendors/js/bootstrap-fileinput/js/fileinput.min.js"
                type="text/javascript"></script>

        <script>
            $(document).ready(function () {

                $("#dropFile").fileinput({
                    showUpload: false,
                    previewFileType: 'any',
                    @if(isset($value))
                    initialPreviewFileType: 'image',
                    initialPreviewAsData: true,
                    initialPreview: [
                        @foreach($value as $item)
                            '{{$item['path']}}',
                        @endforeach
                    ],
                    initialPreviewConfig: [
                            @foreach($value as $image)
                        {
                            downloadUrl: "{{$image['path']}}"
                        },
                        @endforeach
                    ],

                    overwriteInitial: true,
                    @endif
                });

            });
        </script>
    @endpush

    @push('styles')
        <link rel="stylesheet" type="text/css"
              href="/robust-admin/app-assets/vendors/js/bootstrap-fileinput/css/fileinput.min.css">
        <link rel="stylesheet" type="text/css"
              href="/robust-admin/app-assets/vendors/css/bootstrap4-glyphicons/css/bootstrap-glyphicons.min.css">
    @endpush
@endif
