<style>
    .search-block {
        width: 100%;
    }

    input.searchProducts {
        width: 90%;
        margin: 5% 5%;
        font-size: 19px;
        text-align: center;
    }

    div#search-result {
        display: flex;
        flex-direction: column;
    }

    .one-product-search-block>img {
        /* width: 38%; */
        margin-right: 20%;
        margin-bottom: 6%;
    }

    span.product-search-title {
        font-size: 21px;
        color: black;
        font-weight: 900;
        text-shadow: 1px 0px 0px #9292928c;
    }

    .one-product-search-block {
        display: flex;
        position: relative;
        margin-bottom: 3%;
        margin-top: 1%;
    }
    .parent_search_image_block {
        width: 33%;
    }

    span.product-search-title {
        width: 55%;
        margin-left: 5%;
        margin-top: 3%;
    }


    .parent_button_block {
        position: absolute;
        border: none;
        left: 85%;
        top: 40%;
    }

    button.add-search-product-button {
        background-color: green;
        color: white;
        font-size: 15px;
        border-radius: 6px;
        text-shadow: -1px 2px 0px black;
        font-weight: 900;
    }

    h3#pinned-product-title {
        text-align: center;
    }

    .one-pinned-product {
        display: flex;
        margin-top: 3%;
    }

    .parent-image-pinned-product {
        width: 30%;
    }

    span.product-search-title {
        width: 50%;
        margin-left: 7%;
    }

    .pinned_parent_block {
        position: absolute;
        right: 6%;
        top: 30%;
    }

    .one-pinned-product {
        position: relative;
    }

    button.product-unpin-button {
        color: #fff;
        font-weight: 900;
        text-shadow: -1px 2px 0px #00000073;
        font-size: 14px;
        border: none;
        border-radius: 3px;
        background-color: red;
        padding: 12px;
        cursor: pointer;
        transition: .5s;
    }

    span.product-search-title {
        margin-top: 9%;
    }

    button.product-unpin-button:hover {
        background-color: #a70c0ceb;
    }


</style>


<div class="search-block">
    <input type="hidden" id="alternativeProducts" value="" name="alternativeProducts">
    <input type="text" class="searchProducts" onkeydown="isKeyPressed(event)"
          id="searchName" placeholder="просто напиши название товара">
</div>
<div id="search-result">

</div>
<hr>
<h3 id="pinned-product-title">Закрепленные товары</h3>
<div id="product-pinned">
    @if(isset($editMode))
        @foreach($product->child as $pinnedProduct)
            {{--                @php dd($pinnedProduct); @endphp--}}
            <div class="one-product-search-block">
                <img src="{{$pinnedProduct->main_image_path}}" alt="" class="" width="250" height="200">
                <span class="product-search-title">{{$pinnedProduct->name}}</span>
                <div class="parent_button_block">
                    <button class="add-search-product-button" id="{{$pinnedProduct->id}}">закрепить товар</button>
                </div>
            </div>

        @endforeach
    @endif
</div>

@push('scripts')
    <script>
            let alternativeProducts = [];
        function isKeyPressed(event) {
            let searchName = $('#searchName').val();
            //console.log(searchName);
            $('#search-result').empty();

            $.ajax({
                type: 'POST',
                url: '/admin/product/search/alternativeProduct',
                data: {
                    product_name: searchName,
                },


                success: function (response) {
                    let parentBlock = document.getElementById('search-result');
                    //console.log(response);
                    response.forEach(function (product) {
                        let productBlock = document.createElement("div");
                        let parentImageBlock = document.createElement("div");
                        let productImg = document.createElement("img");
                        let productTitle = document.createElement('span');
                        let actionButton = document.createElement('button');
                        let parentButtonBlock = document.createElement('div');
                        actionButton.className = "add-search-product-button";
                        actionButton.textContent = "закрепить товар";
                        actionButton.setAttribute('id',product['id']);
                        parentImageBlock.className = "parent_search_image_block";
                        parentButtonBlock.className = "parent_button_block";

                        productTitle.className = 'product-search-title';
                        productTitle.textContent = product['name'];
                        productImg.src = product['image'];
                        productImg.width = 250;
                        productImg.height = 200;
                        productBlock.className = 'one-product-search-block';

                        parentButtonBlock.appendChild(actionButton);
                        parentImageBlock.appendChild(productImg);
                        productBlock.appendChild(parentImageBlock);
                        productBlock.appendChild(productTitle);
                        productBlock.appendChild(parentButtonBlock);
                        parentBlock.appendChild(productBlock);
                        //console.log(product['image']);
                    })
                },
            });
        }


        $(document).on('click', ".add-search-product-button", function (e) {
            e.preventDefault();
            let alternativeProductsId = this.getAttribute('id');
            let parentPinnedBlock = document.getElementById('product-pinned');
            if(!alternativeProducts.includes(alternativeProductsId)){
                alternativeProducts.push(alternativeProductsId);
               // console.log(alternativeProducts);
                let pinnedProductBlock = document.createElement("div");
                let parentPinnedImageBlock = document.createElement("div");
                let pennedProductImg = document.createElement("img");
                let pinnedproductTitle = document.createElement('span');
                let pinnedactionButton = document.createElement('button');
                let pinnedButtonBlock = document.createElement('div');

                // this varibale needle for get image src
                let imageDivElement = this.parentNode.parentNode.firstChild;

                // this varibale needle for get product name
                let parentNodeElement=this.parentNode.parentNode;
                for(let i = 0; i< parentNodeElement.childNodes.length; i++) {
                    if(parentNodeElement.childNodes[i].className == "product-search-title") {
                        pinnedproductTitle.textContent = parentNodeElement.childNodes[i].textContent;
                    }
                }



                pinnedproductTitle.className = 'product-search-title';
                pennedProductImg.src = imageDivElement.firstChild.getAttribute('src');
                pennedProductImg.width = 250;
                pennedProductImg.height = 200;
                pinnedProductBlock.className = "one-pinned-product";
                pinnedactionButton.className = "product-unpin-button";
                pinnedactionButton.textContent = "открепить товар";
                pinnedButtonBlock.className = "pinned_parent_block";
                parentPinnedImageBlock.className = "parent-image-pinned-product";
                pinnedactionButton.setAttribute('id',this.getAttribute('id'));

                pinnedButtonBlock.appendChild(pinnedactionButton);
                parentPinnedImageBlock.appendChild(pennedProductImg);
                pinnedProductBlock.appendChild(parentPinnedImageBlock);
                pinnedProductBlock.appendChild(pinnedproductTitle);
                pinnedProductBlock.appendChild(pinnedButtonBlock);

                parentPinnedBlock.appendChild(pinnedProductBlock);

                let convertValueStringAlternativeProducts = alternativeProducts.join(',');

                $('#alternativeProducts').val(convertValueStringAlternativeProducts);
                //console.log($('#alternativeProducts').val());


            }
            return false;
        });

        $(document).on('click', ".product-unpin-button", function (e) {
            e.preventDefault();
            //get product id
//            console.log(this.parentNode.parentNode);

            this.parentNode.parentNode.remove();
            //alternativeProducts = alternativeProducts.filter(function (e) { return e !==})
            return false;
        });





    </script>


@endpush
