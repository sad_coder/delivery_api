@if(isset($hide))

    <select class="{{$class ?? 'simple-select2'}} form-control" name="{{$name}}">

        @if(isset($items))
            @if(isset($firstValue))
                <option value="" selected}}></option>
            @endif
            @foreach($items as $item)
                <option
                    value="{{$item->id}}" {{((isset($value)&&$item->id==$value) || old($name)==$item->id)? 'selected' : ''}}>{{$item->name}}</option>
            @endforeach
        @endif

    </select>
@else
    <div class="form-group row">
        <label class="label-control col-md-2 col-sm-2 {{isset($labelClass) ? $labelClass : null}}"></label>
        <div class="col-md-9 col-sm-9">
            <select class="{{$class ?? 'simple-select2'}} form-control" name="{{$name}}" onchange="{{$onchange ?? ''}}">{{$label}} id="{{$id?? ''}}">
                @if(isset($loadItem))
                    <option
                        value="{{$loadItem->id}}" selected>{{$loadItem->$field}}</option>

                @elseif(isset($items))
                    @foreach($items as $item)
                        <option
                            value="{{$item->id}}" {{isset($selectedId) && $selectedId == $item->id ? 'selected' : ''}}>{{$item->name ?? $item->$field}}</option>
                    @endforeach
                @endif

            </select>
        </div>
    </div>
@endif

@push('styles')
    @if(isset($load))
        <link rel="stylesheet" type="text/css"
              href="/robust-admin/app-assets/vendors/css/forms/selects/select2.min.css">
    @endif
@endpush


@if(isset($load))
    @push('scripts')
        <script src="/robust-admin/app-assets/vendors/js/forms/select/select2.full.min.js"
                type="text/javascript"></script>
    @endpush
@endif

@if(isset($url))
    @push('scripts')
        <script>

            var select2 = $(".{{$class ?? 'simple-select2'}}");

            select2.select2({
                ajax: {
                    url: '{{$url}}',
                    dataType: 'json',
                    processResults: function (data) {
                        console.log(data);
                        return {
                            results: data
                        };
                    }
                }
            });

            @if(isset($value) || old($name))

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "{{$url . '?id=' . (old($name) ?: $value) }}"
            }).then(function (data) {
                console.log(data);

                let val = data.id

                var option = new Option(data.text, val, true, true);
                select2.append(option).trigger('change');
            });

            @endif

        </script>
    @endpush
@elseif(isset($init))
    @push('scripts')
        <script>
            $(document).ready(function () {
                var simpleSelect2 = $(".{{$selectClass ?? 'simple-select2'}}");
                simpleSelect2.select2({
                    width: '100%'
                });
            });

            // select2.select2({
            //     placeholder: "Select a state",
            //     allowClear: true
            // });
        </script>
    @endpush
@endif
