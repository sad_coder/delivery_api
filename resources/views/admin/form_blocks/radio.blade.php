<style>
    .radio-block-main {
        display: flex;
    }

    .radio-block-main>p {
        margin-left: 7%;
    }
</style>

<div class="radio-block-main" style="display: none">

    @if(isset($product))
        @foreach($items as $k => $item)
            <p><input type="radio" name="main-blocks" value="{{$k}}" {{$product->main_page_block == $k ? 'checked' : null}}>{{$item}}</p>
        @endforeach
    @else
        @foreach($items as $k => $item)
            <p><input type="radio" name="main-blocks" value="{{$k}}">{{$item}}</p>
        @endforeach

    @endif

</div>

<script>

    @if(isset($edit))

        @if($edit ==1)
            $('.radio-block-main').css('display','flex');
            @php unset($edit) @endphp
        @endif
    @endif

    $("#main-page-display").change(function() {
        if(this.checked) {
            $('.radio-block-main').css('display','flex')
        }else{
            $('.radio-block-main').css('display','none')
        }
    });


</script>
