@if(isset($route_edit))

    <a href="{{$route_edit}}">
        <button class="btn btn-sm btn-primary" style="margin-bottom: 5px;"><i class="fa fa-pencil"></i> Редактировать
        </button>
    </a>
@endif

@if(!isset($destroy_disable) || (isset($destroy_disable) && !$destroy_disable))
    <form action="{{$route_destroy}}" method="post">
        @csrf
        @method('DELETE')
        @if(!isset($restore))
            <button type="submit" class="btn btn-{{isset($hide) ? 'warning' : 'danger'}} btn-sm destroyFormSubmit">
                <i style="margin-right: 5px"
                   class="fa fa-{{isset($hide) ? 'eye-slash' : 'trash'}}"></i>{{isset($hide) ? 'Скрыть' : 'Удалить'}}
            </button>
        @else
            <button type="submit" class="btn btn-success btn-sm destroyFormSubmit"><i style="margin-right: 5px"
                                                                                      class="fa fa-window-restore"></i>Восстановить
            </button>
        @endif
    </form>
@endif

@if(isset($route_lookup_orders))
    <a href="{{$route_lookup_orders}}">
        <button class="btn btn-sm btn-secondary" style="margin-top: 5px;"><i class="fa fa-list"
                                                                             style="margin-right: 4px;"></i>Заказы
        </button>
    </a>
@endif

@if(isset($route_lookup_delivery_statuses))
    <a href="{{$route_lookup_delivery_statuses}}">
        <button class="btn btn-sm btn-secondary" style="margin-top: 5px;"><i class="fa fa-car"
                                                                             style="margin-right: 4px;"></i>Заказы
        </button>
        <button class="btn btn-sm btn-secondary" style="margin-top: 5px;"><i class="fa fa-list"
                                                                             style="margin-right: 4px;"></i>Заказы
        </button>
    </a>
@endif

@if(isset($route_download))
    <a href="{{$route_download}}">
        <button class="btn btn-primary"><i class="fa fa-download"></i>Скачать</button>
    </a>
@endif

