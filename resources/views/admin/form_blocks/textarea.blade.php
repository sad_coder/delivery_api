@if(isset($summernote))
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-lite.min.js"></script>

    <div>
        <textarea id="{{'summernote_'.$name}}" name="{{$name}}" style="margin-bottom: 2%"></textarea>

        <input type="hidden"  id="summernote_value">

        <script>

            $('#'+ "{{'summernote_'.$name}}").summernote({
                placeholder: "{{$placeholder ?? 'Полное описание'}}",
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });

            @if(isset($text))
            var decodeHTML = function (html) {
                    var txt = document.createElement('textarea');
                    txt.innerHTML = html;
                    return txt.value;
                };

            var text = $.parseHTML("{{$text}}")[0].data;
            var decoder = decodeHTML(text);
            $('#'+ "{{'summernote'.$name}}").summernote('code', decoder);
            @endif


        </script>


    </div>
@else
    <div style="margin-top: 20px" class="form-group">
        <label for="exampleFormControlTextarea1">{{$label ?? ''}}</label>
        <textarea name={{$name}} class="form-control" id="exampleFormControlTextarea1"
                  rows="3" placeholder="{{$placeholder ?? ''}}">{{$text ?? null}}</textarea>
    </div>

@endif
