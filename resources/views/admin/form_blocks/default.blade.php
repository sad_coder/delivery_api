<div class="form-group row">
    <label class="label-control col-md-3 col-sm-3 col-xs-12">{{$label}}</label>
    <div class="col-md-9 col-sm-9">
        {{$slot}}
    </div>
</div>
