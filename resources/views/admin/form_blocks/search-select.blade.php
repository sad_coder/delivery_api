{{--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
{{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<div class="container" style="margin-bottom: 1%;">
    <div class="row">
            <label>{{$label}}</label>
            <select class="form-control select2" name="{{$name}}" id="{{$key.'s'}}">
                @if(isset($existOption))
                    <option value="{{$existOption->id}}" selected>{{$existOption->$field}}</option>
                @endif

                @foreach($items as $item)
                    <option value="{{$item->id}}" {{(isset($selectedId) && $selectedId == $item->id) ? 'selected' : ''}}>{{$item->$field}}</option>
                @endforeach
            </select>
    </div>
</div>
<script>
    $('.select2').select2();
</script>
