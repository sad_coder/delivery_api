<div class="row skin skin-flat">
    @foreach($items as $item)

        <div class="col-md-4 col-sm-12">
            <fieldset>
                    <input type="checkbox" {{isset($value)&&in_array($item->id,$value)?'checked':''}} value="{{$item->id}}"
                           name="{{$name}}" id="{{$item->id}}">
                    <label for="test-{{$item->id}}">@if(isset($jsonNestedField) && $jsonNestedField == true) {{$item->{$jsonName}[$field]}} @else{{$item->$field}} @endif</label>
            </fieldset>
            @if(isset($specializationPrice))
                <input placeholder="price" class="form-control"
                       value="{{ !empty($doctorCategories) ?$doctorCategories->where('category_id',$item->id)->first()['price'] : null}}"
                       type="text" name="specializationPrice[{{$item->id}}]">
            @endif
        </div>

    @endforeach

</div>


@if(isset($load))

    @push('styles')
        <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/vendors/css/forms/icheck/icheck.css">
    @endpush

    @push('scripts')
        <script src="/robust-admin/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>

        <script>
            $('.skin-flat input').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        </script>
    @endpush

@endif
