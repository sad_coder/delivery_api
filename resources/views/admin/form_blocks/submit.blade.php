<div class="form-actions center border-top-0">
    <a style="margin-top: 5%;" href="{{ url()->previous() }}" class="btn btn-warning mr-1 btn-min-width">
        <i class="ft-arrow-left align-middle"></i> Назад
    </a>
    <button style="margin-top: 5%;" type="submit" class="btn btn-primary btn-min-width"   id="mySubmit">
        <i class="fa fa-{{$fa ?? 'plus'}}"></i> {{ $title ?? 'Создать' }}
    </button>
</div>
