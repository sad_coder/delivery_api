<style>

    div#image-container {
        margin-top: 3%;
        margin-bottom: 3%;
        margin-left: 2%;
    }

    .send-image-item-class {
        margin-left: 2%;
        margin-top: 3%;
    }

    .image-upload-container {
        position: relative;
    }

    button.image-unpin-button {
        position: absolute;
        top: 78%;
        left: 9%;
        font-size: 19px;
        border-radius: 4px;
        box-shadow: none;
        color: white;
        background: #cf1919;
        border: none;
        padding: 8px;
        cursor: pointer;
        font-weight: 600;
    }

    button.image-set-main-button {
        position: absolute;
        top: 78%;
        left: 41%;
        font-size: 19px;
        border-radius: 4px;
        box-shadow: none;
        color: white;
        background: #3d8bec;
        border: none;
        padding: 8px;
        cursor: pointer;
        font-weight: 600;
    }

    .active-button-image-upload-admin{
        background: #09c709 !important;
    }


</style>


<input id="sendImage" data-buttonText="Your label here."
       type="file" name="file" placeholder="test"
       multiple>
<button type="button" onclick="addPhoto" id="addPhoto"
        class="mt-2 add-photo btn btn-outline-success">
    Добавить изображение
</button>
<input name="imageId" id="imageId" type="hidden" value="">
<input name="mainImageId" id="mainImageId" type="hidden" value="">
<input name="deletedImageId" id="deletedImageId" type="hidden" value="">
<div id="image-container">


</div>


<script>
    let imageContainer = document.getElementById('image-container');


    @if(isset($imageData))
        @foreach($imageData as $image)
            addExistImage({{$image['id']}},"{{$image['path']}}")
        @endforeach
    @endif

    function addExistImage(id,url) {
        let image = document.createElement("img");
        let imgId = document.getElementById("imageId");

        // создаю контейнер для загружаемой картинки
        let oneImageContainer = document.createElement("div")
        oneImageContainer.setAttribute("class", "image-upload-container");

        // создаю кнопку для удаления картинки

        let deleteImageButton = document.createElement("button");
        deleteImageButton.setAttribute("class", "image-unpin-button");
        deleteImageButton.setAttribute("data-image-id", id);
        deleteImageButton.setAttribute("data-image-href", url);
        deleteImageButton.textContent = "открепить";

        deleteImageButton.setAttribute("onclick", "deletePhoto(event)");

        let setMainImageButton = document.createElement("button");
        setMainImageButton.setAttribute("class", "image-set-main-button");
        setMainImageButton.textContent = "сделать главной";
        setMainImageButton.setAttribute("data-image-id", id);
        setMainImageButton.setAttribute("data-image-href", url);

        setMainImageButton.setAttribute("onclick", "setMainImage(event)");


        imgId.value = imgId.value == "" ? id : imgId.value + '|' + id;


        // console.log(response['id']);
        image.setAttribute('width', 500);
        image.setAttribute('class', 'send-image-item-class');
        image.setAttribute('src', url);
        image.setAttribute('height', 'auto');


        oneImageContainer.appendChild(image);
        oneImageContainer.appendChild(deleteImageButton);
        oneImageContainer.appendChild(setMainImageButton);

        imageContainer.appendChild(oneImageContainer);
        let sendImage = document.getElementById('sendImage');
        sendImage.value = null;
    }







    function addPhoto() {

        let formData = new FormData();


        formData.append('file', $('#sendImage')[0].files[0]);
        $.ajax({
            type: "POST",
{{--            url: '{{route('admin.ajax.add_image')}}',--}}
            url: '',
            data: formData,
            processData: false,  // Important!
            contentType: false,
            contentType: false,
            cache: false,

            success: function (response) {
                let image = document.createElement("img");
                let imgId = document.getElementById("imageId");

                // создаю контейнер для загружаемой картинки
                let oneImageContainer = document.createElement("div")
                oneImageContainer.setAttribute("class", "image-upload-container");

                // создаю кнопку для удаления картинки

                let deleteImageButton = document.createElement("button");
                deleteImageButton.setAttribute("class", "image-unpin-button");
                deleteImageButton.setAttribute("data-image-id", response['id']);
                deleteImageButton.setAttribute("data-image-href", response['url']);
                deleteImageButton.textContent = "открепить";

                deleteImageButton.setAttribute("onclick", "deletePhoto(event)");

                let setMainImageButton = document.createElement("button");
                setMainImageButton.setAttribute("class", "image-set-main-button");
                setMainImageButton.textContent = "сделать главной";
                setMainImageButton.setAttribute("data-image-id", response['id']);
                setMainImageButton.setAttribute("data-image-href", response['url']);

                setMainImageButton.setAttribute("onclick", "setMainImage(event)");


                imgId.value = imgId.value == "" ? response['id'] : imgId.value + '|' + response['id'];


                console.log(response['id']);
                console.log(imgId);

                // console.log(response['id']);
                image.setAttribute('width', 500);
                image.setAttribute('class', 'send-image-item-class');
                image.setAttribute('src', response['url']);
                image.setAttribute('height', 'auto');


                oneImageContainer.appendChild(image);
                oneImageContainer.appendChild(deleteImageButton);
                oneImageContainer.appendChild(setMainImageButton);

                imageContainer.appendChild(oneImageContainer);
                let sendImage = document.getElementById('sendImage');
                sendImage.value = null;
            },
        });

    }

    function deletePhoto(event) {

        event.preventDefault();
        let deletedId = event.srcElement.getAttribute('data-image-id');
        let deletedImages = document.getElementById("deletedImageId");

        event.srcElement.parentElement.remove();

        deletedImages.value = deletedImages.value == "" ? deletedId : deletedImages.value + '|' + deletedId;

        console.log(deletedImages.value);
    }

    function setMainImage(event) {
        event.preventDefault();

        event.srcElement.textContent = "Главная";

        let mainImageId = event.srcElement.getAttribute("data-image-id");

        event.srcElement.classList.add("active-button-image-upload-admin");;

        document.getElementById("deletedImageId").value = mainImageId;


        let allButtonsElements = document.getElementsByClassName("active-button-image-upload-admin");
        allButtonsElements = Array.from(allButtonsElements);

        allButtonsElements.forEach((el) =>{

            let elImageId = el.getAttribute("data-image-id");

            if (mainImageId == elImageId) {
                return;
            }

            el.textContent = "сделать главной";
            el.classList.remove("active-button-image-upload-admin");



        })


    }




    document.getElementById("addPhoto").addEventListener("click", addPhoto);
</script>


{{--<svg id="upload-images-svg-admin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"--}}
{{--     version="1.1" id="Capa_1"--}}
{{--     x="0px" y="0px" viewBox="0 0 490.667 490.667" style="enable-background:new 0 0 490.667 490.667;"--}}
{{--     xml:space="preserve">--}}
{{--            <path--}}
{{--                d="M448,128h-67.627l-39.04-42.667H192v64h-64v64H64v213.333c0,23.467,19.2,42.667,42.667,42.667H448     c23.467,0,42.667-19.2,42.667-42.667v-256C490.667,147.2,471.467,128,448,128z M277.333,405.333     c-58.88,0-106.667-47.787-106.667-106.667S218.453,192,277.333,192S384,239.787,384,298.667S336.213,405.333,277.333,405.333z"/>--}}
{{--    <polygon--}}
{{--        points="64,192 106.667,192 106.667,128 170.667,128 170.667,85.333 106.667,85.333 106.667,21.333 64,21.333 64,85.333      0,85.333 0,128 64,128    "/>--}}
{{--    <path--}}
{{--        d="M277.333,230.4c-37.76,0-68.267,30.507-68.267,68.267h0c0,37.76,30.507,68.267,68.267,68.267     c37.76,0,68.267-30.507,68.267-68.267S315.093,230.4,277.333,230.4z"/>--}}
{{--        </svg>--}}
