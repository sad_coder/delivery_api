<style>

    .one-marker {
        display: flex;
        margin-bottom: 6%;
    }

    input.marker-link {
        width: 55%;
    }

    .markerName {
        margin-left: 3%;
        color: #fff;
        padding: 10px;
        font-size: 16px;
        text-shadow: 1px 2px 0px black;
        font-weight: 900;
        border-radius: 3px;
    }

    .markers {
        flex-direction: column;
        text-align: center;
    }

    .markerName {
        width: 40%;
        cursor: default;
    }

    .one-marker {
        flex-wrap: wrap;
    }

    select.selected_search {
        width: 98%;
        margin-top: 1%;
    }

</style>


<div class="markers" style="display: flex">
    @foreach($markers as $marker)
        <div class="one-marker">
            <input type="text" class="marker-link" id="{{$marker->class}}" placeholder="ссылка" autocomplete="off">
            <div class="markerName" style="background: {{$marker->color}}">{{$marker->name}}</div>
            @include('admin.form_blocks.search_marker_block',['needle_link' => $marker->name,'class'=>$marker->class])
        </div>
        <script !src="">
            $("#{{$marker->class}}").bind('change keydown keyup', function () {
                let selectId = this.getAttribute('id');
                let selectElements = document.getElementsByClassName(selectId);
                $.ajax({
                    type: 'POST',
                    url: '/admin/search/product_categories',
                    data: {
                        search_name: this.value,
                    },

                    success: function (response) {
                        console.log(response);
                        response.forEach(e => {
                            let option = document.createElement('option');
                            option.value = e.slug;
                            option.text = e.name;
                            selectElements[0].add(option);
                            selectElements[0].setAttribute('size',response.length +1);
                        });

                    },
                });

            });

            function createDefaultOption(selectElements) {
                let option = document.createElement('option');
                option.text = e.name;
                option.setAttribute('data-link',e.slug);
                selectElements[0].add(option);
                selectElements[0].setAttribute('size',selectElements.length+1);
            }

        </script>

    @endforeach


</div>
