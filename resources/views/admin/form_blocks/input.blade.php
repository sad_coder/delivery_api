<div class="form-group row">
    <label class="label-control col-md-2 col-sm-2">{{$label}} </label>
    <div class="col-md-9 col-sm-9">
        @if($type == 'checkbox')
            <div class="float-left">
                <input name="{{$name}}" value="1" id="{{$id ?? null}}" type="checkbox" class="switchBootstrap" id="switchBootstrap18" data-on-color="success" data-off-color="danger" {{ (bool) $value ? 'checked' : ''}}/>
            </div>
        @elseif($type=='file')
            <span class="btn btn-blue btn-file">
                Browse <input type="file" name="{{$name}}" {{isset($required)?'required':''}} >
            </span>
            <span class="fileNameValue"></span>
        @else
            <input  onchange="{{isset($generateSlug) ? 'generateSlug(value)' : ''}}"  id="{{$id ?? null }}" max="{{$max ?? ''}}" name="{{$name}}" type="{{$type}}" {{isset($required)?'required':''}} value="{{$value ?? null}}" step="{{$step ?? 'any'}}" placeholder="{{$placeholder??''}}" class="form-control">
            <small>{{$hint ?? ''}}</small>
        @endif
    </div>
</div>

@push('scripts')

    @if($type=='file')
        <script>
            $('.btn-file input').change(function() {
                $('.fileNameValue').text($(this).val());
            });
        </script>
    @endif

@endpush

@if($type == 'checkbox' && isset($load))

    @push('styles')
        <link rel="stylesheet" type="text/css" href="/robust-admin/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css">
    @endpush

    @push('scripts')
        <script src="/robust-admin/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js" type="text/javascript"></script>
        <script>
            $("#switchBootstrap18").bootstrapSwitch();
        </script>
    @endpush

@endif
